@extends('front.include.main')
@section('content')
<style>
    .text {
        overflow: hidden;
        text-overflow: ellipsis;
        display: -webkit-box;
        -webkit-line-clamp: 2;
        /* number of lines to show */
        -webkit-box-orient: vertical;
    }
</style>
<style type="text/css">
    .ajax-load {
        background: #e1e1e1;
        padding: 10px 0px;
        width: 100%;
    }
</style>
<meta name="csrf-token" content="{{ csrf_token() }}" />

<!-- Linking -->
<div class="linking">
    <div class="container">
        <ol class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li class="active">Review</li>
        </ol>
    </div>
</div>

<!-- Content -->
<div id="content">

    <!-- Products -->
    <section class="padding-top-40 padding-bottom-60">
        <div class="container">
            <div class="row">
                <!-- Products -->
                <div class="col-md-12" style="align-items:center;text-align:center;">
                    <img src="{{ URL::asset('image/error-404.png') }}">
                    <h4>Haven't purchased this product?</h4>
                    <h6>Sorry! You are not allowed to review this product since you haven't bought it on SportsGear.</h6>
                    <a href="{{ route('index') }}" class="btn-round" style="background-color:#f74b16;">Go To Home</a>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection