@extends('front.include.main')
@section('content')
  <!-- Slid Sec -->
  <section class="slid-sec" > 
    <!-- Main Slider Start -->
    <div class="tp-banner-container">
      <div class="tp-banner-full">
        <ul>
          @foreach($banner as $b)
            @if($b->banner_enable == 'yes')
              @php 
                $href='';
                if($b->image_link == 'yes'){
                  if($b->link_type == 'category'){
                    $href=route('category_product',$b->slug_name);
                  }else if($b->link_type == 'product'){
                    $href=route('product_info',$b->slug_name);
                  }else if($b->link_type == 'brand'){
                    $href=route('brand_product',$b->slug_name);
                  }
                }
              @endphp
              <!-- SLIDE  -->
               
                <li data-transition="random" data-slotamount="7" data-masterspeed="300"  data-saveperformance="off">   
                  @if($href != '')
                    <a href={{$href}}><img src="{{URL::asset('image/'.$b->image)}}"  alt="slider" width="100%" data-bgposition="center bottom" data-bgfit="cover" data-bgrepeat="no-repeat"> </a> 

                  @else 
                    <img src="{{URL::asset('image/'.$b->image)}}"  alt="slider" width="100%" data-bgposition="center bottom" data-bgfit="cover" data-bgrepeat="no-repeat"> 
                  @endif

                </li> 
              
            @endif
          @endforeach
        
        </ul>
      </div>
    </div>
  </section>
  
  <!-- Content -->
  <div id="content"> 


    <!-- Weekly Featured --> </br>
    <section class="padding-bottom-60">
      <div class="container">
        <!-- heading -->
          <div class="heading"><a href="{{ route('product_listing',['type'=>'grid']) }}"><h2>View All</h2></a><hr></div>

        <!-- Items Slider -->
        <div class="item-slide-4 with-nav"> 
          <!-- Product -->
          @foreach($product as $w)
          @if($w->topweekly==1)
          <div class="product">
            <a href="{{ route('product_info',['id'=>$w->slug_name]) }}">
              @php
              $img=json_decode($w->image,true);
              if(count($img) > 0){$images=$img[0];}else {$images='';}
              @endphp
            <article>  <img class="img-responsive height245 inner-img" src="{{URL::asset('image/product/'.$images)}}" alt=""  > 
              <!-- Content --> 
              <span class="tag"></span> <a href="#." class="tittle">{{ $w->name }}</a> 
              <!-- Reviews -->
              <p class="rev">
                  @php $rating=$w->star_rating; @endphp
                  @for ($i =0 ; $i < 5 ; $i++)
                    <i class=" @if($i < $rating) fa fa-star @else fa fa-star-o @endif"></i>
                  @endfor                
                  <span class="margin-left-10">{{  $w->review_count  }}  Review(s)</span>
              </p>

              @if($w->old_price != $w->new_price)
                <div class="price MRP">M.R.P:<i class="fa fa-inr" aria-hidden="true"></i><del>{{ number_format($w->old_price,2) }}</del></div>
                  </br>
                <div class="price offer">Deal Price:<i class="fa fa-inr" aria-hidden="true"></i>{{ number_format($w->new_price,2) }}</div>
              @else
                <div class="price"><i class="fa fa-inr" aria-hidden="true"></i>{{number_format($w->new_price,2)  }}</div>
              @endif    
              <br>
          </div>
          @endif
          @endforeach
        
        </div>
      </div>
    </section>
    
    <!-- Banner -->
    <section class="disply-sec slid-sec margin-bottom-0">
      <div class="container">
        <div class="row"> 
          @php $Bannercount=0; @endphp
          @foreach ($banner as $b1)
              @if($Bannercount >= 2) @break; @endif
              @if($b1->banner_enable != 'yes')
                @php $href='';
                  if($b1->image_link == 'yes'){
                    if($b1->link_type == 'category'){
                      $href=route('category_product',$b1->slug_name);
                    }else if($b1->link_type == 'product'){
                      $href=route('product_info',$b1->slug_name);
                    }else if($b1->link_type == 'brand'){
                    $href=route('brand_product',$b1->slug_name);
                  }
                  }
                @endphp
                
                <div class="col-md-6 pad5">
                  <div class="product">
                    @if($href != '')
                      <a href="{{$href}}"><img src="{{URL::asset('image/'.$b1->image)}}" width="100%"  alt="" ></a> 
                    @else 
                      <img src="{{URL::asset('image/'.$b1->image)}}" width="100%"  alt="" >
                    @endif                                                               
                  </div>
                </div>
                @php $Bannercount++; @endphp
              @endif
          @endforeach
        </div>
      </div>
    </section>
   

    
    
    
    <!-- Latest Products -->
    <section class="padding-bottom-60">
      <div class="container">         
          <!-- heading -->
          <div class="heading">
            <h2>Featured Products</h2>
            <hr>
          </div>
          <!-- Items Slider -->
          <div class="item-slide-4 with-nav"> 
          <!-- Product -->
            @foreach($product as $f)
              @if($f->featured==1)
               @php
                $img=json_decode($f->image,true);
                if(count($img) > 0){
                  $images=$img[0];
                }
                else {
                  $images='';
                }
              @endphp

              <div class="product">
                <a href="{{ route('product_info',['id'=>$f->slug_name]) }}"><article> 
                
                  <img class="img-responsive height245 inner-img" src="{{URL::asset('image/product/'.$images)}}" alt="" > 
                  <!-- Content --> 
                  <span class="tag"></span> <a href="{}" class="tittle">{{ $f->name }}</a> 
                  <!-- Reviews -->
                  <p class="rev">
                    @php $rating1=$f->star_rating; @endphp
                      @for ($j =0 ; $j < 5 ; $j++)
                          <i class=" @if($j < $rating1) fa fa-star @else fa fa-star-o @endif"></i>                      
                      @endfor 

                    <span class="margin-left-10">{{  $f->review_count  }}  Review(s)</span></p>

                  @if($w->old_price != $w->new_price)
                    <div class="price MRP">M.R.P:<i class="fa fa-inr" aria-hidden="true"></i><del>{{ number_format($f->old_price,2) }}</del></div>
                      </br>
                    <div class="price offer">Deal Price:<i class="fa fa-inr" aria-hidden="true"></i>{{ number_format($f->new_price,2) }}</div>
                  @else
                    <div class="price"><i class="fa fa-inr" aria-hidden="true"></i>{{number_format($f->new_price,2)  }}</div>
                  @endif

                  </a>
              </div>
            @endif
          @endforeach
        </div>
      </div>
    </section>


    <!-- Banner section 2-->
    <section class="disply-sec slid-sec margin-bottom-0">
      <div class="container">
        <div class="row"> 
          @php $Bannercount2=0; @endphp
          @foreach ($banner as $b1)

              @if($Bannercount2 >= 2)  
                @if($b1->banner_enable != 'yes')

                  @php $href='';
                    if($b1->image_link == 'yes'){
                      if($b1->link_type == 'category'){
                        $href=route('category_product',$b1->slug_name);
                      }else if($b1->link_type == 'product'){
                        $href=route('product_info',$b1->slug_name);
                      }else if($b1->link_type == 'brand'){
                        $href=route('brand_product',$b1->slug_name);
                      }
                    }
                  @endphp
                  <div class="col-md-6 pad5">
                    <div class="product">
                      @if($href != '')
                        <a href="{{$href}}"><img src="{{URL::asset('image/'.$b1->image)}}" width="100%"  alt="" ></a> 
                      @else 
                        <img src="{{URL::asset('image/'.$b1->image)}}" width="100%"  alt="" >
                      @endif                                         
                    </div>
                  </div>                  
                @endif
              @endif

              @php $Bannercount2++; @endphp
          @endforeach
        </div>
      </div>
    </section>


    <!-- Clients img -->
    <section class="light-gry-bg clients-img">
      <div class="">
        <ul>
          @foreach($brand as $b)
            <a href="{{ route('brand_product',['id'=>$b->slug_name]) }}">
            <li><img src="{{URL::asset('image/'.$b->image)}}" height=80 width="80"  alt="" ></li>
            </a>
          @endforeach          
        </ul>
      </div>
    </section>    
  </div>
  <!-- End Content --> 
@endsection