@extends('front.include.main')
@section('content')
<style>
  .text {
    overflow: hidden;
    text-overflow: ellipsis;
    display: -webkit-box;
    -webkit-line-clamp: 2;
    /* number of lines to show */
    -webkit-box-orient: vertical;
  },
  .radio-input {
   display: inline-block;
    vertical-align: top;
}
</style>

<meta name="csrf-token" content="{{ csrf_token() }}" />

<!-- Content -->
<div id="content">

  <!-- Ship Process -->
  <div class="ship-process padding-top-30 padding-bottom-30">
    <div class="container">
      <ul class="row">

        <!-- Step 1 -->
        <li class="col-sm-3">
          <div class="media-left"> <i class="fa fa-check"></i> </div>
          <div class="media-body"> <span>Step 1</span>
            <h6>Shopping Cart</h6>
          </div>
        </li>

        <!-- Step 2 -->
        <li class="col-sm-3">
          <div class="media-left"> <i class="fa fa-check"></i> </div>
          <div class="media-body"> <span>Step 2</span>
            <h6>Payment Methods</h6>
          </div>
        </li>

        <!-- Step 3 -->
        <li class="col-sm-3 current">
          <div class="media-left"> <i class="flaticon-delivery-truck"></i> </div>
          <div class="media-body"> <span>Step 3</span>
            <h6>Delivery Methods</h6>
          </div>
        </li>

        <!-- Step 4 -->
        <li class="col-sm-3">
          <div class="media-left"> <i class="fa fa-check"></i> </div>
          <div class="media-body"> <span>Step 4</span>
            <h6>Confirmation</h6>
          </div>
        </li>
      </ul>
    </div>
  </div>

  <!-- Payout Method -->
  <section class="padding-bottom-60">
    <div class="container">
      <!-- Payout Method -->
      <div class="pay-method">
        <div class="row">
          <div class="col-md-6">

            <!-- Your information -->
            <div class="heading">
              <h2>Your information</h2>
              <hr>
            </div>
            <form method="post" action="{{route('add_address')}}" enctype="multipart/form-data" class="uk-form-stacked task-form address_form" onsubmit="return add_address()">
              @csrf
              <div class="row">
                <div class="col-sm-6">
                  <label> First name
                    <input class="form-control form_data" type="text" name="firstname" required>
                  </label>
                </div>
                <div class="col-sm-6">
                  <label> Last Name
                    <input class="form-control form_data" type="text" name="lastname" required>
                  </label>
                </div>

                <!-- Card Number -->
                <div class="col-sm-12">
                  <label> Address
                   <textarea class="form-control form_data" name="address" required></textarea>
                  </label>
                </div><br>
                  <div class="col-md-6">
                      <label for="city">City</label>
                      <input type="text" value="" id="e_city" name="city" class="form-control form_data">
                  </div>
                  <div class="col-md-6">
                      <label for="state">State</label>
                      <select id="e_state" class="form-control form_data" required name="state">
                        @foreach($states as $state)
                          <option value="{{$state->id}}">{{$state->name}}</option>
                        @endforeach
                      </select>
                  </div>
             
               

                <!-- Zip code -->
                <div class="col-sm-6">
                  <label> Pin code
                    <input class="form-control form_data" type="text" name="pincode" required>
                  </label>
                </div>

                <!-- Address -->
                <div class="col-sm-6">
                  <label> Mobile
                    <input class="form-control form_data" type="text" name="mobile" required>
                  </label>
                </div>

               

                <!-- Number -->
                <div class="col-sm-6">
                  <label> Type </label>
                  <select class="selectpicker form_data" name="type" required>
                    <option value="home"> Home</option>
                    <option value="office"> Office</option>
                    <option value="other"> Other</option>
                  </select>
                </div>

                <div class="col-sm-6">
                  <label> Default </label>
                  <select class="selectpicker form_data" name="default">
                    <option value="1"> Yes</option>
                    <option value="0"> No</option>
                  </select>
                </div>
              </div><br>
              <button type="submit" class="btn-round btn-light pro-btn" style="border-color:#f74b16;">Submit</button>
            </form>
          </div>
          @php $txn_id=rand(1111111111,9999999999).'_'.Auth::User()->id; @endphp
          <!-- Select Your Transportation -->
          <div class="col-md-6">
            <div class="heading">
              <h2>Select Your Address</h2>
              <hr>
            </div>
            <div class="transportation">
              <div class="row">
              <input type="hidden" value="1" id="add_exist" />
                <form method="POST" action="{{route('place_order')}}" id="place_order">
                    @csrf
                    
                      @if(sizeof($address)>0)
                        @php $numOfCols = 2;
                        $rowCount = 0;
                        $bootstrapColWidth = 12 / $numOfCols; @endphp

                        @foreach($address as $key=>$a)
                          <div class="col-sm-6">
                            <div class="charges">
                              <h6>{{ ucfirst($a->firstname) }} {{ ucfirst($a->lastname) }}</h6><br>
                              <span>{{ $a->address }} {{ $a->city }} <br> {{ $a->mobile }} </span><br>
                              <span>{{ $a->pincode }}</span>
                              <br>
                            </div>
                            <div style="margin-bottom: 15px;text-align:center;">
                              <input class="radio-input" type="radio" name="address_id" value="{{$a->id}}" @if($a->default==1 || $key==0) checked @php $dl_charge=$a->dl_charge; @endphp @endif required onchange="change_dl('{{$a->dl_charge}}')" />
                            </div>
                          </div>
                        @endforeach
                        @php $rowCount++; @endphp
                          @if($rowCount % $numOfCols == 0)
                              </div><div class="row"> 
                          @endif
                      @else
                        <input type="hidden" value="0" id="add_exist" />
                      @endif
                    </div>
                    <input type="hidden" value="{{$items}}" name="items">
                    <input type="hidden" value="{{ $txn_id }}" name="txn_id">
                </form>
              </div>
            </div>
          </div>
        </div>
        <input type="hidden" id="product_amount" value="{{ $price }}">
        <input type="hidden" id="dl_amount" value="@if(isset($dl_charge) && $price<2001) {{ $dl_charge }} @else 0 @endif">
        <div class="pro-btn"> 
        <div class="row">
          <div class="col-md-12">
            <b>Total Payable Amount :  {{ $price }} <span id="dl_charge">@if(isset($dl_charge) && $price<2001) + {{$dl_charge}} (Delivery Charge) @endif</span></b>
          </div>
        </div><br><br>
        <div></div>
            {{-- <button type="button" class="btn btn-round btn-light" onclick="place_order('cod')">COD</button>  --}}
            <button type="button" class="btn btn-round" onclick="submit_form()">Place Order</button> 

            <form action="{{url('PayuMoney/PayUMoney_form.php')}}" method='post' id="payuform">
                <input type="hidden" name="txnid" value="{{ $txn_id }}" />
                <input type="hidden" name="productinfo" value="" id="productinfo" />
                <input type="hidden" name="amount" id="amount" />
                <input type="hidden" name="email" value="{{ Auth::User()->email }}" />
                <input type="hidden" name="firstname" value="{{ Auth::User()->name }}" />
                <input type="hidden" name="lastname" value="" />
                <input type="hidden" name="surl" value="{{route('payment_status')}}" />
                <input type="hidden" name="furl" value="{{route('payment_status')}}" />
                <input type="hidden" name="phone" value="{{ Auth::User()->phone }}" />
                <input type="hidden" name="udf1" value="{{ Auth::User()->id }}" />
            </form>

        </div>
      </div>
  </section>
</div>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script>
function change_dl(amount){
  if (('#product_amount').val()>2000) {
      $('#dl_charge').text('+ '+amount+' (Delivery Charge)');
      $('#dl_amount').val(amount);
  }
  else{
      $('#dl_charge').text('');
      $('#dl_amount').val(0);
  }
}

  function add_address(){
    $.ajax({
        type: "POST",
        url: "{{route('add_address')}}",
        data: $('.address_form').serialize(),
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function(res) {
            if (res.status=='success') {
                var html='<div class="col-sm-6"><div class="charges"><h6>'+res.data["firstname"]+' '+res.data["lastname"]+'</h6><br><span>'+res.data["address"]+' '+res.data["city"]+'<br>'+res.data["mobile"]+'</span><br><span>'+res.data["pincode"]+'</span><br></div><div style="text-align:center;"><input class="radio-input" type="radio" name="address_id" value="'+res.data["id"]+'" required onchange="change_dl('+res.data["dl_charge"]+')" checked /></div></div>';
              $('#place_order').append(html);
              change_dl(res.data["dl_charge"]);
            } else {
              alert('Fail to add address, try again later!!');
            }
        }
    });
    return false;
  }

  function submit_form(){
    $.ajax({
        type: "POST",
        url: "{{route('place_order')}}",
        data: $('#place_order').serialize(),
        success: function(res) {
            if (res.status=='success') {
                $('#productinfo').val(res.order_id);
                $('#amount').val(res.amount);
                $('#payuform').submit();
            } else {
              alert('Fail to place order, please refresh the page and try again later!!');
            }
        }
    });
  }

  function place_order(type){
    var add=$("#add_exist").val();
    if (add==1) {
      $('#type').val(type);
      $('#place_order').submit();
    } else {
      alert('Please add delivery address first!!');
    }
  }
</script>
@endsection