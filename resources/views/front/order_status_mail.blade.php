<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" "http://www.w3.org/TR/REC-html40/loose.dtd">
<html lang="en">
  <head>

    <!--[if gte mso 15]>
      <xml>
        <o:OfficeDocumentSettings>
          <o:AllowPNG />
            <o:PixelsPerInch>96</o:PixelsPerInch>
          </o:OfficeDocumentSettings>
        </xml>
    <![endif]-->
    <meta name="viewport" content="width=device-width">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="x-apple-disable-message-reformatting">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <!-- Shipment delivered email template for Shopify -->
    <style type="text/css" data-premailer="ignore">
      /* What it does: Remove spaces around the email design added by some email clients. */
      /* Beware: It can remove the padding / Margin and add a background color to the compose a reply window. */
      html,
      body {
        Margin: 0 auto !important;
        padding: 0 !important;
        width: 100% !important;
        height: 100% !important;
      }
      /* What it does: Stops email clients resizing small text. */
      * {
        -ms-text-size-adjust: 100%;
        -webkit-text-size-adjust: 100%;
        text-rendering: optimizeLegibility;
        -webkit-font-smoothing: antialiased;
        -moz-osx-font-smoothing: grayscale;
      }
      /* What it does: Forces Outlook.com to display emails full width. */
      .ExternalClass {
        width: 100%;
      }
      /* What is does: Centers email on Android 4.4 */
      div[style*="Margin: 16px 0"] {
        Margin: 0 !important;
      }
      /* What it does: Stops Outlook from adding extra spacing to tables. */
      table,
      th {
        mso-table-lspace: 0pt;
        mso-table-rspace: 0pt;
      }
      /* What it does: Fixes Outlook.com line height. */
      .ExternalClass,
      .ExternalClass * {
        line-height: 100% !important;
      }
      /* What it does: Fixes webkit padding issue. Fix for Yahoo mail table alignment bug. Applies table-layout to the first 2 tables then removes for anything nested deeper. */
      table {
        border-spacing: 0 !important;
        border-collapse: collapse !important;
        border: none;
        Margin: 0 auto;
      }
      div[style*="Margin: 16px 0"] {
        Margin: 0 !important;
      }
      /* What it does: Uses a better rendering method when resizing images in IE. */
      img {
        -ms-interpolation-mode: bicubic;
      }
      /* What it does: Overrides styles added when Yahoo's auto-senses a link. */
      .yshortcuts a {
        border-bottom: none !important;
      }
      /* What it does: Overrides blue, underlined link auto-detected by iOS Mail. */
      /* Create a class for every link style needed; this template needs only one for the link in the footer. */
      /* What it does: A work-around for email clients meddling in triggered links. */
      *[x-apple-data-detectors],
      /* iOS */
      .x-gmail-data-detectors,
      /* Gmail */
      .x-gmail-data-detectors *,
      .aBn {
        border-bottom: none !important;
        cursor: default !important;
        color: inherit !important;
        text-decoration: none !important;
        font-size: inherit !important;
        font-family: inherit !important;
        font-weight: inherit !important;
        line-height: inherit !important;
      }
      /* What it does: Prevents Gmail from displaying an download button on large, non-linked images. */
      .a6S {
        display: none !important;
        opacity: 0.01 !important;
      }
      /* If the above doesn't work, add a .g-img class to any image in question. */
      img.g-img+div {
        display: none !important;
      }
      /* What it does: Prevents underlining the button text in Windows 10 */
      a,
      a:link,
      a:visited {
        color: #ababab;
        text-decoration: none !important;
      }
      .header a {
        color: #b2b2b2;
        text-decoration: none;
        text-underline: none;
      }
      .main a {
        color: #ababab;
        text-decoration: none;
        text-underline: none;
        word-wrap: break-word;
      }
      .main .section.customer_and_shipping_address a,
      .main .section.shipping_address_and_fulfillment_details a {
        color: #4c5154;
        text-decoration: none;
        text-underline: none;
        word-wrap: break-word;
      }
      .footer a {
        color: #edff00;
        text-decoration: none;
        text-underline: none;
      }
      /* What it does: Overrides styles added images. */
      img {
        border: none !important;
        outline: none !important;
        text-decoration: none !important;
      }
      td.menu_bar_1 a:hover,
      td.menu_bar_6 a:hover {
        color: #ababab !important;
      }
      th.related_product_wrapper.first {
        border-right: 13px solid #ffffff;
        padding-right: 6px;
      }
      th.related_product_wrapper.last {
        border-left: 13px solid #ffffff;
        padding-left: 6px;
      }
    </style>

<!-->
    <style type="text/css" data-premailer="ignore">
      /* What it does: Fixes fonts for Google WebFonts; */
      [style*="Libre Franklin"] {
        font-family: 'Libre Franklin', -apple-system, BlinkMacSystemFont, 'Segoe UI', Arial, sans-serif !important;
      }
      [style*="Arimo"] {
        font-family: 'Arimo', -apple-system, BlinkMacSystemFont, 'Segoe UI', Arial, sans-serif !important;
      }
      [style*="Libre Franklin"] {
        font-family: 'Libre Franklin', -apple-system, BlinkMacSystemFont, 'Segoe UI', Arial, sans-serif !important;
      }
      [style*="Arimo"] {
        font-family: 'Arimo', -apple-system, BlinkMacSystemFont, 'Segoe UI', Arial, sans-serif !important;
      }
    </style>
    <link href="https://fonts.googleapis.com/css?family=Libre+Franklin:400,700%7CArimo:700,400%7CArimo:700,400%7CLibre+Franklin:700,700&amp;subset=latin-ext" rel="stylesheet" type="text/css" data-premailer="ignore">

    <!--<![endif]-->
    <style type="text/css" data-premailer="ignore">
      /* Media Queries */
      /* What it does: Removes right gutter in Gmail iOS app */
      @media only screen and (min-device-width: 375px) and (max-device-width: 413px) {
        /* iPhone 6 and 6+ */
        .container {
          min-width: 375px !important;
        }
      }
      /* Main media query for responsive styles */
      @media only screen and (max-width:480px) {
        /* What it does: Overrides email-container's desktop width and forces it into a 100% fluid width. */
        .email-container {
          width: 100% !important;
          min-width: 100% !important;
        }
        .section>th {
          padding: 13px 26px 13px 26px !important;
        }
        .section.divider>th {
          padding: 26px 26px !important;
        }
        .main .section:first-child>th,
        .main .section:first-child>td {
          padding-top: 26px !important;
        }
        .main .section:last-child>th,
        .main .section:last-child>td {
          padding-bottom: 39px !important;
        }
        .section.recommended_products>th,
        .section.discount>th {
          padding: 26px 26px !important;
        }
        /* What it does: Forces images to resize to the width of their container. */
        img.fluid,
        img.fluid-centered {
          width: 100% !important;
          min-width: 100% !important;
          max-width: 100% !important;
          height: auto !important;
          Margin: auto !important;
          box-sizing: border-box;
        }
        /* and center justify these ones. */
        img.fluid-centered {
          Margin: auto !important;
        }
        /* What it does: Forces table cells into full-width rows. */
        th.stack-column,
        th.stack-column-left,
        th.stack-column-center,
        th.related_product_wrapper,
        .column_1_of_2,
        .column_2_of_2 {
          display: block !important;
          width: 100% !important;
          min-width: 100% !important;
          direction: ltr !important;
          box-sizing: border-box;
        }
        /* and left justify these ones. */
        th.stack-column-left {
          text-align: left !important;
        }
        /* and center justify these ones. */
        th.stack-column-center,
        th.related_product_wrapper {
          text-align: center !important;
          border-right: none !important;
          border-left: none !important;
        }
        .column_button,
        .column_button>table,
        .column_button>table th {
          width: 100% !important;
          text-align: center !important;
          Margin: 0 !important;
        }
        .column_1_of_2 {
          padding-bottom: 26px !important;
        }
        .column_1_of_2 th {
          padding-right: 0 !important;
        }
        .column_2_of_2 th {
          padding-left: 0 !important;
        }
        .column_text_after_button {
          padding: 0 13px !important;
        }
        /* Adjust product images */
        th.table-stack {
          padding: 0 !important;
        }
        th.product-image-wrapper {
          padding: 26px 0 13px 0 !important;
        }
        img.product-image {
          width: 160px !important;
          max-width: 160px !important;
        }
        tr.row-border-bottom th.product-image-wrapper {
          border-bottom: none !important;
        }
        th.related_product_wrapper.first,
        th.related_product_wrapper.last {
          padding-right: 0 !important;
          padding-left: 0 !important;
        }
        .text_banner th.banner_container {
          padding: 13px !important;
        }
        .mobile_app_download .column_1_of_2 .image_container {
          padding-bottom: 0 !important;
        }
        .mobile_app_download .column_2_of_2 .image_container {
          padding-top: 0 !important;
        }
      }
    </style>
    <style type="text/css" data-premailer="ignore">
      /* Custom Media Queries */
      @media only screen and (max-width:480px) {
        .column_logo,
        .logo {
          text-align: center !important;
          Margin: 0 auto !important;
        }
        .header .section_wrapper_th {
          padding: 0 13px !important;
        }
        th.product-image-wrapper {
          border: 1px solid #f5f5f5 !important;
          border-bottom: 0 !important;
        }
        th.product-details-wrapper {
          padding-right: 13px !important;
          padding-left: 13px !important;
          border: 1px solid #f5f5f5 !important;
          border-top: 0 !important;
        }
        .section.products>th,
        .section.products_with_pricing>th,
        .section.products_with_refund>th,
        .section.products_with_exchange>th,
        .section.discount>th {
          padding-left: 13px !important;
          padding-right: 13px !important;
        }
        .section.recommended_products>th {
          padding-left: 0px !important;
          padding-right: 0px !important;
        }
        th.related_product_wrapper.first,
        th.related_product_wrapper.last {
          padding-left: 13px !important;
          padding-right: 13px !important;
        }
        .section.recommended_products,
        .related_products_title,
        .section.recommended_products,
        .related_products_title h1,
        th.discount_title h1,
        .section.heading h1 {
          padding: 0 !important;
          text-align: center !important;
        }
        .product-table h3 {
          padding-left: 13px !important;
        }
        th.discount_code_wrapper {
          padding: 20px !important;
        }
        th.related_product_wrapper th a p,
        th.discount_title,
        th.discount_title h1,
        th.discount_text *,
        th.discount_terms * {
          text-align: center !important;
        }
        .pricing-table .table-title {
          padding-left: 13px !important;
        }
        .pricing-table .table-text {
          padding-right: 13px !important;
        }
        .footer .section_wrapper_th {
          padding: 39px 26px !important;
        }
        th.column_2_of_2 {
          padding-left: 0px !important;
          text-align: left !important;
        }
        .footer th.column_2_of_2 {
          padding-top: 26px !important;
          border-top: 1px solid #f5f5f5 !important;
          border-bottom: 1px solid #f5f5f5 !important;
        }
        td.menu_bar_6 {
          padding-bottom: 0 !important;
        }
      }
    </style>
  </head>
  <body class="body" id="body" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" bgcolor="#ffffff" style="-webkit-text-size-adjust: none; -ms-text-size-adjust: none; margin: 0; padding: 0;">

    <!--[if !mso 9]>

<!-->
    <div style="display: none; overflow: hidden; line-height: 1px; max-height: 0px; max-width: 0px; opacity: 0; mso-hide: all;">
      The below items from your order have now been {{$order_detail->tracking_status}}.
    </div>

    <!--<![endif]-->

    <!-- BEGIN: CONTAINER -->
    <table class="container container_header" cellpadding="0" cellspacing="0" border="0" width="100%" style="border-collapse: collapse; min-width: 100%;" role="presentation" bgcolor="#161616">
      <tbody>
        <tr>
          <th valign="top" style="mso-line-height-rule: exactly;background-color: white;">
            <center style="width: 100%;">
              <table border="0" width="600" cellpadding="0" cellspacing="0" align="center" style="width: 600px; min-width: 600px; max-width: 600px; margin: auto;" class="email-container" role="presentation">
                <tr>
                  <th valign="top" style="mso-line-height-rule: exactly;">

                    <!-- BEGIN : SECTION : HEADER -->
                    <table class="section_wrapper header" data-id="header" id="section-header" border="0" width="100%" cellpadding="0" cellspacing="0" align="center" style="min-width: 100%;" role="presentation" bgcolor="#161616">
                      <tr>
                        <td class="section_wrapper_th" style="mso-line-height-rule: exactly; padding: 0 52px; background-color:white;" bgcolor="#161616">
                          <table border="0" width="100%" cellpadding="0" cellspacing="0" align="center" style="min-width: 100%;" role="presentation">
                            <tr>
                              <th class="column_logo" style="mso-line-height-rule: exactly; padding-top: 13px; padding-bottom: 13px; margin: 0;background-color: white;" align="left" bgcolor="#161616">

                                <!-- Logo : BEGIN -->
                                <a href="{{url('/')}}" target="_blank" style="color: #b2b2b2; text-decoration: none !important; text-underline: none;">
                                  <img src="{{URL::asset('frontend/main-files/html/images/SportsLogo.png')}}" class="logo " width="120" border="0" style="width: 120px; height: auto !important; display: block; text-align: left; margin: 0;">
                                </a>

                                <!-- Logo : END -->
                              </th>
                            </tr>
                          </table>
                        </td>
                      </tr>
                    </table>

                    <!-- END : SECTION : HEADER -->
                  </th>
                </tr>
              </table>
            </center>
          </th>
        </tr>
      </tbody>
    </table>

    <!-- END : CONTAINER -->

    <!-- BEGIN: CONTAINER -->
    <table class="container container_main" cellpadding="0" cellspacing="0" border="0" width="100%" style="border-collapse: collapse; min-width: 100%;" role="presentation" bgcolor="#ffffff">
      <tbody>
        <tr>
          <th valign="top" style="mso-line-height-rule: exactly;">
            <center style="width: 100%;">
              <table border="0" width="600" cellpadding="0" cellspacing="0" align="center" style="width: 600px; min-width: 600px; max-width: 600px; margin: auto;" class="email-container" role="presentation">
                <tr>
                  <th valign="top" style="mso-line-height-rule: exactly;">

                    <!-- BEGIN : SECTION : MAIN -->
                    <table class="section_wrapper main" data-id="main" id="section-main" border="0" width="100%" cellpadding="0" cellspacing="0" align="center" style="min-width: 100%; text-align: left;" role="presentation" bgcolor="#ffffff">
                      <tr>
                        <td class="section_wrapper_th" style="mso-line-height-rule: exactly; padding-top: 13px;" align="left" bgcolor="#ffffff">
                          <table border="0" width="100%" cellpadding="0" cellspacing="0" align="center" style="min-width: 100%;" id="mixContainer" role="presentation">

                            <!-- BEGIN SECTION: Heading -->
                            <tr id="section-3251497" class="section heading">
                              <th style="mso-line-height-rule: exactly; padding: 26px 52px 13px;" align="left" bgcolor="#ffffff">
                                <table cellspacing="0" cellpadding="0" border="0" width="100%" role="presentation">
                                  <tr>
                                    <th style="mso-line-height-rule: exactly;" align="left" bgcolor="#ffffff" valign="top">
                                      <h1 data-key="3251497_heading" style="font-family: -apple-system,BlinkMacSystemFont,'Segoe UI',Arial,'Arimo'; font-size: 30px; line-height: 50px; font-weight: 700; color: #161616; text-transform: none; margin: 0;">Shipment {{$order_detail->tracking_status}}</h1>
                                    </th>
                                  </tr>
                                </table>
                              </th>
                            </tr>

                            <!-- END SECTION: Heading -->

                            <!-- BEGIN SECTION: Introduction -->
                            <tr id="section-3251498" class="section introduction">
                              <th style="mso-line-height-rule: exactly; padding: 13px 52px;" align="left" bgcolor="#ffffff">
                                <p style="mso-line-height-rule: exactly; font-family: -apple-system,BlinkMacSystemFont,'Segoe UI',Arial,'Libre Franklin'; font-size: 16px; line-height: 26px; font-weight: 400; color: #4c5154; margin: 0 0 13px;" align="left">
                                  <span data-key="3251498_greeting_text" style="text-align: left; font-family: -apple-system,BlinkMacSystemFont,'Segoe UI',Arial,'Libre Franklin'; font-size: 16px; line-height: 26px; font-weight: 400; color: #4c5154;">
                                          Hi
                                        </span>
                                  {{$data->name}},
                                </p>
                                <span data-key="3251498_introduction_text" class="text" style="text-align: left; font-family: -apple-system,BlinkMacSystemFont,'Segoe UI',Arial,'Libre Franklin'; font-size: 16px; line-height: 26px; font-weight: 400; color: #4c5154;">
                                      <p style="mso-line-height-rule: exactly; font-family: -apple-system,BlinkMacSystemFont,'Segoe UI',Arial,'Libre Franklin'; font-size: 16px; line-height: 26px; font-weight: 400; color: #4c5154; margin: 13px 0 0;" align="left"></p>
                                      <p style="mso-line-height-rule: exactly; font-family: -apple-system,BlinkMacSystemFont,'Segoe UI',Arial,'Libre Franklin'; font-size: 16px; line-height: 26px; font-weight: 400; color: #4c5154; margin: 13px 0 0;" align="left">The below items from your order have now been {{$order_detail->tracking_status}}.</p>
                                    </span>
                              </th>
                            </tr>

                            <!-- END SECTION: Introduction -->

                            <!-- BEGIN SECTION: Divider -->
                            <tr id="section-3251499" class="section divider">
                              <th style="mso-line-height-rule: exactly; padding: 26px 52px;" align="left" bgcolor="#ffffff">
                                <table cellspacing="0" cellpadding="0" border="0" width="100%" role="presentation">
                                  <tr>
                                    <th style="mso-line-height-rule: exactly; border-top-width: 1px; border-top-color: #f5f5f5; border-top-style: solid;" align="left" bgcolor="#ffffff" valign="top">
                                    </th>
                                  </tr>
                                </table>
                              </th>
                            </tr>

                            <!-- END SECTION: Divider -->

                            <!-- BEGIN SECTION: Order Number And Date -->
                            <tr id="section-3251500" class="section order_number_and_date">
                              <th style="mso-line-height-rule: exactly; padding: 13px 52px;" align="left" bgcolor="#ffffff">
                                <h2 style="font-family: -apple-system,BlinkMacSystemFont,'Segoe UI',Arial,'Arimo'; color: #161616; font-size: 18px; line-height: 30px; font-weight: 700; text-transform: none; padding-top: 13px; margin: 0;" align="left">
                                  <span data-key="3251500_order_number">Order No.</span> #{{$order_detail->order_id}}
                                </h2>
                                <p class="muted" style="mso-line-height-rule: exactly; font-family: -apple-system,BlinkMacSystemFont,'Segoe UI',Arial,'Libre Franklin'; font-size: 14px; line-height: 26px; font-weight: normal; color: #a9aaaa; margin: 0;" align="left">{{date('M d,Y',strtotime($order_detail->date))}}</p>
                              </th>
                            </tr>

                            <!-- END SECTION: Order Number And Date -->

                            @php 
                            $address=explode(",",$order_detail->dl_address);

                            @endphp
                            <!-- BEGIN SECTION: Shipping Address And Fulfillment Details -->
                            <tr id="section-3251501" class="section shipping_address_and_fulfillment_details">

                              <!-- BEGIN : 2 COLUMNS : SHIP_TO -->
                              <th style="mso-line-height-rule: exactly; padding: 13px 52px;" align="left" bgcolor="#ffffff">
                                <table border="0" width="100%" cellpadding="0" cellspacing="0" align="center" style="min-width: 100%;" role="presentation">
                                  <tr>

                                    <!-- BEGIN : Column 1 of 2 : SHIP_TO -->
                                    <th width="50%" class="column_1_of_2 column_ship_to " style="mso-line-height-rule: exactly;" align="left" bgcolor="#ffffff" valign="top">
                                      <table align="center" border="0" width="100%" cellpadding="0" cellspacing="0" style="min-width: 100%;" role="presentation">
                                        <tr>
                                          <th style="mso-line-height-rule: exactly; padding-right: 5%;" align="left" bgcolor="#ffffff" valign="top">
                                            <h3 data-key="3251501_ship_to" style="font-family: -apple-system,BlinkMacSystemFont,'Segoe UI',Arial,'Arimo'; color: #161616; font-size: 18px; line-height: 30px; font-weight: 700; text-transform: none; padding-top: 6px; padding-bottom: 6px; border-bottom-width: 0; margin: 0;" align="left">Shipping Address</h3>
                                          </th>
                                        </tr>
                                        <tr>
                                          <th class="shipping_address " style="mso-line-height-rule: exactly; padding-right: 5%;" align="left" bgcolor="#ffffff" valign="top">
                                            <p style="mso-line-height-rule: exactly; font-family: -apple-system,BlinkMacSystemFont,'Segoe UI',Arial,'Libre Franklin'; font-size: 16px; line-height: 26px; font-weight: 400; color: #4c5154; margin: 0;" align="left">{{$address[0]}} <br>
                                              {{$address[1]}}<br>
                                              {{$address[2]}}<br>
                                              {{$address[3]}} , {{$address[4]}}<br>
                                            </p>
                                          </th>
                                        </tr>
                                      </table>
                                    </th>

                                    <!-- END : Column 1 of 2 : SHIP_TO -->

                                    <!-- BEGIN : Column 2 of 2 : TRACKING_INFO -->
                                    <th width="50%" class="column_2_of_2 column_tracking_info " style="mso-line-height-rule: exactly;" align="left" bgcolor="#ffffff" valign="top">
                                      <table align="center" border="0" width="100%" cellpadding="0" cellspacing="0" style="min-width: 100%;" role="presentation">
                                        <tr>
                                          <th style="mso-line-height-rule: exactly; padding-left: 26px;" align="left" bgcolor="#ffffff" valign="top">
                                            <h3 data-key="3251501_shipping_method" style="font-family: -apple-system,BlinkMacSystemFont,'Segoe UI',Arial,'Arimo'; color: #161616; font-size: 18px; line-height: 30px; font-weight: 700; text-transform: none; padding-top: 6px; padding-bottom: 6px; border-bottom-width: 0; margin: 0;" align="left">Payment Method</h3>
                                          </th>
                                        </tr>
                                        <tr>
                                          <th style="mso-line-height-rule: exactly; padding-left: 26px;" align="left" bgcolor="#ffffff" valign="top">
                                            <p style="mso-line-height-rule: exactly; font-family: -apple-system,BlinkMacSystemFont,'Segoe UI',Arial,'Libre Franklin'; font-size: 16px; line-height: 26px; font-weight: 400; color: #4c5154; margin: 0;" align="left">
                                              Transaction ID<br>
                                              <a href="#" style="color: #ababab; text-decoration: none !important; text-underline: none; word-wrap: break-word;">{{$order_detail->transaction_id}}</a><br>
                                              <br>
                                            </p>
                                          </th>
                                        </tr>
                                        <tr>
                                          <th style="mso-line-height-rule: exactly; padding-left: 26px;" align="left" bgcolor="#ffffff" valign="top">
                                            <h3 data-key="3251501_tracking_items" style="font-family: -apple-system,BlinkMacSystemFont,'Segoe UI',Arial,'Arimo'; color: #161616; font-size: 18px; line-height: 30px; font-weight: 700; text-transform: none; padding-top: 6px; padding-bottom: 6px; border-bottom-width: 0; margin: 0;" align="left">Shipped Items</h3>
                                          </th>
                                        </tr>
                                        <tr>
                                          <th style="mso-line-height-rule: exactly; padding-left: 26px;" align="left" bgcolor="#ffffff" valign="top">
                                            <p style="mso-line-height-rule: exactly; font-family: -apple-system,BlinkMacSystemFont,'Segoe UI',Arial,'Libre Franklin'; font-size: 16px; line-height: 26px; font-weight: 400; color: #4c5154; margin: 0;" align="left">
                                            {{count(json_decode($order_detail->product_id))}}   <br>
                                            </p>
                                          </th>
                                        </tr>
                                      </table>
                                    </th>

                                    <!-- END : Column 2 of 2 : TRACKING_INFO -->
                                  </tr>
                                </table>
                              </th>

                              <!-- END : 2 COLUMNS : TRACKING_INFO -->
                            </tr>

                            <!-- END SECTION: Shipping Address And Fulfillment Details -->

                            <!-- BEGIN SECTION: Products -->
                            <tr id="section-3251502" class="section products">
                              <th style="mso-line-height-rule: exactly; padding: 13px 32px;" align="left" bgcolor="#ffffff">
                                <table class="table-inner" cellspacing="0" cellpadding="0" border="0" width="100%" style="min-width: 100%;" role="presentation">

                                  <!-- Bold 1 -->

                                  <!-- end Bold 1 -->
                                  <tr>
                                    <th class="product-table" style="mso-line-height-rule: exactly;" align="left" bgcolor="#ffffff" valign="top">
                                      <table cellspacing="0" cellpadding="0" border="0" width="100%" style="min-width: 100%;" role="presentation">
                                        <tr>
                                          <th colspan="2" class="product-table-h3-wrapper" style="mso-line-height-rule: exactly;" align="left" bgcolor="#ffffff" valign="top">
                                            <h3 data-key="3251502_item" style="font-family: -apple-system,BlinkMacSystemFont,'Segoe UI',Arial,'Arimo'; color: #161616; font-size: 18px; line-height: 30px; font-weight: 700; text-transform: none; border-bottom-width: 0; border-bottom-color: #f5f5f5; border-bottom-style: solid; padding-top: 6px; padding-bottom: 6px; padding-left: 20px; margin: 0;" align="left">Items Delivered</h3>
                                          </th>
                                        </tr>

                                        <!-- Bold 2 -->

                                        <!-- end Bold 2 -->

                                        @php $product_details= json_decode($order_detail->product_detail);
                                          $sum = 0;
                                        @endphp

                                        @foreach($product_details as $key)
                                          @foreach($products as $product) @if($key->product_id==$product->id) @php $prod=$product; @endphp @endif @endforeach
                                            
                                            @php $images=json_decode($prod->image); @endphp

                                            <tr class="row-border-bottom">
                                              <th class="table-stack product-image-wrapper stack-column-center" width="1" style="mso-line-height-rule: exactly; padding: 20px 10px 20px 20px; border-color: #f5f5f5; border-style: solid; border-width: 1px 0 1px 1px;" align="left" bgcolor="#ffffff" valign="middle"><img width="120" class="product-image" src="{{ asset('image/product/'.$images[0]) }}" alt="WAVE Kit" style="vertical-align: middle; text-align: center; width: 120px; max-width: 120px; height: auto !important; border-radius: 0px; padding: 0px;"></th>
                                              <th class="product-details-wrapper table-stack stack-column" style="mso-line-height-rule: exactly; padding: 20px 20px 20px 10px; border-color: #f5f5f5; border-style: solid; border-width: 1px 1px 1px 0;" align="left" bgcolor="#ffffff" valign="middle">
                                                <table cellspacing="0" cellpadding="0" border="0" width="100%" style="min-width: 100%;" role="presentation">
                                                  <tr>
                                                    <th class="line-item-description" style="mso-line-height-rule: exactly; font-family: -apple-system,BlinkMacSystemFont,'Segoe UI',Arial,'Libre Franklin'; font-size: 16px; line-height: 26px; font-weight: 400; color: #4c5154; padding: 13px 6px 13px 0;" align="left" bgcolor="#ffffff" valign="top">
                                                      <p style="mso-line-height-rule: exactly; font-family: -apple-system,BlinkMacSystemFont,'Segoe UI',Arial,'Libre Franklin'; font-size: 16px; line-height: 26px; font-weight: 400; color: #4c5154; margin: 0;" align="left">
                                                        {{ ucfirst($prod->name) }}
                                                      </p>
                                                    </th>
                                                    <th class="right line-item-qty" width="1" style="mso-line-height-rule: exactly; white-space: nowrap; padding: 13px 0 13px 13px;" align="left" bgcolor="#ffffff" valign="top">
                                                      <p style="mso-line-height-rule: exactly; font-family: -apple-system,BlinkMacSystemFont,'Segoe UI',Arial,'Libre Franklin'; font-size: 16px; line-height: 26px; font-weight: 400; color: #4c5154; margin: 0;" align="right">
                                                        × {{ $key->quantity }}
                                                      </p>
                                                    </th>

                                                    <th class="right line-item-qty" width="1" style="mso-line-height-rule: exactly; white-space: nowrap; padding: 13px 0 13px 13px;" align="left" bgcolor="#ffffff" valign="top">
                                                      <p style="mso-line-height-rule: exactly; font-family: -apple-system,BlinkMacSystemFont,'Segoe UI',Arial,'Libre Franklin'; font-size: 16px; line-height: 26px; font-weight: 400; color: #4c5154; margin: 0;" align="right">
                                                         ₹{{ $prod->new_price }}
                                                      </p>
                                                    </th>
                                                  </tr>
                                                </table>
                                              </th>
                                            </tr>

                                            @php $sum = $sum + $key->price; @endphp
                                        @endforeach

                                        

                                      </table>
                                    </th>
                                  </tr>

                                    {{--  Total Charge	--}}
                                    <tr class="row-border-bottom">
                                      <th class="product-details-wrapper table-stack stack-column" style="mso-line-height-rule: exactly; padding: 20px 20px 20px 10px; border-color: #f5f5f5; border-style: solid; border-width: 1px 1px 1px 0;" align="left" bgcolor="#ffffff" valign="middle">
                                        <table cellspacing="0" cellpadding="0" border="0" width="100%" style="min-width: 100%;" role="presentation">
                                          <tr>
                                            <th class="line-item-description" style="mso-line-height-rule: exactly; font-family: -apple-system,BlinkMacSystemFont,'Segoe UI',Arial,'Libre Franklin'; font-size: 16px; line-height: 26px; font-weight: 400; color: #4c5154; padding: 13px 6px 13px 0;" align="left" bgcolor="#ffffff" valign="top">
                                              <p style="mso-line-height-rule: exactly; font-family: -apple-system,BlinkMacSystemFont,'Segoe UI',Arial,'Libre Franklin'; font-size: 16px; line-height: 26px; font-weight: 400; color: #4c5154; margin: 0;" align="left">
                                               Total
                                              </p>
                                            </th>
                                            <th class="right line-item-qty" width="1" style="mso-line-height-rule: exactly; white-space: nowrap; padding: 13px 0 13px 13px;" align="left" bgcolor="#ffffff" valign="top">
                                              <p style="mso-line-height-rule: exactly; font-family: -apple-system,BlinkMacSystemFont,'Segoe UI',Arial,'Libre Franklin'; font-size: 16px; line-height: 26px; font-weight: 400; color: #4c5154; margin: 0;" align="right">
                                                
                                              </p>
                                            </th>
  
                                            <th class="right line-item-qty" width="1" style="mso-line-height-rule: exactly; white-space: nowrap; padding: 13px 0 13px 13px;" align="left" bgcolor="#ffffff" valign="top">
                                                <p style="mso-line-height-rule: exactly; font-family: -apple-system,BlinkMacSystemFont,'Segoe UI',Arial,'Libre Franklin'; font-size: 16px; line-height: 26px; font-weight: 400; color: #4c5154; margin: 0;" align="right">
                                                  ₹{{$sum}}
                                                </p>
                                            </th>
                                          </tr>
                                        </table>
                                      </th>
                                    </tr>

                                    {{--  Shipping Charge	 --}}
                                    <tr class="row-border-bottom" style="width:100%;">
                                      <th class="product-details-wrapper table-stack stack-column" style="mso-line-height-rule: exactly; padding: 20px 20px 20px 10px; border-color: #f5f5f5; border-style: solid; border-width: 1px 1px 1px 0;" align="left" bgcolor="#ffffff" valign="middle">
                                        <table cellspacing="0" cellpadding="0" border="0" width="100%" style="min-width: 100%;" role="presentation">
                                          <tr>
                                            <th class="line-item-description" style="mso-line-height-rule: exactly; font-family: -apple-system,BlinkMacSystemFont,'Segoe UI',Arial,'Libre Franklin'; font-size: 16px; line-height: 26px; font-weight: 400; color: #4c5154; padding: 13px 6px 13px 0;" align="left" bgcolor="#ffffff" valign="top">
                                              <p style="mso-line-height-rule: exactly; font-family: -apple-system,BlinkMacSystemFont,'Segoe UI',Arial,'Libre Franklin'; font-size: 16px; line-height: 26px; font-weight: 400; color: #4c5154; margin: 0;" align="left">
                                                  Shipping Charge	
                                              </p>
                                            </th>
                                            <th class="right line-item-qty" width="1" style="mso-line-height-rule: exactly; white-space: nowrap; padding: 13px 0 13px 13px;" align="left" bgcolor="#ffffff" valign="top">
                                              <p style="mso-line-height-rule: exactly; font-family: -apple-system,BlinkMacSystemFont,'Segoe UI',Arial,'Libre Franklin'; font-size: 16px; line-height: 26px; font-weight: 400; color: #4c5154; margin: 0;" align="right">
                                                
                                              </p>
                                            </th>
                                            
                                            <th class="right line-item-qty" width="1" style="mso-line-height-rule: exactly; white-space: nowrap; padding: 13px 0 13px 13px;" align="left" bgcolor="#ffffff" valign="top">
                                                <p style="mso-line-height-rule: exactly; font-family: -apple-system,BlinkMacSystemFont,'Segoe UI',Arial,'Libre Franklin'; font-size: 16px; line-height: 26px; font-weight: 400; color: #4c5154; margin: 0;" align="right">
                                                  @if($order_detail->delivery_charge != '')   ₹{{$order_detail->delivery_charge}} @else ₹0 @endif
                                                </p>
                                            </th>
                                          </tr>
                                        </table>
                                      </th>
                                    </tr>

                                    {{--  Net Amount Payable --}}
                                    <tr class="row-border-bottom">
                                      <th class="product-details-wrapper table-stack stack-column" style="mso-line-height-rule: exactly; padding: 20px 20px 20px 10px; border-color: #f5f5f5; border-style: solid; border-width: 1px 1px 1px 0;" align="left" bgcolor="#ffffff" valign="middle">
                                        <table cellspacing="0" cellpadding="0" border="0" width="100%" style="min-width: 100%;" role="presentation">
                                          <tr>
                                            <th class="line-item-description" style="mso-line-height-rule: exactly; font-family: -apple-system,BlinkMacSystemFont,'Segoe UI',Arial,'Libre Franklin'; font-size: 16px; line-height: 26px; font-weight: 400; color: #4c5154; padding: 13px 6px 13px 0;" align="left" bgcolor="#ffffff" valign="top">
                                              <p style="mso-line-height-rule: exactly; font-family: -apple-system,BlinkMacSystemFont,'Segoe UI',Arial,'Libre Franklin'; font-size: 16px; line-height: 26px; font-weight: 400; color: #4c5154; margin: 0;" align="left">
                                                  Net Amount Payable	
                                              </p>
                                            </th>
                                            <th class="right line-item-qty" width="1" style="mso-line-height-rule: exactly; white-space: nowrap; padding: 13px 0 13px 13px;" align="left" bgcolor="#ffffff" valign="top">
                                              <p style="mso-line-height-rule: exactly; font-family: -apple-system,BlinkMacSystemFont,'Segoe UI',Arial,'Libre Franklin'; font-size: 16px; line-height: 26px; font-weight: 400; color: #4c5154; margin: 0;" align="right">
                                                &nbsp;
                                              </p>
                                            </th>
                                            
                                            <th class="right line-item-qty" width="1" style="mso-line-height-rule: exactly; white-space: nowrap; padding: 13px 0 13px 13px;" align="left" bgcolor="#ffffff" valign="top">
                                                <p style="mso-line-height-rule: exactly; font-family: -apple-system,BlinkMacSystemFont,'Segoe UI',Arial,'Libre Franklin'; font-size: 16px; line-height: 26px; font-weight: 400; color: #4c5154; margin: 0;" align="right">
                                                  ₹{{$sum+$order_detail->delivery_charge}}
                                                </p>
                                            </th>
                                          </tr>
                                        </table>
                                      </th>
                                    </tr>

                                </table>
                              </th>
                            </tr>

                            <!-- END SECTION: Products -->

                            <!-- BEGIN SECTION: Closing Text -->
                            <tr id="section-3251503" class="section closing_text">
                              <th data-key="3251503_closing_text" class="text" style="mso-line-height-rule: exactly; font-family: -apple-system,BlinkMacSystemFont,'Segoe UI',Arial,'Libre Franklin'; font-size: 16px; line-height: 26px; font-weight: 400; color: #4c5154; padding: 13px 52px 52px;" align="left" bgcolor="#ffffff">
                                Please send an email  to <a  style="color: #ababab; text-decoration: none !important; text-underline: none; word-wrap: break-word;">sportsgearind@gmail.com</a> if you have any questions.<br>
                                <br>
                                The Sports gear Team <br>
                                Copyright © 2021
                              </th>
                            </tr>

                            <!-- END SECTION: Closing Text -->
                          </table>
                        </td>
                      </tr>
                    </table>

                    <!-- END : SECTION : MAIN -->
                  </th>
                </tr>
              </table>
            </center>
          </th>
        </tr>
      </tbody>
    </table>

    <!-- END : CONTAINER -->

  </body>
</html>