@extends('front.include.main')
@section('content')
 <div id="content"> 
    <input type="hidden" id="previous_url" value="{{URL::previous()}}">
    <!-- Blog -->
    <section class="login-sec padding-top-30 padding-bottom-100">
      <div class="container">
        <div class="row">
        <div class="col-md-6"> 
         <h5>Login Or Register</h5>
            <div>
            <img src="{{URL::asset('image/sports.jpeg')}}" style="max-width:150px;margin-left:7%;">
            </div><br><br>
           <div>
             <a href="{{ route('fb_login') }}"> <button class="btn-round" style="background-color:#3b5998;border-radius:50px;width:50%;margin:3%;"><i class="fa fa-facebook">&nbsp;Facebook</i></button></a>
              </div>
              <div>
              <a href="{{ route('gmail_login') }}"> <button class="btn-round" style="background-color:#c32f10;border-radius:50px;width:50%;margin:3%;"><i class="fa fa-google-plus"></i>&nbsp;Gmail</button></a>
              </div>
          </div>
          <div class="col-md-6"> 
            <!-- Login Your Account -->
            <h5>Login Your Account</h5>
            <!-- FORM -->
            <form enctype="multipart/form-data" id="user_login">
              <ul class="row">
                <li class="col-sm-12">
                  <label>Useremail
                    <input type="text" class="form-control" name="username" placeholder="" required>
                  </label>
                </li>
                <li class="col-sm-12">
                  <label>Password
                    <input type="password" class="form-control" name="password" placeholder="" required>
                  </label>
                </li>
                
                <li class="col-sm-12" style="text-align:left;"> <a href="{{ route('password_forgot') }}" class="forget">Forgot your password?</a> </li>
                <li class="col-sm-6 text-left">
                  <button type="submit" class="btn-round">Login</button>
                </li>
                <li class="col-sm-6 text-left">
                  <a  href="{{  route('user_register')}}"  class="btn-round" style="float:right;">Register</a>
                </li>
              </ul>
            </form>
          </div>
      </div>
    </section>
    
    <!-- Clients img -->
  </div>
  <div id="snackbar"></div>
  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
  <script>
    //Register user
    $('#user_login').submit(function(e){
        e.preventDefault();
        var form = $('#user_login')[0];
        var data = new FormData(form);
        var baseurl=$("#base_url").val();
        var url = baseurl + "/auth_check";
        $.ajax({		            	
                    type: "POST",
                    url: baseurl+'/api/login',
                    enctype: 'multipart/form-data',
                    data: data,
                    processData: false,
                    contentType: false,
                    cache: false,                                       
                    success: function(data)
                    {
                      console.log(data);
                        if(data.status ==200)
                        {
                          $.ajax({
                                url: url,
                                type: 'POST',
                                data: {
                                    "_token": "{{ csrf_token() }}",
                                    "id": data.data.id,
                                },
                                success: function(response) {
                                    if (response.message == 'success') {
                                        var purl=$("#previous_url").val();
                                        var array = purl.split('/');
                                        var lastsegment = array[array.length-1];
                                        if(lastsegment=='register' || lastsegment=='login'){
                                          window.location=baseurl;
                                          
                                        }
                                        else{
                                          window.location=purl;
                                        }
                                    } 
                                    else {
                                       alert("else");
                                    }
                                }
                            });
                        }
                        else
                        {
                          var x = document.getElementById("snackbar");
                          $("#snackbar").text(data.message);
                          x.className = "show";
                          setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
                          }
                    }
                });
        });
      </script>
@endsection