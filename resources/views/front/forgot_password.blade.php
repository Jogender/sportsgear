@extends('front.include.main')
@section('content')
<div id="content">
    <!-- Linking -->
    <input type="hidden" value="{{url('/')}}" id="base_url">
    <input type="hidden" id="previous_url" value="{{URL::previous()}}">
    <!-- Blog -->
    <section class="login-sec padding-top-30 padding-bottom-100">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                </div>
                <div class="col-md-4">
                    <!-- Login Your Account -->
                    <h5 style="text-align:center;">Forgot Password</h5>
                    {{-- <!-- FORM -->
                    <div style="text-align:center;">
                    <img src="{{URL::asset('frontend/main-files/html/images/SportsLogo.png')}}" style="max-width:150px;margin-left:7%;">
                    </div> --}}
                    <form enctype="multipart/form-data" id="check_email">
                        <ul class="row">
                            <li class="col-sm-12">
                                <label>Email address
                                    <input type="text" class="form-control" id="email" name="email" placeholder="" required>
                                </label>
                            </li>
                            <li class="col-sm-12 text-left">
                                <button type="submit" style="width:100%;" class="btn-round">Submit</button><br><br>
                                <a href="{{route('user_login')}}" style="float:right;color:blue;"><b>Back To Login</b></a>
                            </li>
                        </ul>
                    </form>
                </div>
                <div class="col-md-4">
                </div>
            </div>
    </section>


    <!-- Newslatter -->
</div>
<div id="snackbar"></div>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
<script>

    //Register user
    $('#check_email').submit(function (e) {
        e.preventDefault();

        var form = $('#check_email')[0];
        var data = new FormData(form);
        var baseurl = $("#base_url").val();
        var url = baseurl + "/auth_check";
        
        $.ajax({
            type: "POST",
            url: baseurl + '/api/check_email',
            enctype: 'multipart/form-data',
            data: data,
            processData: false,
            contentType: false,
            cache: false,
            success: function (data) {
                
                if (data.status == 200) {
                    $("#snackbar").text(data.message);                  
                } else {
                    $("#snackbar").text(data.message);  
                }

                var x = document.getElementById("snackbar");
                x.className = "show";
                setTimeout(function () {
                    x.className = x.className.replace("show", "");
                }, 3000,$("#email").val(''));

            }
        });
    });
</script>
@endsection