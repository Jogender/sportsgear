@extends('front.include.main')
@section('content')
<!----Content------>
<div id="content"> 
    
    <!-- Ship Process -->
    <div class="ship-process padding-top-30 padding-bottom-30">
      <div class="container">
        <ul class="row">
          
          <!-- Step 1 -->
          <li class="col-sm-3 current">
            <div class="media-left"> <i class="flaticon-shopping"></i> </div>
            <div class="media-body"> <span>Step 1</span>
              <h6>Shopping Cart</h6>
            </div>
          </li>
          
          <!-- Step 2 -->
          <li class="col-sm-3">
            <div class="media-left"> <i class="flaticon-business"></i> </div>
            <div class="media-body"> <span>Step 2</span>
              <h6>Payment Methods</h6>
            </div>
          </li>
          
          <!-- Step 3 -->
          <li class="col-sm-3">
            <div class="media-left"> <i class="flaticon-delivery-truck"></i> </div>
            <div class="media-body"> <span>Step 3</span>
              <h6>Delivery Methods</h6>
            </div>
          </li>
          
          <!-- Step 4 -->
          <li class="col-sm-3">
            <div class="media-left"> <i class="fa fa-check"></i> </div>
            <div class="media-body"> <span>Step 4</span>
              <h6>Confirmation</h6>
            </div>
          </li>
        </ul>
      </div>
    </div>
    

    <form method="POST" action="{{route('checkout')}}" onsubmit="return check_no()">
      @csrf
      <!-- Shopping Cart -->
      <section class="shopping-cart padding-bottom-60">
        <div class="container">
          <table class="table">
            <thead>
              <tr>
                <th>Items</th>
                <th class="text-center">Price</th>
                <th class="text-center">Quantity</th>
                <th class="text-center">Total Price </th>
                <th class="text-center" colspan="2">GST(Included in price)</th>
                <th>&nbsp; </th>
              </tr>
            </thead>
            <tbody class="products">
              @php $total=0; @endphp
              <!-- Item Cart -->
              @foreach($data as $d)
              @php $total=$total+$d->price*$d->quantity; @endphp
              @php $images=json_decode($d->image,true);  @endphp
              <tr id="data{{$d->id}}">
                <td>
                    <div class="media">
                      <div class="media-left"> <a href="#."> <img class="img-responsive" src="{{ URL::asset('image/product/'.$images[0]) }}" alt="" > </a> </div>
                      <div class="media-body">
                        <p>{{ ucfirst($d->name) }}</p>
                      </div>
                    </div>
                </td>
                <td class="text-center padding-top-60 product_price"> {{ $d->price}} </td>
                <td class="text-center">
                  <div class="quinty padding-top-20">
                    <input type="hidden" value="{{ $d->id }}" name="id[]">
                    <input type="number" min="1" value="{{ $d->quantity }}" id="{{ $d->id }}" class="quantity" name="quantity[]">
                  </div></td>
                <td class="text-center padding-top-60 total_product_price">{{ $d->quantity * $d->price }}</td>
                <td class="text-center padding-top-60" > {{$d->tax}}% </td>
                <td class="text-center padding-top-60" > {{ $d->price*$d->tax/100 }} </td>

                <td class="text-center padding-top-60"><a href="#." class="remove" onclick="remove_product({{ $d->id }})"><i class="fa fa-2x fa-trash"></i></a></td>
              </tr>
              @endforeach
            </tbody>
          </table>
          
          <!-- Promotion -->
          <div class="promo">
            <div class="coupen">
            </div>
            
            <!-- Grand total -->
            <div class="g-totel">
              <h5>Grand total: <span id="grand_total">{{$total  }}</span></h5>
            </div>
          </div>
          
          <!-- Button -->
          <div class="pro-btn"> 
              <a href="{{ route('product_listing') }}" class="btn-round btn-light">Continue Shopping</a> 
              <button class="btn btn-round" type="submit" @if($total<1) disabled @endif>Go Payment Methods</button> 
          </div>
        </div>
      </section>
    </form>
    <input type="hidden" value="{{Auth::User()->phone}}" id="phone">
    

    
    <!-- Newslatter -->
  </div>
<!-----End Content-------->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script>
  function  check_no(){
      var phone=$('#phone').val();
      if (phone=='') {
        if(confirm("Please verify your phone number first to place order!!")){
            window.location='{{route('user_profile')}}';
            return false;
        }
        else{
            return false;
        }
      }
  }
$('.quantity').on('change', function() {
    var price=0;
    var quantity=this.value;
    var total_price=0;
    $('#data'+this.id).find(".product_price").each(function(){
      price=$(this).html();
    });

    $('#data'+this.id).find(".total_product_price").each(function(){
      $(this).html(parseInt(price)*parseInt(quantity));
    });

    $('.table tr').find(".total_product_price").each(function(){
      total_price=parseInt(total_price) + parseInt($(this).html());
      // console.log(total_price);
      $('#grand_total').text(total_price);
    });
});

function remove_product(id){
  var baseurl=$("#base_url").val();
      $.ajax({		            	
        url: baseurl+'/api/remove_product',
        type: 'POST',
        data: {
            "_token": "{{ csrf_token() }}",
            "id":id,
            "userid":{{ Auth::user()->id }}
        },                                   
        success: function(data)
        {
            if(data.status ==200)
            {
              $('#data'+id).remove();
              var total_price=0;
              $('.table tr').find(".total_product_price").each(function(){
                total_price=parseInt(total_price) + parseInt($(this).html());
                $('#grand_total').text(total_price);
              });
              alert("successfully remove from cart");
            }
            else
            {
              alert("error");
            }
        }
    });
}
</script>
@endsection