@extends('front.include.main')
@section('content')
<!--Snackbar CSS --->
<style>
    #snackbar {
      visibility: hidden;
      min-width: 250px;
      margin-left: -125px;
      background-color: #333;
      color: #fff;
      text-align: center;
      border-radius: 2px;
      padding: 16px;
      position: fixed;
      z-index: 1;
      left: 50%;
      bottom: 30px;
      font-size: 17px;
    }
    
    #snackbar.show {
      visibility: visible;
      -webkit-animation: fadein 0.5s, fadeout 0.5s 2.5s;
      animation: fadein 0.5s, fadeout 0.5s 2.5s;
    }
    
    @-webkit-keyframes fadein {
      from {bottom: 0; opacity: 0;} 
      to {bottom: 30px; opacity: 1;}
    }
    
    @keyframes fadein {
      from {bottom: 0; opacity: 0;}
      to {bottom: 30px; opacity: 1;}
    }
    
    @-webkit-keyframes fadeout {
      from {bottom: 30px; opacity: 1;} 
      to {bottom: 0; opacity: 0;}
    }
    
    @keyframes fadeout {
      from {bottom: 30px; opacity: 1;}
      to {bottom: 0; opacity: 0;}
    }
    </style>
<div id="content">
    <section class="padding-bottom-60">
        <div class="container">
            <!-- Payout Method -->
            <div class="pay-method">
                <div class="row">
                    <div class="col-md-6">
                        <!-- Your information -->
                        <div class="heading">
                            <h2>User information</h2>
                            <hr>
                        </div>
                        <form>
                            <div class="row">
                                <!-- Name -->
                                <div class="col-sm-6">
                                    <label> First name
                                        <input class="form-control" type="text" id="firstname" name="firstname"
                                            value="{{Auth::user()->name}}">
                                    </label>
                                </div>
                                <!-- Number -->
                                <div class="col-sm-6">
                                    <label> Last Name
                                        <input class="form-control" type="text" id="lastname" name="lastname"
                                            value="{{Auth::user()->lastname}}">
                                    </label>
                                </div>
                                <input type="hidden" value="{{ Auth::user()->mobile_verify }}" id="mobile_verify">
                                <!-- Phone -->
                                <div class="col-sm-6">
                                    <label> Phone
                                        <input class="form-control" type="text" id="phone" name="phone"
                                            value="{{Auth::user()->phone}}">
                                        <span id="mobile_error" style="color:red;display:none;">Please enter valid mobile number.</span>
                                        @if(Auth::user()->mobile_verify!=1)<span><i class="fa fa-times-circle" style="color:red;"
                                                aria-hidden="true"></i>&nbsp;Not Verified</span>
                                        @else
                                        <span><i class="fa fa-check" style="color:green;"
                                            aria-hidden="true"></i>&nbsp;Verified</span>
                                        @endif
                                    </label>
                                </div>
                                <!-- Number -->
                                <div class="col-sm-6">
                                    <label> Email
                                        <input class="form-control" type="email" name="email"
                                            value="{{Auth::user()->email}}" readonly>
                                        <span><i class="fa fa-check" style="color:green;"
                                                aria-hidden="true"></i>&nbsp;Verified</span>
                                    </label>
                                </div>
                            </div>
                            @if(Auth::user()->mobile_verify!=1)
                            <button type="button" class="btn-round btn-light"
                                style="float:left;border-color:#f74b16;"  onclick="mobile_verification()">Update
                                Profile</button>
                            @else
                            <button type="button" class="btn-round btn-light"
                                style="float:left;border-color:#f74b16;"   onclick="update_profile()" >Update
                                Profile</button>
                            @endif

                        </form>
                    </div>
                    <div class="col-md-6">
                        <!-- Your information -->
                        <div class="heading">
                            <h2>Change Password</h2>
                            <hr>
                        </div>
                        <form method="post" action="{{route('change_password')}}" enctype="multipart/form-data"
                            class="uk-form-stacked task-form">
                            @csrf
                            <div class="row">
                                <!-- Name -->
                                <div class="col-sm-12">
                                    <label> Old Password
                                        <input class="form-control" type="password" name="old_password" value="">
                                    </label>
                                </div>

                            </div>

                            <div class="row">
                                <!-- Number -->
                                <div class="col-sm-12">
                                    <label> New Password
                                        <input class="form-control" type="password" name="new_password" value="">
                                    </label>
                                </div>
                            </div>
                            <div class="row">
                                <button type="submit" class="btn-round btn-light" style="border-color:#f74b16;">Change Password</button>
                            </div>
                        </form>
                    </div>
                    <!-- Select Your Transportation -->
                </div>
            </div>
        </div>
    </section>
</div>
<div class="bs-example">
    <div id="otpModal" class="modal fade" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                <form>
                    <div class="modal-header">
                        <h5 class="modal-title">Mobile Verification</h5>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-6">
                                <label for="oldpassword">OTP</label>
                                <input type="text" id="otp" name="name" class="form-control" required />
                                <span id="otp_error" style="color:red;display:none;">Please enter valid otp</span>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" style="background-color:#372e41 !important;"
                            data-dismiss="modal">Cancel</button>
                        <button type="button" class="btn-round btn-light" onclick="update_profile()">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!----------End otp--------------->
<!-----------End Modal ----------->
<!----- Edit Modal -------->
<!-----------End Modal ----------->
<div class="bs-example">
    <div id="passwordModal" class="modal fade" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                <form id="password" onsubmit="form_submit()">
                    <div class="modal-header">
                        <h5 class="modal-title">Change Password</h5>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-6">
                                <label for="oldpassword">Old Password</label>
                                <input type="text" id="oldpassword" name="oldpassword" class="form-control" required />
                            </div>
                            <div class="col-md-6">
                                <label for="newpassword">New Password</label>
                                <input type="text" id="newpassword" name="newpassword" class="form-control" required />
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" style="background-color:#372e41 !important;"
                            data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn-round btn-light">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div id="snackbar"></div>
@endsection
<!---------Otp Modal-------------->
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js">  </script>
<script>
   function mobile_verification() {
    var phone=$("#phone").val();
    var phone_length=phone.length;

    if(phone_length==10){
        $.ajax({
                url: "{{route('mobile_verify')}}",
                type: 'POST',
                data: {
                    "_token": "{{ csrf_token() }}",
                    "phone": $("#phone").val(),
                },
                success: function (response) {
                    if (response.message == 'success') {
                        $("#mobile_error").css('display','none');
                        $('#otpModal').modal('toggle');
                    } else {
                        window.location.reload();
                    }
                }
            });
      }
      else{
        $("#mobile_error").css('display','block');
      }
    }  

    function update_profile() {
        var mobile_verify=$("#mobile_verify").val();
        if(mobile_verify==0){
            var otp=$("#otp").val();
            if(otp!=null){
                $.ajax({
                url: "{{route('update_profile')}}",
                type: 'POST',
                data: {
                    "_token": "{{ csrf_token() }}",
                    "firstname": $("#firstname").val(),
                    "lastname": $("#lastname").val(),
                    "otp":$("#otp").val(),
                },
                success: function (response) {
                    if (response.message == 'success') {
                        $('#otpModal').modal('hide');
                        $("#otp_error").css('display','none');
                        var x = document.getElementById("snackbar");
                        $("#snackbar").text("Mobile number verify successfull");
                        x.className = "show";
                        setTimeout(function(){ x.className = x.className.replace("show", "");window.location.reload(); }, 3000);
                    } else {
                        $('#otpModal').modal('show');
                        $("#otp_error").css('display','block');
                    }
                }
            });
            }
            else{
                $("#otp_error").css('display','block');
            }
        }
        else{
            $.ajax({
            url: "{{route('update_profile')}}",
            type: 'POST',
            data: {
                "_token": "{{ csrf_token() }}",
                "firstname": $("#firstname").val(),
                "lastname": $("#lastname").val(),
            },
            success: function (response) {
                if (response.message == 'success') {
                    var x = document.getElementById("snackbar");
                    $("#snackbar").text("Profile update successfully");
                    x.className = "show";
                    setTimeout(function(){ x.className = x.className.replace("show", "");window.location.reload(); }, 3000);
                } else {
                    $('#otpModal').modal('show');
                    $("#otp_error").css('display','block');
                }
            }
        });
        }
      
   
        return false;
    }  
</script>






