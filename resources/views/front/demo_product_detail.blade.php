<!doctype html>
<html class="no-js" lang="en">
<head>
@include('front.include.css')
</head>
<body>
<style>
    .grayscale {
      -webkit-filter: brightness(1.10) grayscale(100%) contrast(90%);
      -moz-filter: brightness(1.10) grayscale(100%) contrast(90%);
      filter: brightness(1.10) grayscale(100%); 
    }
 /* these styles are for the demo, but are not required for the plugin */
		.zoom {
			display:inline-block;
			position: relative;
		}
		
		/* magnifying glass icon */
		.zoom:after {
			content:'';
			display:block; 
			width:33px; 
			height:33px; 
			position:absolute; 
			top:0;
			right:0;
			background:url(icon.png);
		}

		.zoom img {
			display: block;
		}

		.zoom img::selection { background-color: transparent; }

		#ex2 img:hover { cursor: url(grab.cur), default; }
		#ex2 img:active { cursor: url(grabbed.cur), default; }
    
</style>
@include('front.include.header')

  <div class="linking">
    <div class="container">
      <ol class="breadcrumb">
        <li><a href="#">Home</a></li>
        <li><a href="#">User</a></li>
        <li class="active">Profile</li>
      </ol>
    </div>
  </div>

<div id="content"> 
    <!-- Products -->
    <section class="padding-top-40 padding-bottom-60">
      <div class="container">
        <div class="row"> 
          <!-- Products -->
          <div class="col-md-12">
            <div class="product-detail">
              <div class="product">
                <div class="row"> 

                  
                  <!-- Slider Thumb -->
                  <div class="col-xs-5">
                    <article class="slider-item on-nav">
                      <div class="thumb-slider">
                        <ul class="slides">
                         @php $image=json_decode($data->image); @endphp
                         @foreach($image as $i)
                          <li data-thumb="{{URL::asset('image/product/'.$i)}}" ><span class='zoom ex1'><img src="{{URL::asset('image/product/'.$i)}}"></span></li>
                          @endforeach
                        </ul>
                      </div>
                    </article>
                  </div>
                  
                    
                  
                  <!-- Item Content -->
                  <div class="col-xs-7"> 
                    <h5>{{ $data->name }}</h5>
                    <p class="rev"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <span class="margin-left-10">5 Review(s)</span></p>
                    <div class="row">
                      <div class="col-sm-6">                
                        <span>M.R.P: ₹<del>{{$data->old_price}}</del> </span>   </br>                            
                        <span class="price">Deal Price: ₹<span id="price">{{$data->new_price}}</span></span>  
                        <input type="hidden" value="{{$data->new_price}}" id="real_price">                      
                      </div>
                      <div class="col-sm-6">                        
                        @if($data->quantity>0)
                          <p>Availability: <span class="in-stock">In stock</span></p>
                        @else
                          <p>Availability: <span class="in-stock" style="color:red;">Out of stock</span></p>
                        @endif
                      </div>
                    </div>

                    <input type="hidden" value="100" name="product_price" id="product_price">
                    <input type="hidden" value="weight" name="product_type" id="product_type">

                 

                   

                    <!-- Compare Wishlist -->
                    {{-- <ul class="cmp-list">
                      <li><a href="#."><i class="fa fa-heart"></i> Add to Wishlist</a></li>
                      <li><a href="#."><i class="fa fa-navicon"></i> Add to Compare</a></li>
                      <li><a href="#."><i class="fa fa-envelope"></i> Email to a friend</a></li>
                    </ul> --}}

                 

                  {{-- Varient --}}
                    @if($data->variant_status == 'yes')
                      @php $variantDetail=json_decode($data->variant);  $variantLoop=0; @endphp
                        @foreach($variantDetail as $key => $value)</br>
                          @foreach($value as $k=>$v)                                                     
                              <div class="row">
                                <div class="col-xs-10">
                                  @foreach($v as $l=>$w)
                                      @if($l == 0 )  <h5>*{{$k}}</h5> @endif 
                                      <button class="varientButton" onclick=change_varient({{$w->price}},'{{$w->pricechange}}')>{{$w->value}}</button> &nbsp;
                                  @endforeach
                                </div>                          
                              </div>                           
                          @endforeach
                        @endforeach
                      </br>
                    @endif

                  {{-- ENd varient --}}

                  @if($data->quantity)
                   <!-- Quinty -->
                  <div class="quinty">
                    <input type="number" value="01" min="1">
                  </div>
                  <a href="#." class="btn-round"><i class="icon-basket-loaded margin-right-5"></i> Add to Cart</a> 
                  @endif
                  </div>
                </div>
              </div>
              
              <!-- Details Tab Section-->
              <div class="item-tabs-sec"> 
                
                <!-- Nav tabs -->
                <ul class="nav" role="tablist">
                  <li role="presentation" class="active"><a href="#pro-detil"  role="tab" data-toggle="tab">Product Details</a></li>
                  <li role="presentation"><a href="#cus-rev"  role="tab" data-toggle="tab">Customer Reviews</a></li>
                  <li role="presentation"><a href="#ship" role="tab" data-toggle="tab">Shipping & Payment</a></li>
                </ul>
                
                <!-- Tab panes -->
                <div class="tab-content">
                  <div role="tabpanel" class="tab-pane fade in active" id="pro-detil"> 
                    <!-- List Details --> 
                      {!! $data->short_description !!}
                  </div>


                  <div role="tabpanel" class="tab-pane fade" id="cus-rev">
                    <div class="row">
                    <div class="col-md-6">  
                      <p class="rev"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <span class="margin-left-10">5 Review(s)</span></p>
                    </div>
                    <div class="col-md-6">
                      <a href="#." class="btn-round" style="float:right;background-color:#f74b16;"><i class="icon-basket-loaded margin-right-5"></i>Add Review</a>
                    </div>
                    <div class=row>
                      <div class="col-md-12">
                        <h6><b>Jogender</b></h6>
                        <p>This phone is the best low Budget phone i have ever seen...I dont usually play games though it is a budget gaming phone...camera is avg , storage is less but can be increased ...super smooth performance. I am a student and this phone is very good for me....</p>
                      </div> 
                      <div class="col-md-12">
                        <h6><b>Jogender</b></h6>
                        <p>This phone is the best low Budget phone i have ever seen...I dont usually play games though it is a budget gaming phone...camera is avg , storage is less but can be increased ...super smooth performance. I am a student and this phone is very good for me....</p>
                      </div> 
                      <div class="col-md-12">
                        <h6><b>Jogender</b></h6>
                        <p>This phone is the best low Budget phone i have ever seen...I dont usually play games though it is a budget gaming phone...camera is avg , storage is less but can be increased ...super smooth performance. I am a student and this phone is very good for me....</p>
                      </div>  
                    <div>
                  </div>
                  <div role="tabpanel" class="tab-pane fade" id="ship"></div>
                </div>
              </div>
            </div>
            
            <!-- Related Products -->
            <section class="padding-top-30 padding-bottom-0"> 
              <!-- heading -->
              <div class="heading">
                <h2>Others Products</h2>
                <hr>
              </div>
              <!-- Items Slider -->
              <div class="item-slide-4 with-nav"> 
                <!-- Product -->
                @foreach($other_product as $p)
                <div class="product">
                  <article> 
                    @php $img=json_decode($p->image);  @endphp
                    @php if(count($img) > 0){
                    $images=$img[0];
                    }
                    else {
                    $images='';
                    }
                    @endphp
                    <img class="img-responsive" src="{{URL::asset('image/product/'.$images)}}" alt="" > 
                    <!-- Content --> 
                    <span class="tag"></span> <a href="#." class="tittle">{{ $p->name }}</a> 
                    <!-- Reviews -->
                    <p class="rev"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <span class="margin-left-10">5 Review(s)</span></p>
                    <div class="price">$350.00 </div>
                    <a href="#." class="cart-btn"><i class="icon-basket-loaded"></i></a> </article>
                </div>
                @endforeach
              </div>
            </section>
          </div>
        </div>
      </div>
    </section>
    
    @include('front.include.footer')
    
    

  </div>
  <input type="hidden" value="{{ $data->variant }}" name="varient" id="varient">
  <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js">  </script>

<!-- JavaScripts --> 
<script src="{{URL::asset('frontend/main-files/html/js/vendors/jquery/jquery.min.js')}}"></script> 
<script src="{{URL::asset('frontend/main-files/html/js/vendors/wow.min.js')}}"></script> 
<script src="{{URL::asset('frontend/main-files/html/js/vendors/bootstrap.min.js')}}"></script> 
<script src="{{URL::asset('frontend/main-files/html/js/vendors/own-menu.js')}}"></script> 
<script src="{{URL::asset('frontend/main-files/html/js/vendors/jquery.sticky.js')}}"></script> 
<script src="{{URL::asset('frontend/main-files/html/js/vendors/owl.carousel.min.js')}}"></script> 
<!-- SLIDER REVOLUTION 4.x SCRIPTS  --> 
<script type="text/javascript" src="{{URL::asset('frontend/main-files/html/rs-plugin/js/jquery.tp.t.min.js')}}"></script> 
<script type="text/javascript" src="{{URL::asset('frontend/main-files/html/rs-plugin/js/jquery.tp.min.js')}}"></script> 
<script src="{{URL::asset('frontend/main-files/html/js/main.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('frontend/main-files/html/rs-plugin/js/jquery.tp.t.min.js')}}"></script> 
<script type="text/javascript" src="{{URL::asset('frontend/main-files/html/rs-plugin/js/jquery.tp.min.js')}}"></script> 

<!-- <script src='http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js'></script> -->
	<script src='{{ asset('zoom-master/jquery.zoom.js')}}'></script>
	<script>
		$(document).ready(function(){
			$('.ex1').zoom();
			$('#ex2').zoom({ on:'grab' });
			$('#ex3').zoom({ on:'click' });			 
			$('#ex4').zoom({ on:'toggle' });
		});
  
  function change_varient(price,pricechange){
    if (pricechange=='on') {
      $('#price').text(parseInt($('#real_price').val()) + parseInt(price));
    }
  }

  function search_product(){
   var keyword=$("#search").val();
   if(keyword.length > 2){
    var category=$("#category").val();
    $.ajax({		            	
          url: "{{ route('product_search') }}",
          type: 'POST',
          data: {
              "_token": "{{ csrf_token() }}",
              "category":$("#category").val(),
              "keyword":keyword,
          },                                   
          success: function(data)
          {
            console.log(data);
              if(data.status ==200)
              {
                $('#searchList').fadeIn();  
                $('#searchList').html(data.ul);
              }
              else
              {
                
                $('#searchList').html(data.ul);
                setTimeout(function(){ $('#searchList').empty(); }, 3000);
              }
          }
      });
   }
  }
</script>