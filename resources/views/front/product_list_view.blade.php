@extends('front.include.main')
@section('content')
<style>
    .text {
   overflow: hidden;
   text-overflow: ellipsis;
   display: -webkit-box;
   -webkit-line-clamp: 2; /* number of lines to show */
   -webkit-box-orient: vertical;
}
</style>
<style type="text/css">
  		.ajax-load{
  			background: #e1e1e1;
		    padding: 10px 0px;
		    width: 100%;
  		}
  	</style>
<meta name="csrf-token" content="{{ csrf_token() }}" />

  <!-- Linking -->
 
  <!-- Content -->
  <div id="content"> 
    
    <!-- Products -->
    <section class="padding-top-40 padding-bottom-60">
      <div class="container">
        <div class="row"> 
          
          <!-- Shop Side Bar -->
          <div class="col-md-3">
            <div class="shop-side-bar"> 
              
              <!-- Categories -->
              <h6>Categories</h6>
              <div class="checkbox checkbox-primary">
                <ul>
                  @foreach($category as $c)
                  <li>
                    <input name="cat" id="cate1{{ $c->id }}" class="styled" type="checkbox" value="{{ $c->id }}" onclick="filter_product()">
                    <label for="cate1{{ $c->id }}">{{ ucfirst($c->name) }}</label>
                  </li>
                  @endforeach
                </ul>
              </div>
              
              <!-- Categories -->
              <h6>Price</h6>
              <!-- PRICE -->
              <div class="cost-price-content">
                <div id="price-range" class="price-range" onchange="filter_product()"></div>
                <span id="price-min" class="price-min">20</span> <span id="price-max" class="price-max">80</span> <a href="#." class="btn-round" >Filter</a> </div>
              
              <!-- Featured Brands -->
              <h6>Featured Brands</h6>
              <div class="checkbox checkbox-primary">
                <ul>
                  @foreach($brand as $key=>$b)
                  <li>
                    <input id="brand1{{ $key }}" class="styled" type="checkbox" name="brand" value="{{ $b->id }}"
                      onclick="filter_product()">
                    <label for="brand1{{ $key }}">{{ $b->name }}<span>{{ ($b->count) }}</span> </label>
                  </li>
                  @endforeach
                </ul>
              </div>
              
            </div>
          </div>
          
          <!-- Products -->
          <div class="col-md-9"> 
            
            <!-- Short List -->
            <div class="short-lst">
              <h2>All Product</h2>
              <ul>
                <!-- Short List -->
                <li>
                  <p>Showing Results</p>
                </li>
                <li>
                    <select class="selectpicker" id="price_type">
                      <option value="low">Low to High </option>
                      <option value="high">High to Low </option>
                    </select>
                  </li>
                  
                  <!-- Grid Layer -->
                  <li class="grid-layer"> <a href="{{ route('product_listing',['type'=>'list']) }}"><i class="fa fa-list margin-right-10"></i></a> <a href="{{ route('product_listing',['type'=>'grid']) }}" class="active"><i class="fa fa-th"></i></a> </li>
                   </ul>
            </div>
            
            <!-- Items -->
            <div class="col-list"> 
              <!-- Product -->
              <div class="product" id="product_list">
                @foreach ($products as $product)
                @php 
                 $img=json_decode($product->image,true);
                  if(count($img) > 0){
                  $images=$img[0];
                  }
                  else {
                  $images='';
                  }
                @endphp
               
                <article>                
                  <!-- Product img -->
                  <div class="media-left">
                    <div class="item-img"> <img class="img-responsive inner-img" src="{{ URL::asset('image/product/'.$images) }}" alt="" >  </div>
                  </div>                  
                  <!-- Content -->
                  <div class="media-body">
                    <div class="row">                       
                      <!-- Content Left -->
                      <div class="col-sm-7"> <span class="tag"></span> <a href="#." class="tittle">{{$product->name }}</a> 
                        <!-- Reviews -->
                        <p class="rev">
                          @php $rating=$product->star_rating; @endphp
                          @for ($i =0 ; $i < 5 ; $i++)
                            <i class=" @if($i < $rating) fa fa-star @else fa fa-star-o @endif"></i>
                          @endfor                
                          <span class="margin-left-10">{{  $product->review_count  }}  Review(s)</span>
                      </p>
                          <span class="margin-left-10">
                            {{  $product->review_count  }}  Review(s)</span></p>
                        <ul class="bullet-round-list">
                            <div class="text">
                            {!! $product->short_description !!}
                            </div>
                        </ul>
                      </div>                      
                      <!-- Content Right -->
                      <div class="col-sm-5 text-center"> 
                        <div class="position-center-center">
                            @if ($product->variant_status=='yes')
                            @php 
                            $variant=json_decode($product->variant);
                            @endphp
                            @foreach ($variant as $key => $value)
                            @foreach($value as $v=>$w)
                              @foreach($w as $k=>$p)
                                @if($k==0)
                                @php $price=$product->new_price; $quantity=$p->quantity; @endphp
                                @endif
                                @endforeach
                              @endforeach
                            @endforeach
                            <div class="price"><i class="fa fa-inr" aria-hidden="true"></i>{{ $price }}</div>
                            @else
                            @php $quantity=$product->quantity; @endphp  
                              <div class="price"><i class="fa fa-inr" aria-hidden="true"></i>{{ $product->new_price }}</div>
                            @endif<br>
                            @if($quantity!='0')
                            <p>Availability: <span class="in-stock">In stock</span></p>
                            @else
                            <p>Availability: <span class="in-stock" style="color:red;">Out of stock</span></p>
                            @endif
                      </div>
                    </div>
                  </div>
                </article>
          
                @endforeach
              </div>
              {{$products->links()}}
            </div>
          </div>
        </div>
      </div>
    </section>
    
    

    
  </div>
  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script>
  function filter_product(){
    var cat_array = [];
    var brand_array = [];
    $('input[name="cat"]:checked').each(function() {
       cat_array.push($(this).val());
    });

    $('input[name="brand"]:checked').each(function() {
      brand_array.push($(this).val());
    });
    
    var price_min=$('#price-min').text();
    var price_max=$('#price-max').text();

    //remove string from price
    min_price= price_min.replace('₹','');
    max_price= price_max.replace('₹','');


    $.ajax({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: "{{route('filter_product')}}",
        type: 'POST',
        enctype: 'multipart/form-data',
        data: {
            category: cat_array,
            brand: brand_array,
            min_price:parseInt(min_price),
            max_price:parseInt(max_price),
            price_type:$("#price_type").val(),
            type:'list',
        },
        dataType: 'json',
        success: function (response) {
          if (response.message == 'success') {
            $('#product_list').empty();
            $('#product_list').html(response.data);
              
            } else {
              window.location.reload();
            }
        }
  });

  }
</script>
@endsection



