@extends('front.include.main')
@section('content')
<style>
  #loader {  
    position: fixed;  
    left: 0px;  
    top: 0px;  
    width: 100%;  
    height: 100%;  
    z-index: 9999;  
    background: url('image/loader_ajax.gif') 50% 50% no-repeat rgb(249,249,249);  
}  
</style>
<div id="loader" style='display: none;'></div> 
 <div id="content"> 
    <!-- Linking -->    
    <!-- Blog -->
    <section class="login-sec padding-top-30 padding-bottom-100">
      <div class="container">  
       <div class="row">
         <div class="col-md-6"> 
         <h5>Login Or Register</h5>
            <div>
            <img src="{{URL::asset('image/sports.jpeg')}}" style="max-width:150px;margin-left:7%;">
            </div><br><br>
           <div>
             <a href="{{ route('fb_login') }}"> <button class="btn-round" style="background-color:#3b5998;border-radius:50px;width:50%;margin:3%;"><i class="fa fa-facebook">&nbsp;Facebook</i></button></a>
              </div>
              <div>
              <a href="{{ route('gmail_login') }}"> <button class="btn-round" style="background-color:#c32f10;border-radius:50px;width:50%;margin:3%;"><i class="fa fa-google-plus"></i>&nbsp;Gmail</button></a>
              </div>
          </div>
          <!-- Don’t have an Account? Register now -->
          <div class="col-md-6">
            <h5>Register Here</h5>
            <input type="hidden" value="{{url('/')}}" id="base_url">
            <input type="hidden" id="previous_url" value="{{URL::previous()}}">
            <!-- FORM -->
            <form enctype="multipart/form-data" id="add_user">
              <ul class="row">
                <li class="col-sm-12">
                  <label>Username
                    <input type="text" class="form-control" name="username" placeholder="" autocomplete="off" required>
                  </label>
                </li>
                <li class="col-sm-12">
                  <label>Email
                    <input type="email" class="form-control regsiter_plus" name="email" placeholder="" id="email" autocomplete="off" required>
                    <span class="email_error" class="form-control" style="display:none;color:#f74b16;">Enter valid email address</span>
                  </label>
                </li>
                <li class="col-sm-12">
                  <label>Password
                    <input type="password" class="form-control" name="password" placeholder="" autocomplete="off" required="">
                  </label>
                </li>
                <li class="col-sm-12 text-left">
                  <button type="submit" class="btn-round" id="submit">Register</button>
                </li>
              </ul>
            </form>
          </div>
        </div>
      </div>
    </section>
    
  </div>

  <!----- Otp match Modal -------->
  <div class="bs-example">
    <div id="myModal" class="modal fade" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Otp Confirmation</h5>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <p>Please submit you otp which send recently on your email!</p>
                    <div class="row">
                      <div class="col-md-6">
                        <label for="email_otp">OTP</label>
                        <input type="text"  name="email_otp" id="email_otp" class="form-control">
                        <input type="hidden" value="" name="email" id="user_email" class="form-control">
                       </div>
                      
                     </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" style="background-color:#372e41 !important;" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-primary" style="background-color:#f74b16 !important;" OnClick="verify_otp()">Submit Otp</button>
                </div>
            </div>
        </div>
    </div>
  </div>
  <!-----------End Modal ----------->



  <!--- Snackbar ------->
  <div id="snackbar"></div>
  <script src="http://code.jquery.com/jquery-1.8.0.js"></script>
  <script>

    
        //check email validation 
        $(document).ready(function(){  
            $("#submit").prop("disabled", true);
            $("#email").keyup(function(){  
                var email=$("#email").val();
                var regex = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                if(!regex.test(email)) {
                  $(".email_error").css('display','block');                 
                  $("#submit").prop("disabled", true);
                }else{
                    $(".email_error").css('display','none');
                    $("#submit").prop("disabled", false);                 
                }
            });  
        });  

      

        //Register user
        $('#add_user').submit(function(e){
        e.preventDefault();
        var form = $('#add_user')[0];
        var data = new FormData(form);
        var baseurl=$("#base_url").val();
        console.log(baseurl);
        console.log(data);
        $.ajax({		            	
                    type: "POST",
                    url: baseurl+'/api/register',
                    enctype: 'multipart/form-data',
                    data: data,
                    beforeSend: function(){
                      // Show image container
                      $("#loader").show();
                    },
                    processData: false,
                    contentType: false,
                    cache: false,                                       
                    success: function(data)
                    {
                        if(data.status ==200)
                        {
                          $("#myModal").modal('show');
                          $("#user_email").val(data.email);
                        }
                        else
                        {
                         
                          var x = document.getElementById("snackbar");
                          $("#snackbar").text(data.message);
                          x.className = "show";
                          setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
                          $("#otp_email").val(data.email);
                          
                          }
                    },
                    complete:function(data){
                      // Hide image container
                      $("#loader").hide();
                    }
                });
        });

        //Verify otp
        function verify_otp(){
          var baseurl=$("#base_url").val();
          var url = baseurl + "/auth_check";
          $.ajax({		            	
                    type: "POST",
                    url: baseurl+'/api/verify-otp',
                    enctype: 'multipart/form-data',
                    data: {
                     email: $('#user_email').val(),
                     email_otp: $('#email_otp').val(),
                     },
                     beforeSend: function(){
                      // Show image container
                      $("#loader").show();
                    },
                    success: function(data)
                    {
                        if(data.status == 200)
                        {
                          $("#myModal").modal('hide');
                          $.ajax({
                                url: url,
                                type: 'POST',
                                data: {
                                    "_token": "{{ csrf_token() }}",
                                    "id": data.data.id,
                                },
                                beforeSend: function(){
                                  // Show image container
                                  $("#loader").show();
                                },
                                success: function(response) {
                                    if (response.message == 'success') {
                                        var purl=$("#previous_url").val();
                                        var array = purl.split('/');
                                        var lastsegment = array[array.length-1];
                                        if(lastsegment=='register' || lastsegment=='login'){
                                            window.location=baseurl;
                                        }
                                        else{
                                            window.location=purl;
                                        }
                                    } 
                                    else {
                                       alert("else");
                                    }
                                },
                                complete:function(data){
                                  // Hide image container
                                  $("#loader").hide();
                                }
                            });
                        }
                        else
                        {
                          var x = document.getElementById("snackbar");
                          $("#snackbar").text(data.message);
                          x.className = "show";
                          setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
                          $("#otp").val('');
                          }
                         
                      },
                      complete:function(data){
                      // Hide image container
                      $("#loader").hide();
                    }
                });
        }
        
        function mobileValidation(){
          console.log("hello");
          var mobileNum = $("#phone").val();
          var validateMobNum= /^\d*(?:\.\d{1,2})?$/;
          if (validateMobNum.test(mobileNum ) && mobileNum.length == 10) {
              $(".mobile_error").css('display','none');
              $("#submit").prop("disabled", false);
          }
          else {
            $(".mobile_error").css('display','block');
            $("#submit").prop("disabled", true);
          }
        }


  


</script>
@endsection