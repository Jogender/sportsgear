<!doctype html>
<html class="no-js" lang="en">

<head>
  @include('front.include.css')
</head>
<style>
    #snackbar {
      visibility: hidden;
      min-width: 250px;
      margin-left: -125px;
      background-color: #333;
      color: #fff;
      text-align: center;
      border-radius: 2px;
      padding: 16px;
      position: fixed;
      z-index: 1;
      left: 50%;
      bottom: 30px;
      font-size: 17px;
    }
    
    #snackbar.show {
      visibility: visible;
      -webkit-animation: fadein 0.5s, fadeout 0.5s 2.5s;
      animation: fadein 0.5s, fadeout 0.5s 2.5s;
    }
    
</style>

<body>

  <script src="{{URL::asset('html/js-image-zoom.js')}}" type="application/javascript"></script>
  <link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.6.0/pure-min.css">

  

  @include('front.include.header')
  <div id="content">
    <!-- Products -->
    <section class="padding-top-40 padding-bottom-60">
      <div class="container">
        <div class="row">
          <!-- Products -->
          <div class="col-md-12">
            <div class="product-detail">
              <div class="product">
                <div class="row">


                  <!-- Slider Thumb -->
                
                  @php $image=json_decode($data->image); @endphp
                  <div class="col-xs-5">
                    <article class="slider-item on-nav">
                      <div class="thumb-slider">
                        @foreach($image as $key => $i)
                          @if($key == 0)
                            <div class="liveImage">
                              <div id="img_container" style="width: 100%; height:400px;">
                                <img id="productActiveImage"  src="{{URL::asset('image/product/'.$i)}}" />
                              </div>
                            </div>
                          @endif 

                        @endforeach
                      </div> </br>
                        
                          {{-- Thumb scroll --}}
                      <div class="scrollthumb">
                        <div class="row ">                          
                          @foreach($image as $key => $i)
                            <div class="col-md-3">
                              <a href="javascript:;" class="thumb-link"  onclick=changeImage("{{URL::asset('image/product/'.$i)}}") >
                                <img class="thumbImage" src="{{URL::asset('image/product/'.$i)}}" border="1"  />
                              </a>
                            </div>  
                          @endforeach
                        </div>
                      </div>
                      
                    </article>
                  </div>



                  <!-- Item Content -->
                  <div class="col-xs-7 positionInherit" >
                    <h5>{{ $data->name }}</h5>
                    <p class="rev"> 
                      @for ($i =0 ; $i < 5 ; $i++)
                        <i class=" @if($i < $star_rating) fa fa-star @else fa fa-star-o @endif"></i>
                      @endfor                
                      <span class="margin-left-10">{{  $review_count  }}  Review(s)</span></p>
                    <div class="row">
                      <div class="col-sm-4">
                        <span>M.R.P: ₹<del>{{$data->old_price}}</del> </span> </br>
                        <span class="price">Deal Price: ₹<span id="price">{{number_format($data->new_price,2)}}</span></span>
                        <input type="hidden" value="{{$data->new_price}}" id="real_price">
                      </div>
                      <div class="col-sm-4">
                        @if($data->quantity>0)
                        <p>Availability: <span class="in-stock">In stock</span></p>
                        @else
                        <p>Availability: <span class="in-stock" style="color:red;">Out of stock</span></p>
                        @endif
                      </div>
                      <div class="col-sm-4">
                      <p><span>Brand : {{ $brand_name->name }}</span></p>
                      </div>
                    </div>

                    <input type="hidden" value="100" name="product_price" id="product_price">
                    <input type="hidden" value="weight" name="product_type" id="product_type">
                    <input type="hidden" value="{{$status}}" id="wish_status">
                    <input type="hidden" @if(Auth::Check()) value="{{Auth::User()->id}}" @endif id="user_id">
                    <input type="hidden" value="{{$data->id}}" name="product_id" id="product_id">

                    <!-- Compare Wishlist -->
                    <!-- <ul class="cmp-list">
                      <li onclick="add_to_wishlist()"><a href="#."><i class="fa fa-heart fa-2" id="whislist" @if($status) style="color:red;" @endif></i> Add to Wishlist</a></li>
                      <li><a href="#."><i class="fa fa-navicon"></i> Add to Compare</a></li>
                      <li><a href="#."><i class="fa fa-envelope"></i> Email to a friend</a></li>
                    </ul> -->



                    {{-- Varient --}}
                    @if($data->variant_status == 'yes')
                      @php $variantDetail=json_decode($data->variant); $variantLoop=0; @endphp
                      @foreach($variantDetail as $key => $value)</br>
                        @foreach($value as $k=>$v)
                        <div class="row">
                          <div class="col-xs-10">
                            @foreach($v as $l=>$w)
                              @if($l == 0 ) <h5>*{{$k}}</h5> @endif
                              <button id="{{str_replace('.', '_', str_replace(' ', '_', $w->value))}}" class="varientButton @if($l==0) selected_btn @endif {{str_replace(' ', '_', $k)}}" 
                                onclick=change_varient({{$w->price}},'{{$w->pricechange}}','{{str_replace(' ', '_', $k)}}','{{str_replace('.', '_', str_replace(' ', '_', $w->value))}}')>{{$w->value}}</button> &nbsp;
                            @endforeach
                          </div>
                        </div>
                        @endforeach
                      @endforeach
                      </br>
                    @endif

                    {{-- ENd varient --}}

                    @if($data->quantity)
                      @if($data->variant_status=='no')
                        <div class="text"> {!! $data->description !!}</div><br>
                        <a href="#pro-detil"><b>Show More</b></a><br>
                      @endif
                      <br>
                      <div class="quinty">
                        <input type="number" value="01" min="1" id="quantity" name="quantity" />
                      </div>
                    
                      @if(Auth::Check())
                        @if($cartStatus == 1)
                          <a href="{{route('my_cart')}}" class="btn-round go_to_cart" ><i class="icon-basket-loaded margin-right-5"></i> Go to Cart</a>
                          <a href="{{route('my_cart')}}" class="btn-round go_to_cart" ><i class="icon-basket-loaded margin-right-5"></i>Order Now</a>
                        @else 
                          <a href="javascript:;" class="btn-round add_to_cart" onclick=add_to_cart({{Auth::User()->id}},{{$data->id}},'')><i class="icon-basket-loaded margin-right-5"></i> Add to Cart</a>
                          <a href="javascript:;" class="btn-round add_to_cart" onclick=add_to_cart({{Auth::User()->id}},{{$data->id}},'order_now')><i class="icon-basket-loaded margin-right-5"></i>Order Now</a>
                        @endif                      
                      @else
                        <a href="{{route('product_info_auth',['id'=>$data->slug_name])}}" class="btn-round add_to_cart"><i class="icon-basket-loaded margin-right-5"></i> Add to Cart</a>
                        <a href="{{route('product_info_auth',['id'=>$data->slug_name])}}" class="btn-round add_to_cart"><i class="icon-basket-loaded margin-right-5"></i> Order Now</a>
                      @endif
                      <a href="{{route('my_cart')}}" class="btn-round go_to_cart" style="display:none;"><i class="icon-basket-loaded margin-right-5"></i> Go to Cart</a>
                    @endif

                    &nbsp;&nbsp;
                    <a href="javascript:void(0);" onclick="add_to_wishlist()" style="padding-left: 15px;"><i class="fa fa-heart fa-2x" id="whislist" @if($status) style="color:red;" @endif></i></a>
                   
                  </div>
                 
                </div>
              </div>

              <!-- Details Tab Section-->
              <div class="item-tabs-sec">

                <!-- Nav tabs -->
                <ul class="nav" role="tablist">
                  <li role="presentation" class="active"><a href="#pro-detil" role="tab" data-toggle="tab">Product
                      Details</a></li>
                  <li role="presentation"><a href="#cus-rev" role="tab" data-toggle="tab">Customer Reviews</a></li>
                  {{-- <li role="presentation"><a href="#ship" role="tab" data-toggle="tab">Shipping & Payment</a></li> --}}
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                  <div role="tabpanel" class="tab-pane fade in active" id="pro-detil">
                    <!-- List Details -->
                    {!! $data->short_description !!}
                  </div>

                  <div role="tabpanel" class="tab-pane fade" id="cus-rev">
                    <div class="row">
                      <div class="col-md-6">
                        <p class="rev">
                            @for ($i =0 ; $i < 5 ; $i++)
                              <i class=" @if($i < $star_rating) fa fa-star @else fa fa-star-o @endif"></i>
                            @endfor  
                          </i> <span class="margin-left-10">{{ $review_count }} Review(s)</span></p>
                      </div>
                      <div class="col-md-6">
                        <a href="{{ route('review',['id'=>encrypt($data->id)]) }}" class="btn-round"
                          style="float:right;background-color:#f74b16;"><i
                            class="margin-right-5"></i>Add Review</a>
                      </div>
                      <div class=row>
                        @php $size=sizeof($rating);@endphp
                        @if($size > 0)

                        @foreach($rating as $r)
                        <div class="col-md-12">
                          <h6><b>{{ $r->username }}</b></h6>
                          <p style="padding-left:10px;">{{ $r->review }}</p>
                        </div>
                        @endforeach
                        @else
                        <div class="col-md-12">
                          <h6 style="padding-left:15px;"><b>No Review Yet!</b></h6>
                          <p></p>
                        </div>
                        @endif
                        <div>
                        </div>

                        <div role="tabpanel" class="tab-pane fade" id="ship"></div>
                      </div>
                    </div>
                  </div>

                  <!-- Related Products -->
                  <section class="padding-top-30 padding-bottom-0">
                    <!-- heading -->
                    <div class="heading">
                      <h2>Others Products</h2>
                      <hr>
                    </div>
                    <!-- Items Slider -->
                    <div class="item-slide-4 with-nav">
                      <!-- Product -->
                      @foreach($other_product as $p)
                      <div class="product">
                        <a href="{{ route('product_info',['id'=>$p->slug_name]) }}" class="tittle">
                        <article>
                          @php $img=json_decode($p->image); @endphp
                          @php if(count($img) > 0){
                          $images=$img[0];
                          }
                          else {
                          $images='';
                          }
                          @endphp
                          <img class="img-responsive height245 inner-img" src="{{URL::asset('image/product/'.$images)}}" alt="">
                          <!-- Content -->
                          <p>{{ $p->name }}</p>
                          <!-- Reviews -->
                          {{-- <p class="rev"><i class="fa fa-star"></i><i class="fa fa-star"></i><i
                              class="fa fa-star"></i><i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <span
                              class="margin-left-10">5 Review(s)</span></p> --}}
                          <div class="price"><i class="fa fa-inr" aria-hidden="true"></i>{{ $p->new_price }}</div>                        
                        </article>
                        </a>
                      </div>
                      @endforeach
                    </div>
                  </section>
                </div>
              </div>
            </div>
    </section>

    <div id="snackbar"></div>

    @include('front.include.footer')

  </div>
  <input type="hidden" value="{{ $data->variant }}" name="varient" id="varient">
  <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"> </script>

  <!-- JavaScripts -->
  <script src="{{URL::asset('frontend/main-files/html/js/vendors/jquery/jquery.min.js')}}"></script>
  <script src="{{URL::asset('frontend/main-files/html/js/vendors/wow.min.js')}}"></script>
  <script src="{{URL::asset('frontend/main-files/html/js/vendors/bootstrap.min.js')}}"></script>
  <script src="{{URL::asset('frontend/main-files/html/js/vendors/own-menu.js')}}"></script>
  <script src="{{URL::asset('frontend/main-files/html/js/vendors/jquery.sticky.js')}}"></script>
  <script src="{{URL::asset('frontend/main-files/html/js/vendors/owl.carousel.min.js')}}"></script>
  <!-- SLIDER REVOLUTION 4.x SCRIPTS  -->
  <script type="text/javascript" src="{{URL::asset('frontend/main-files/html/rs-plugin/js/jquery.tp.t.min.js')}}">
  </script>
  <script type="text/javascript" src="{{URL::asset('frontend/main-files/html/rs-plugin/js/jquery.tp.min.js')}}">
  </script>
  <script src="{{URL::asset('frontend/main-files/html/js/main.js')}}"></script>
  <script type="text/javascript" src="{{URL::asset('frontend/main-files/html/rs-plugin/js/jquery.tp.t.min.js')}}">
  </script>
  <script type="text/javascript" src="{{URL::asset('frontend/main-files/html/rs-plugin/js/jquery.tp.min.js')}}">
  </script>

  <!-- <script src='http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js'></script> -->
  <script src='{{ asset('zoom-master/jquery.zoom.js')}}'></script>
  <script>
   function add_to_wishlist(){
    var status=$('#wish_status').val();
    var user_id=$('#user_id').val();
    if (user_id) {
      $.ajax({
        url: "{{ route('add_to_wishlist') }}",
        type: 'POST',
        data: {
          "_token": "{{ csrf_token() }}",
          "user_id": user_id,
          "product_id": $('#product_id').val()
        },
        success: function (data) {
          if (data.status == 'true') {
            if (status==0) {
              $('#wish_status').val(1);
              $('#whislist').css("color", "red");
              $("#snackbar").text("Product wishlisted successfully");
            } else {
              $('#wish_status').val(0);
              $('#whislist').css("color", "");
              $("#snackbar").text("Product removed from wishlist successfully");
            }
          }
        }
      });
    }
    else{
      $("#snackbar").text("Login first to wishlist the products!!");
    }
    var x = document.getElementById("snackbar");
    x.className = "show";
    setTimeout(function(){ x.className = x.className.replace("show", "");window.location.reload(); }, 3000);
  }

    $(document).ready(function () {
      $('.ex1').zoom();
      $('#ex2').zoom({
        on: 'grab'
      });
      $('#ex3').zoom({
        on: 'click'
      });
      $('#ex4').zoom({
        on: 'toggle'
      });
    });

    function change_varient(price, pricechange,myclass,id) {
      $("."+myclass).removeClass('selected_btn');
      $("#"+id).addClass("selected_btn");
      if (pricechange == 'on') {
        $('#price').text(parseInt($('#real_price').val()) + parseInt(price));
      }
    }

    function add_to_cart(user_id, product_id,order_now) {
      var varients = [];
      $(".selected_btn").each(function(){
        varients.push($(this).text());
      });
      $.ajax({
        url: "{{ route('add_to_cart') }}",
        type: 'POST',
        data: {
          "_token": "{{ csrf_token() }}",
          "user_id": user_id,
          "product_id": product_id,
          "quantity": $('#quantity').val(),
          "price": $('#price').text(),
          "varients": varients
        },
        success: function (data) {
          
          if (data.status == 'true') {
            $('.add_to_cart').hide();
            $('.go_to_cart').show();
            //CartCount
            var cartCount= parseInt($('.cart_count').text());            
            $('.cart_count').text(cartCount+1);
            if (order_now=='order_now') {
              window.location='{{route('my_cart')}}';
            }
            else{
              $("#snackbar").text(data.message);
            }
          } else {
            $("#snackbar").text(data.message);
          }

          var x = document.getElementById("snackbar");
          x.className = "show";
          setTimeout(function(){ x.className = x.className.replace("show", "");window.location.reload(); }, 3000);
        }
      });
    }

    function search_product() {
      var keyword = $("#search").val();
      if (keyword.length > 2) {
        var category = $("#category").val();
        $.ajax({
          url: "{{ route('product_search') }}",
          type: 'POST',
          data: {
            "_token": "{{ csrf_token() }}",
            "category": $("#category").val(),
            "keyword": keyword,
          },
          success: function (data) {
            console.log(data);
            if (data.status == 200) {
              $('#searchList').fadeIn();
              $('#searchList').html(data.ul);
            } else {

              $('#searchList').html(data.ul);
              setTimeout(function () {
                $('#searchList').empty();
              }, 3000);
            }
          }
        });
      }
    }

    function changeImage(src){ 
      $('.liveImage').html("");
      var html= "<div id='img_container' style='width: 500px; height:400px;'>"+
                  "<img id='productActiveImage'  src='"+src+"' />"+
                "</div>";


      $('.liveImage').append(html); 

      var options1 = {
        width: 400,
        zoomWidth: 500,
        offset: {vertical: 0, horizontal: 10}
      };

      // If the width and height of the image are not known or to adjust the image to the container of it
      var options2 = {
          fillContainer: true,
          offset: {vertical: 0, horizontal: 10}
      };
      new ImageZoom(document.getElementById("img_container"), options2);
     
    }


  </script>

<script>
  var options1 = {
    width: 400,
    zoomWidth: 500,
    offset: {vertical: 0, horizontal: 10}
  };

  // If the width and height of the image are not known or to adjust the image to the container of it
  var options2 = {
      fillContainer: true,
      offset: {vertical: 0, horizontal: 10}
  };
  new ImageZoom(document.getElementById("img_container"), options2);

</script>