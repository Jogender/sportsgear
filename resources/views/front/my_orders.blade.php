@section('css')
<style>
@import url("https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600&display=swap");
*, *::after, *::before {
  padding: 0;
  margin: 0;
  box-sizing: border-box;
}

html {
  font-size: 62.5%;
}

body {
  color: #2c3e50;
  font-family: 'Montserrat', sans-serif;
  font-weight: 300;
  min-height: 100vh;
  position: relative;
  display: block;
  margin: 2rem auto;
}

h2, h4, h6 {
  margin: 0;
  padding: 0;
  display: inline-block;
}

.root {
  padding: 1rem;
  border-radius: 5px;
  box-shadow: 0 2rem 6rem rgba(0, 0, 0, 0.3);
}

figure {
  display: flex;
}
figure img {
  width: 8rem;
  height: 8rem;
  border-radius: 15%;
  border: 1.5px solid #f05a00;
  margin-right: 1.5rem;
  padding:1rem;
}
figure figcaption {
  display: flex;
  flex-direction: column;
  justify-content: space-evenly;
}
figure figcaption h4 {
  font-size: 1.4rem;
  font-weight: 500;
}
figure figcaption h6 {
  font-size: 1rem;
  font-weight: 300;
}
figure figcaption h2 {
  font-size: 1.6rem;
  font-weight: 500;
}

.order-track {
  margin-top: 2rem;
  padding: 0 1rem;
  border-top: 1px dashed #2c3e50;
  padding-top: 2.5rem;
  display: flex;
  flex-direction: column;
}
.order-track-step {
  display: flex;
  height: 5rem;
}
.order-track-step:last-child {
  overflow: hidden;
  height: 4rem;
}
.order-track-step:last-child .order-track-status span:last-of-type {
  display: none;
}
.order-track-status {
  margin-right: 1.5rem;
  position: relative;
}
.order-track-status-dot {
  display: block;
  width: 2.2rem;
  height: 2.2rem;
  border-radius: 50%;
  background: #f05a00;
}
.order-track-status-line {
  display: block;
  margin: 0 auto;
  width: 2px;
  height: 5rem;
  background: #f05a00;
}
.order-track-text-stat {
  font-size: 1.3rem;
  font-weight: 500;
  margin-bottom: 3px;
}
.order-track-text-sub {
  font-size: 1rem;
  font-weight: 300;
}

.order-track {
  transition: all .3s height 0.3s;
  transform-origin: top center;
}
</style>
@extends('front.include.main')
@section('content')
<section class="root" style="padding:5%;">
    <div class="row">
        <div class="col-md-3">
            <figure>
                <img src="https://image.flaticon.com/icons/svg/970/970514.svg" alt="">
                <figcaption>
                    <h4>Tracking Details</h4>
                </figcaption>
            </figure>
        </div>
        <div class="col-md-3">
            <figure>
                <img src="{{ URL::asset('image/shipping.png') }}" alt="">
                <figcaption>
                    <h4>Product Details</h4>
                </figcaption>
            </figure>
        </div>
        <div class="col-md-3">
            <figure>
                <img src="{{ URL::asset('image/address.png') }}" alt="">
                <figcaption>
                    <h4>Address Details</h4>
                </figcaption>
            </figure>
        </div>
        <div class="col-md-3">
          <figure>
              <img src="{{ URL::asset('image/box.png') }}" alt="">
              <figcaption>
                  <h4>Order Details</h4>
              </figcaption>
          </figure>
      </div>
    </div>

  @foreach($orders as $order)
    <div class="order-track">
      <div class="row">
        <div class="col-md-3">
            <div class="order-track-step">
                <div class="order-track-status">
                    <span class="order-track-status-dot" style="background-color:green;"></span>
                    <span class="order-track-status-line" style="background-color:green;"></span>
                </div>
                <div class="order-track-text">
                    <p class="order-track-text-stat">Ordered</p>
                </div>
            </div>
            @if($order->tracking_status=='Cancelled' || $order->tracking_status=='Rejected')
            <div class="order-track-step">
                <div class="order-track-status">
                    <span class="order-track-status-dot" style="background-color:green;"></span>
                    <span class="order-track-status-line" style="background-color:green;"></span>
                </div>
                <div class="order-track-text">
                    <p class="order-track-text-stat">Cancelled or Rejected</p>
                </div>
            </div>
            @else
            <div class="order-track-step">
                <div class="order-track-status">
                    <span class="order-track-status-dot" @if($order->tracking_status=='Delivered' || $order->tracking_status=='Dispatched' || $order->tracking_status=='Packed') style="background-color:green;" @endif></span>
                    <span class="order-track-status-line" @if($order->tracking_status=='Delivered' || $order->tracking_status=='Dispatched' || $order->tracking_status=='Packed') style="background-color:green;" @endif></span>
                </div>
                <div class="order-track-text">
                    <p class="order-track-text-stat" > Packed</p>
                </div>
            </div>
            <div class="order-track-step">
              <div class="order-track-status">
                  <span class="order-track-status-dot" @if($order->tracking_status=='Delivered' || $order->tracking_status=='Dispatched') style="background-color:green;" @endif></span>
                  <span class="order-track-status-line" @if($order->tracking_status=='Delivered' || $order->tracking_status=='Dispatched') style="background-color:green;" @endif></span>
              </div>
              <div class="order-track-text">
                  <p class="order-track-text-stat" >Shipped</p>
              </div>
            </div>
            <div class="order-track-step">
                <div class="order-track-status">
                    <span class="order-track-status-dot" @if($order->tracking_status=='Delivered') style="background-color:green;" @endif></span>
                    <span class="order-track-status-line" @if($order->tracking_status=='Delivered') style="background-color:green;" @endif></span>
                </div>
                <div class="order-track-text">
                    <p class="order-track-text-stat"> Delivered</p>
                </div>
            </div>
            @endif
        </div>
        @php $details=json_decode($order->product_detail);  $products=$order->products; @endphp
        <div class="col-md-3">
          @foreach($products as $key=>$product)
            @if($key==0)
               @php $images=json_decode($product->image); @endphp
              <img src="{{URL::asset('image/product/'.$images[0])}}" style="width: 100px;" />
            @endif
          @endforeach
          <p class="order-track-text-stat">@foreach($products as $product) @foreach($details as $det) @if($det->product_id==$product->id) {{$product->name.', '}} @endif @endforeach @endforeach</p>
          @foreach($details as $det)
            @php $prod_detsils=json_decode($det->details); @endphp
            <p class="order-track-text-stat">@if($prod_detsils) {{ implode(", ",$prod_detsils) }} @endif</p>
          @endforeach
        </div>
        <div class="col-md-3">
          @php $address=explode(",",$order->dl_address); @endphp
          @foreach($address as $add)
            <p class="order-track-text-stat">{{$add}}</p>
          @endforeach
        </div>
        <div class="col-md-3">
        <p class="order-track-text-stat"><span style="font-weight:bold;">Amount:</span>  {{$order->amount}}</p>
          <p class="order-track-text-stat"><span style="font-weight:bold;">Order Date:</span>  {{$order->date}}</p>
          <p class="order-track-text-stat"><span style="font-weight:bold;">Order ID:</span>  {{$order->transaction_id}}</p>
          <p class="order-track-text-stat"><span style="font-weight:bold;">Transation ID:</span>  {{$order->amount}}</p>
          <!-- <a href="{{ route('order_detail',['id'=>encrypt($order->id)]) }}" class="btn-round">View Detail</a> -->
        </div>
      </div>
    </div>
    @endforeach
    <div class="order-track">
        
    </div>
    {{$orders->links()}}
</section>
@endsection