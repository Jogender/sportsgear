@extends('front.include.main')
@section('content')
<div id="content">
    <!-- Payout Method -->
    <section class="padding-bottom-60">
        <div class="container">
          <!-- Payout Method -->
          <div class="pay-method">
            <div class="row">
              <div class="col-md-6">
                <!-- Your information -->
                <div class="heading">
                  <h2>Add New Address</h2>
                  <hr>
                </div>
                <form method="post" action="{{route('add_address')}}" enctype="multipart/form-data"
                class="uk-form-stacked task-form">
                @csrf
                  <div class="row">
    
                    <!-- Name -->
                    <div class="col-sm-6">
                      <label> First name
                        <input class="form-control" type="text" name="firstname" required>
                      </label>
                    </div>
    
                    <!-- Number -->
                    <div class="col-sm-6">
                      <label> Last Name
                        <input class="form-control" type="text" name="lastname" required>
                      </label>
                    </div>
    
                    <!-- Card Number -->
                    <div class="col-sm-12">
                      <label> Address
                       <textarea class="form-control" name="address" required></textarea>
                      </label>
                      <br>
                    </div>

                      <!-- Zip code -->
                      <div class="col-sm-6">
                        <label> City
                          <input class="form-control" type="text" name="city" required>
                        </label>
                      </div>
      
                      <!-- Address -->
                      <div class="col-sm-6">
                        <label> State
                          <select class="form-control" required name="state">
                            @foreach($states as $state)
                              <option value="{{$state->id}}">{{$state->name}}</option>
                            @endforeach
                          </select>
                        </label>
                      </div>
                   
    
                    <!-- Zip code -->
                    <div class="col-sm-6">
                      <label> Pin code
                        <input class="form-control" type="text" name="pincode" id="zip" onkeyup="check_pincode()" required>
                    </label>
                    </div>
    
                    <!-- Address -->
                    <div class="col-sm-6">
                      <label> Mobile
                        <input class="form-control" type="text" name="mobile" onkeyup="check_mobile()" id="mobile" required>
                        <p class="mobile_error" style="color:red !important;"></p>
                    </label>
                    </div>
    
                    <!-- Number -->
                    <div class="col-sm-6">
                      <label> Type </label>
                      <select class="selectpicker" name="type" required>
                        <option value="home"> Home</option>
                        <option value="office"> Office</option>
                        <option value="other"> Other</option>
                      </select>
                    </div>
    
                    <div class="col-sm-6">
                      <label> Default </label>
                      <select class="selectpicker" name="default">
                        <option value="1"> Yes</option>
                        <option value="0"> No</option>
                      </select>
                    </div>
                  </div><br>
                  <button type="submit" id="submit" class="btn-round btn-light pro-btn" style="border-color:#f74b16;">Submit</button>
                </form>
              </div>
    
              <!-- Select Your Transportation -->
              <div class="col-md-6">
                <div class="heading">
                  <h2>User Address</h2>
                  <hr>
                </div>
                <div class="transportation">
                  @php $numOfCols = 2;
                  $rowCount = 0;
                  $bootstrapColWidth = 12 / $numOfCols; @endphp
                  <div class="row">
                    <!-- Free Delivery -->
                    @foreach($address as $a)
                    <div class="col-sm-6">
                      <div class="charges">
                        <h6>{{ ucfirst($a->firstname) }} {{ ucfirst($a->lastname) }}</h6><br>
                        <span>{{ $a->address }} {{ $a->city }} <br> {{ $a->mobile }} </span><br>
                        <span>{{ $a->pincode }}</span>
                        <br>
                        {{-- <span>7 - 12 days</span> </div> --}}
                      </div>
                      <div style="text-align:center;">
                        <a href="javascript:void(0)"><i class="fa fa-pencil" style="color:blue;" onclick="edit_address({{$a->id}})"></i></a>&nbsp;&nbsp;<a href="javascript:void(0)"><i
                                            class="fa fa-trash" style="color:red;" data-target="#deleteModal" data-toggle="modal"
                                            onclick="setId({{$a->id}})"></i></a>
                      </div>
                    </div>
                      @endforeach
                      @php $rowCount++; @endphp
                      @if($rowCount % $numOfCols == 0)</div>
                    <div class="row"> @endif
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- Button -->
      </section>
</div>
    <!----- Edit Modal -------->
    <div class="bs-example">
        <div id="editModal" class="modal fade" tabindex="-1">
            <div class="modal-dialog">
                <div class="modal-content">
                    <form method="post" action="{{route('update_address')}}" id="form_validation"
                        enctype="multipart/form-data" class="uk-form-stacked task-form">
                        @csrf
                        <input type="hidden" value="" id="address_id" name="id">
                        <div class="modal-header">
                            <h5 class="modal-title">Update Address</h5>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <label for="firstname">FirstName</label>
                                    <input type="text" value="" id="e_firstname" name="firstname" class="form-control">
                                </div>
                                <div class="col-md-6">
                                    <label for="lastname">Lastname</label>
                                    <input type="text" value="" id="e_lastname" name="lastname" class="form-control">
                                </div>
                            </div><br>

                            <div class="row">
                                <div class="col-md-12">
                                    <label for="firstname">Address</label>
                                    <textarea value="" name="address" id="e_address" class="form-control"></textarea>
                                </div>
                            </div><br>

                            <div class="row">
                                <div class="col-md-6">
                                    <label for="city">City</label>
                                    <input type="text" value="" id="e_city" name="city" class="form-control">
                                </div>
                                <div class="col-md-6">
                                    <label for="state">State</label>
                                    <select id="e_state" name="state" class="form-control">
                                    </select>
                                </div>
                            </div><br>

                            <div class="row">
                                <div class="col-md-6">
                                    <label for="mobile">Mobile</label>
                                    <input type="number" value="" id="e_mobile" name="mobile" class="form-control">
                                </div>
                                <div class="col-md-6">
                                    <label for="pincode">PinCode</label>
                                    <input type="number" value="" id="e_pincode" name="pincode" class="form-control">
                                </div>
                            </div><br>

                            <div class="row">
                                <div class="col-sm-6">
                                    <label> Default </label>
                                    <select class="form-control" name="default" id="e_default">
                                      <option value="1"> Yes</option>
                                      <option value="0"> No</option>
                                    </select>
                                  </div>
                                <div class="col-sm-6">
                                    <label> Type </label>
                                    <select class="form-control" name="type" required id="e_type">
                                      <option value="home"> Home</option>
                                      <option value="office"> Office</option>
                                      <option value="other"> Other</option>
                                    </select>
                                  </div>
                            </div><br>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" style="background-color:#372e41 !important;"
                                data-dismiss="modal">Cancel</button>
                            <button type="submit" class="btn-round btn-light">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-----------End Modal ----------->
    <div class="bs-example">
        <div id="deleteModal" class="modal fade" tabindex="-1">
            <div class="modal-dialog">
                <div class="modal-content">
                    <form method="post" action="{{route('delete_address')}}" id="form_validation"
                        enctype="multipart/form-data" class="uk-form-stacked task-form">
                        @csrf
                        <input type="hidden" value="" id="add_id" name="address_id">
                        <div class="modal-header">
                            <h6 class="modal-title">Delete Address</h6>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                        <div class="modal-body">
                            <p>Are you sure to delete this address? </p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" style="background-color:#372e41 !important;"
                                data-dismiss="modal">No</button>
                            <button type="submit" class="btn-round btn-light" style="border-color:#f74b16;">Yes</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
<script src="http://code.jquery.com/jquery-1.8.0.js"></script>
<script>
    function form_submit()
    {
        alert('hello');
        return false;
    }
    </script>
    <script>
        function edit_address(id) {
            var address_id = id;
            $.ajax({
                url: "{{route('edit_address')}}",
                type: 'POST',
                data: {
                    "_token": "{{ csrf_token() }}",
                    "address_id": address_id,
                },
                success: function (response) {
                    if (response.message == 'success') {
                        $("#e_firstname").val(response.data.firstname);
                        $("#e_lastname").val(response.data.lastname);
                        $("#e_mobile").val(response.data.mobile);
                        $("#e_address").val(response.data.address);
                        $("#e_city").val(response.data.city);
                        response.states.map(item=>{
                          $('#e_state').append('<option value="' + item['id'] + '">' + item['name'] + '</option>');
                        })
                        
                        $("#e_pincode").val(response.data.pincode);
                        var type=response.data.type;
                        if(type="home"){
                            $("#e_type").val("home");
                        }else if(type="office"){
                            $("#e_type").val("home");
                        }else if(type="other"){
                            $("#e_type").val("other");
                        }
                        $("#address_id").val(response.data.id);
                        if (response.data.default == 1) {
                            $("#e_default").val("1");
                        } else {
                            $("#e_default").val("0");
                        }
                        $('#editModal').modal('toggle');
                    } else {
                        alert("else");
                    }
                }
            });
        }

        function setId(id) {
            $("#add_id").val(id);
        }

        //Check mobile validation
        function check_mobile(){
            console.log("hi");
            var mobileNum = $("#mobile").val();
            var validateMobNum= /^\d*(?:\.\d{1,2})?$/;
            if (validateMobNum.test(mobileNum ) && mobileNum.length == 10) {
               $('#submit').prop('disabled', false);
               $(".mobile_error").text("");
            }
            else {
                $(".mobile_error").text("Invalid Mobile Number");
                $('#submit').prop('disabled', true);
            }

        }

        //Check Pin Code
        function check_pincode(){
            var zip = $('#zip').val();
            var reg = /^[0-9]+$/;
            if ((zip.length)< 5 || (zip.length)>6 ){
                $(".zip_error").text( "Invalid Pincode");
                $('#submit').prop('disabled', true);
            }
            else{
                $(".zip_error").text("");
                $('#submit').prop('disabled', false);
            }
        }
    </script>