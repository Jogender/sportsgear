
<!-- JavaScripts --> 
<script src="{{URL::asset('frontend/main-files/html/js/vendors/jquery/jquery.min.js')}}"></script> 
<script src="{{URL::asset('frontend/main-files/html/js/vendors/wow.min.js')}}"></script> 
<script src="{{URL::asset('frontend/main-files/html/js/vendors/bootstrap.min.js')}}"></script> 
<script src="{{URL::asset('frontend/main-files/html/js/vendors/own-menu.js')}}"></script> 
<script src="{{URL::asset('frontend/main-files/html/js/vendors/jquery.sticky.js')}}"></script> 
<script src="{{URL::asset('frontend/main-files/html/js/vendors/owl.carousel.min.js')}}"></script> 
<!-- SLIDER REVOLUTION 4.x SCRIPTS  --> 
<script type="text/javascript" src="{{URL::asset('frontend/main-files/html/rs-plugin/js/jquery.tp.t.min.js')}}"></script> 
<script type="text/javascript" src="{{URL::asset('frontend/main-files/html/rs-plugin/js/jquery.tp.min.js')}}"></script> 
<script src="{{URL::asset('frontend/main-files/html/js/main.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('frontend/main-files/html/rs-plugin/js/jquery.tp.t.min.js')}}"></script> 
<script type="text/javascript" src="{{URL::asset('frontend/main-files/html/rs-plugin/js/jquery.tp.min.js')}}"></script> 

<script>window.pageData={}; window.pageData.baseUrl = "{{ url('/') }}";</script>


<script>
jQuery(document).ready(function($) {
  
  //  Price Filter ( noUiSlider Plugin)
    $("#price-range").noUiSlider({
    range: {
      'min': [ 100 ],
      'max': [ 50000 ]
    },
    start: [0,50000],
        connect:true,
        serialization:{
            lower: [
        $.Link({
          target: $("#price-min")
        })
      ],
      upper: [
        $.Link({
          target: $("#price-max")
        })
      ],
      format: {
      // Set formatting
        decimals: 2,
        prefix: '₹'
      }
        }
  })
})

</script>

<script>
  function search_product(){
   var keyword=$("#search").val();
   if(keyword.length > 2){
    var category=$("#category").val();
    $.ajax({		            	
          url: "{{ route('product_search') }}",
          type: 'POST',
          data: {
              "_token": "{{ csrf_token() }}",
              "category":$("#category").val(),
              "keyword":keyword,
          },                                   
          success: function(data)
          {
            console.log(data);
              if(data.status ==200)
              {
                $('#searchList').fadeIn();  
                $('#searchList').html(data.ul);
              }
              else
              {
                
                $('#searchList').html(data.ul);
                setTimeout(function(){ $('#searchList').empty(); }, 3000);
              }
          }
      });
   }
   else{
    $('#searchList').empty();
   }
  }
</script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-notify/0.2.0/js/bootstrap-notify.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-notify/0.2.0/css/bootstrap-notify.css">


<div class='notifications top-right'></div>


<script>


  @if(Session::has('success'))
     $('.top-right').notify({
        message: { text: "{{ Session::get('success') }}" }
      }).show();
     @php
       Session::forget('success');
     @endphp
  @endif


  @if(Session::has('info'))
      $('.top-right').notify({
        message: { text: "{{ Session::get('info') }}" },
        type:'info'
      }).show();
      @php
        Session::forget('info');
      @endphp
  @endif


  @if(Session::has('warning'))
  		$('.top-right').notify({
        message: { text: "{{ Session::get('warning') }}" },
        type:'warning'
      }).show();
      @php
        Session::forget('warning');
      @endphp
  @endif


  @if(Session::has('error'))
  		$('.top-right').notify({
        message: { text: "{{ Session::get('error') }}" },
        type:'danger'
      }).show();
      @php
        Session::forget('error');
      @endphp
  @endif

  


</script>
