<header class="header-style-5">
  <div class="container">
    <div class="logo"> <a href="{{URL('/')}}"><img src="{{asset('/image/sports.jpeg')}}" alt="" class="height100"></a> </div>
   
      <div class="search-cate searchBox">         
        <input type="search" placeholder="Search entire store here..." onkeyup="search_product()" id="search">          
        <button class="submit" type="submit"><i class="icon-magnifier"></i></button>
        <div id="searchList"></div>  
      </div>

      {{-- <ul class="nav navbar-right cart-pop paddingtop33">  
          @if(Auth::user())
            <li class="dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><img src="{{ URL::asset('image/default_avtar.png') }}" class="width13"><br>
          @else
            <li> <a href="{{ route('user_login') }}"><img src="{{ URL::asset('image/default_avtar.png') }}" class="width13">
          @endif  
          </a>
            <ul class="dropdown-menu">
              <li>
                <div class=""> <a href="{{ route('my_orders') }}" class="tittle"><i class="fa fa-archive" aria-hidden="true"></i>&nbsp; My orders</a> <span></span> </div>
              </li>
              <li>
                <div class=""> <a href="{{ route('user_address') }}" class="tittle"><i class="fa fa-university" aria-hidden="true"></i>&nbsp; My Address</a><span></span> </div>
              </li>
              <li>
                <div class=""> <a href="{{ route('user_profile') }}" class="tittle"><i class="fa fa-user" aria-hidden="true"></i>&nbsp; My Profiles</a> <span></span> </div>
              </li>            
              <li><div class=""> <a href="{{ route('my_cart') }}"><i class="fa fa-cart-plus" aria-hidden="true"></i>&nbsp;My Cart</a></div> </li>
              <li>
                <div class=""> <a href="{{ route('user_logout') }}" class="tittle"><i class="fa fa-sign-out" aria-hidden="true"></i>&nbsp;Logout</a> <span></span> </div>
              </li>
            </ul>
          </li>
      </ul> --}}


      
      <ul class="nav navbar-right cart-pop paddingtop31">
        @if(Auth::user())
          <li class="dropdown floatR"> <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> <img src="{{ URL::asset('image/user/avatar.svg') }}" class="avatarImg" /> </a>
            <ul class="dropdown-menu">
              
              <li>
                <div class=""> <a href="{{ route('user_profile') }}" class="tittle"><i class="fa fa-user" aria-hidden="true"></i>&nbsp; My Profiles</a> <span></span> </div>
              </li>
              <li>
                <div class=""> <a href="{{ route('user_address') }}" class="tittle"><i class="fa fa-university" aria-hidden="true"></i>&nbsp; My Address</a><span></span> </div>
              </li>
              <li>
                <div class=""> <a href="{{ route('my_orders') }}" class="tittle"><i class="fa fa-archive" aria-hidden="true"></i>&nbsp; My orders</a> <span></span> </div>
              </li> 
              <li>
                <div class=""> <a href="{{ route('user_logout') }}" class="tittle"><i class="fa fa-sign-out" aria-hidden="true"></i>&nbsp;Logout</a> <span></span> </div>
              </li>
            </ul>
          </li> 
        @else
         <li class="floatR"> <a href="{{ route('user_login') }}"> <img src="{{ URL::asset('image/user/avatar.svg') }}" class="avatarImg" /> </a></li> 
        @endif 

        <li class="floatL"> <a href="{{ route('my_cart') }}"><span class="itm-cont cart_count">{{$cartCount}}</span><i class="flaticon-shopping cartIcon"></i></a></li>  
        <li class="floatL"> <a href="{{ route('wishlist') }}"><span class="itm-cont wishlist_count">{{$wishlistCount}}</span><i class="flaticon-shopping-bag cartIcon"></i></a></li>      
      </ul>
      
  </div>
  
  <!-- Nav -->
  <nav class="navbar" style="margin-top:20px;border-top:1.5px solid #e2e2e2;border-bottom:1.5px solid #e2e2e2;">
    <div class="container"> 
      <!-- Categories -->
      <!-- Responsive Btn -->
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#nav-open-btn" aria-expanded="false"> <span><i class="fa fa-navicon"></i></span> </button>
      </div>
      
     
      <!-- NAV -->
      <div class="collapse navbar-collapse" id="nav-open-btn">
        <ul class="nav" style="padding-top:0%;">          
          @foreach($category as $c)
            <li @if(sizeof($c->sub_cat)>0) class="dropdown" @endif> <a href="{{ route('category_product',['id'=>$c->slug_name]) }}" @if(sizeof($c->sub_cat)>0)  class="dropdown-toggle" data-toggle="dropdown" @endif>{{ strtoupper($c->name) }} </a>
              {{-- Sub category loop --}}
              @if(sizeof($c->sub_cat)>0)
                <ul class="dropdown-menu multi-level animated-2s fadeInUpHalf">
                  @foreach ($c->sub_cat as $sub)
                    <li @if(sizeof($sub->subsubcategory) > 0) class="dropdown-submenu" @endif><a href="{{ route('subcategory_product',['id'=>$sub->slug_name]) }}" > {{strtoupper($sub->name)}} </a>
                      @if(sizeof($sub->subsubcategory) > 0)                     
                        <ul class="dropdown-menu animated-2s fadeInRight">
                          @foreach($sub->subsubcategory as $subsubcat)
                            <li style="width=100%;"><a href="{{ route('subsubcategory_product',['id'=>$subsubcat->slug_name]) }}"> {{strtoupper($subsubcat->name)}} </a></li>
                            @if(strlen($subsubcat->name < 5))
                              </br>
                            @endif
                          @endforeach                       
                        </ul>
                      @endif
                    </li>
                  @endforeach
                </ul>
              @endif
            </li>
          @endforeach
        </ul>
      </div>
      
      <!-- NAV RIGHT -->
      <div class="nav-right"> </div>
    </div>
  </nav>
</header>