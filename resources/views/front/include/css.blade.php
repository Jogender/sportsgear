<input type="hidden" value="{{url('/')}}" id="base_url">
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="author" content="M_Adnan" />
<!-- Document Title -->
<title>Sports Gear</title>

<!-- Favicon -->
<link rel="shortcut icon" href="{{URL::asset('frontend/main-files/html/images/favicon.ico')}}" type="image/x-icon">
<link rel="icon" href="{{URL::asset('frontend/main-files/html/images/favicon.ico')}}" type="image/x-icon">

<!-- SLIDER REVOLUTION 4.x CSS SETTINGS -->
<link rel="stylesheet" type="text/css" href="{{URL::asset('frontend/main-files/html/rs-plugin/css/settings.css')}}" media="screen" />

<!-- StyleSheets -->
<link rel="stylesheet" href="{{URL::asset('frontend/main-files/html/css/ionicons.min.css')}}">
<link rel="stylesheet" href="{{URL::asset('frontend/main-files/html/css/bootstrap.min.css')}}">
<link rel="stylesheet" href="{{URL::asset('frontend/main-files/html/css/font-awesome.min.css')}}">
<link rel="stylesheet" href="{{URL::asset('frontend/main-files/html/css/main.css')}}">
<link rel="stylesheet" href="{{URL::asset('frontend/main-files/html/css/style.css')}}">
<link rel="stylesheet" href="{{URL::asset('frontend/main-files/html/css/responsive.css')}}">

<!-- Fonts Online -->
<link href="https://fonts.googleapis.com/css?family=Lato:100i,300,400,700,900" rel="stylesheet">

<!-- JavaScripts -->
<script src="{{URL::asset('frontend/main-files/html/js/vendors/modernizr.js')}}"></script>
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
<!-----Toaster Css ----------->
<style>
#snackbar {
  visibility: hidden;
  min-width: 250px;
  margin-left: -125px;
  background-color: #333;
  color: #fff;
  text-align: center;
  border-radius: 2px;
  padding: 16px;
  position: fixed;
  z-index: 1;
  left: 50%;
  bottom: 30px;
  font-size: 17px;
}

#snackbar.show {
  visibility: visible;
  -webkit-animation: fadein 0.5s, fadeout 0.5s 2.5s;
  animation: fadein 0.5s, fadeout 0.5s 2.5s;
}

@-webkit-keyframes fadein {
  from {bottom: 0; opacity: 0;} 
  to {bottom: 30px; opacity: 1;}
}

@keyframes fadein {
  from {bottom: 0; opacity: 0;}
  to {bottom: 30px; opacity: 1;}
}

@-webkit-keyframes fadeout {
  from {bottom: 30px; opacity: 1;} 
  to {bottom: 0; opacity: 0;}
}

@keyframes fadeout {
  from {bottom: 30px; opacity: 1;}
  to {bottom: 0; opacity: 0;}
}


/* RohitDev */
  /* product detail varient button page */
      /* Varient Button */
    .varientButton{
      border: 2px solid black;
      background-color: white;
      color: black;
      padding: 8px 20px;
      font-size: 16px;
      cursor: pointer;
    }

    .pad5{
      padding-bottom: 5% !important; 
    }

    .height245{
      height: 245px !important;
    }

    .height100{
      height: 100px !important;
    }

    .searchBox{
      margin-top:3% !important;
      margin-left:13% !important;
      border:1px solid #e2e2e2 !important;
    }

    
    .width13{
      width: 13% !important;      
    }

    .width23{
      width: 23% !important;      
    }

    .width20{
      width: 20% !important;      
    }

    .cartIcon{
      /* color: #686868; */
      /* border: 2px solid #dddddd; */
      display: inline-block;
      height: 54px;
      width: 54px;
      margin-right: 26px;
      border-radius: 50%;
      line-height: 50px;
      text-align: center;
      font-size: 28px;
      /* background: #eeeeee; */
    }

    .avatarImg{
      height: 56px;
      width: 41px;
      margin-right: 122px;
      border-radius: 50%;
    }

    .cart-pop .flaticon-shopping-bag{
      height: 54px;
      width: 54px;    
      margin-right:32px;
      background-color:#ffffff !important;
      color:#000000 !important;

    }
      

    .floatL{
      float: left !important;
    }

    .floatR{
      float: right !important;
    }

    .paddingtop31{
      padding-top:31px !important;
      min-width: 28% !important;
    }

    .positionInherit{
      position: inherit !important;
    }

    .thumbImage{
      border: 1px dotted #dddddd;
    }

    .thumbImage:hover{
      border: 1px solid #dddddd;
    }

    .productImage{
      width: 500px;
      height:400px;
    }

    /*  horizontal scrollable row product page */
    .scrollthumb > .row {
      overflow-x: auto;
      white-space: nowrap;
    }

    .scrollthumb > .row > .col-md-3 {
      display: inline-block;
      float: none;
    }

    .inner-img {
      transition: 0.2s;
    }
    
    .inner-img:hover {
      transform: scale(1.1);
    }

  @media only screen and (max-width: 600px) {
    #img_container {
      width: 50%;
    }
    .liveImage .thumbImage{    
      width: 100%;
      height: 30%;
    }

    #productActiveImage{
      width: 100% !important;
      height: 30% !important;
    }
  }


  .wrapper {
    min-height: 400px;
    margin-left: 10px;
    display: flex;
}
.imageblock {
    margin-left: 15px;
}
.settings {
    width: 400px;
}

.settings__button {
    display: block;
    width: 120px;
    margin-bottom: 5px;
}
.setting__inputset {
    width: 100px;
}
.setting__inputset__fieldset {
    border: 0;
    margin-top: -15px;
}
#container {
    margin-top: 200px;
}

#container.onehundredpercent {
    width: 350px;
    height: 250px;
}

#container.onehundredpercent img {
    max-width: 100%;
}

.text {
  overflow: hidden;
  text-overflow: ellipsis;
  display: -webkit-box;
  -webkit-line-clamp: 8; /* number of lines to show */
  -webkit-box-orient: vertical;
}
.grayscale {
  -webkit-filter: brightness(1.10) grayscale(100%) contrast(90%);
  -moz-filter: brightness(1.10) grayscale(100%) contrast(90%);
  filter: brightness(1.10) grayscale(100%);
}

/* these styles are for the demo, but are not required for the plugin */
.zoom {
  display: inline-block;
  position: relative;
}

/* magnifying glass icon */
.zoom:after {
  content: '';
  display: block;
  width: 33px;
  height: 33px;
  position: absolute;
  top: 0;
  right: 0;
  background: url(icon.png);
}

.zoom img {
  display: block;
}

.zoom img::selection {
  background-color: transparent;
}

#ex2 img:hover {
  cursor: url(grab.cur), default;
}

#ex2 img:active {
  cursor: url(grabbed.cur), default;
}

.selected_btn{
  border:2px solid #f74b15;
}

.varientButton{
  padding:8px 10px !important;
}


/* Header */
header.header-style-5 .navbar li a{
  font-weight: 400 !important;
}

header .navbar li a{
  font-size: 15px !important;
}

.whatsapp_bar_pc{
  height: 30px;
  overflow: hidden;
  padding: 6px 20px;
  position: fixed;
  bottom: 0;
  width: auto;
  background-color: #f2f2f2;
  z-index: 1;
  text-align: center;
  font-weight: bold;
  right: 0;
}
       
.cart-pop .dropdown-menu{
  min-height: 0px !important;
}

#snackbar {
      visibility: hidden;
      min-width: 250px;
      margin-left: -125px;
      background-color: #333;
      color: #fff;
      text-align: center;
      border-radius: 2px;
      padding: 16px;
      position: fixed;
      z-index: 1;
      left: 50%;
      bottom: 30px;
      font-size: 17px;
    }
    
    #snackbar.show {
      visibility: visible;
      -webkit-animation: fadein 0.5s, fadeout 0.5s 2.5s;
      animation: fadein 0.5s, fadeout 0.5s 2.5s;
    }

    .MRP{
      font-size:12px;
    } 

    .offer{
      color:#d42a2a!important;
    } 

</style>