<!doctype html>
<html class="no-js" lang="en">
<head>
@include('front.include.css')
@yield('css') 
</head>
<body>
<!-- Page Wrapper -->
<div id="wrap" class="layout-3"> 
  
  <!-- Top bar -->


  <!-- Header -->

  @include('front.include.header')

  <!---content ----->
  @yield('content')
  <!-----end content ------>

  @include('front.include.footer')
  
  
  <!-- GO TO TOP  --> 
  <a href="#" class="cd-top"><i class="fa fa-angle-up"></i></a> 
  <!-- GO TO TOP End --> 
</div>
<!-- End Page Wrapper --> 
@include('front.include.js')
@yield('js') 
</body>
</html>