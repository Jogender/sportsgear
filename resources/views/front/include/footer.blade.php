<div class="whatsapp_bar_pc">
  <a href="https://api.whatsapp.com/send?phone=+917999995190&amp;text=Hi, i have a query" target="_blank">
      <div class="icon_whatsapp"></div>
      <span class="whatsapp_bar_text"><i class="fa fa-whatsapp"></i> For more details watsapp us +917999995190</span>
   </a>
</div>


<!-- Newslatter -->
  <section class="newslatter">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <h5 style="color:white;">Secure Payment by : <img src="{{URL::asset('image/card-logo.png')}}" style="padding-left:10px;height:50px;" alt=""></h5>
        </div>
      </div>
    </div>
  </section>

<footer>
    <div class="container"> 
      
      <!-- Footer Upside Links -->
      <div class="foot-link">
        <ul>
          <li><a href="#."> About us </a></li>
          <li><a href="#."> Privacy Policy </a></li>
          <li><a href="#."> Contact Us</a></li>
        </ul>
      </div>
      <div class="row"> 
        
        <!-- Contact -->
        <div class="col-md-4">
          <h4>Contact Sports Gear!</h4>
          <p>Address: Hisar
            Haryana</p>
          <p>Phone: +91-79999 95190</p>
          <p>Email: Support@sportsgear.com</p>
          <div class="social-links"> <a href="https://www.facebook.com/SportsGear.co.in/?ref=aymt_homepage_panel&eid=ARCCUoHcNzdUdOdeVbAtWjmabJujU-56LB3dKiqCS07Z8E7GDlxRvBpyBoWhhDmf5dwSqs3wM1Q7aXeZ" target="_blank"><i class="fa fa-facebook"></i></a> <a href="#."><i class="fa fa-twitter"></i></a> <a href="https://wa.me/message/KOJMRGTQN3A6B1" taget="_blank"><i class="fa fa-whatsapp"></i></a> <a href="#."><i class="fa fa-pinterest"></i></a> <a href="https://www.instagram.com/invites/contact/?i=c0xlqwps7aft&utm_content=2gx1a8t" target="_blank"><i class="fa fa-instagram"></i></a> <a href="#."><i class="fa fa-google"></i></a> </div>
        </div>
        
        <!-- Categories -->
        <div class="col-md-3">
          <h4>Categories</h4>
          <ul class="links-footer">
            @foreach($category as $c)
            <li><a href="{{ route('category_product',['id'=>$c->slug_name]) }}">{{ $c->name }}</a></li>
            @endforeach
          
          </ul>
        </div>
        
        <!-- Categories -->
        <div class="col-md-3">
          <h4>Customer Services</h4>
          <ul class="links-footer">
            <li><a href="#.">Shipping & Returns</a></li>
            <li><a href="#."> Contact </a></li>
          </ul>
        </div>
        
        <!-- Categories -->
        <div class="col-md-2">
          <h4>Information</h4>
          <ul class="links-footer">
            <li><a href="#."> About Sports Gear</a></li>
            <li><a href="#."> Delivery Infomation</a></li>
          </ul>
        </div>
      </div>
    </div>
  </footer>

  <!-- Rights -->
  <div class="rights">
    <div class="container">
      <div class="row">
        <div class="col-sm-6">
          <p>Copyright © 2021 <a href="{{URL('/')}}" class="ri-li"> Sports Gear </a></p>
        </div>
        <div class="col-sm-6 text-right"> <img src="#" alt=""> </div>
      </div>
    </div>
  </div>
  
  <!-- End Footer --> 