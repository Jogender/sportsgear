@extends('front.include.main')
@section('content')
<!--Snackbar CSS --->
<style>
    #snackbar {
      visibility: hidden;
      min-width: 250px;
      margin-left: -125px;
      background-color: #333;
      color: #fff;
      text-align: center;
      border-radius: 2px;
      padding: 16px;
      position: fixed;
      z-index: 1;
      left: 50%;
      bottom: 30px;
      font-size: 17px;
    }
    
    #snackbar.show {
      visibility: visible;
      -webkit-animation: fadein 0.5s, fadeout 0.5s 2.5s;
      animation: fadein 0.5s, fadeout 0.5s 2.5s;
    }
    
    @-webkit-keyframes fadein {
      from {bottom: 0; opacity: 0;} 
      to {bottom: 30px; opacity: 1;}
    }
    
    @keyframes fadein {
      from {bottom: 0; opacity: 0;}
      to {bottom: 30px; opacity: 1;}
    }
    
    @-webkit-keyframes fadeout {
      from {bottom: 30px; opacity: 1;} 
      to {bottom: 0; opacity: 0;}
    }
    
    @keyframes fadeout {
      from {bottom: 30px; opacity: 1;}
      to {bottom: 0; opacity: 0;}
    }
    </style>
<div id="content">
    <section class="padding-bottom-60">
        <div class="container">
            <!-- Payout Method -->
            <div class="pay-method">
                <div class="row">
                    <div class="col-md-12">
                        <!-- Your information -->
                        <!-- <div class="heading">
                            <h2>Order Payment Status</h2>
                            <hr>
                        </div> -->
                        <div class="row">
                            <div class="col-sm-6 col-sm-offset-3">
                                @if($order->status=='success')
                                    <img src="{{asset('image/successful.png')}}" style="height: 300px;">
                                @elseif($order->status=='failure')
                                    <img src="{{asset('image/failed.png')}}" style="height: 300px;">
                                @else
                                    <h5 style="color: yellowgreen;">Your payment is under proccessing. We will notify about your order payment stutus shortly.</h5>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4 col-sm-offset-5">
                                <a href="{{route('my_cart')}}" class="btn-round btn-light" style="float:left;border-color:#f74b16;margin-right:10px;" >Go To Cart</a>
                                <a href="{{route('my_orders')}}" class="btn-round btn-light" style="float:left;border-color:#f74b16;" >Go To Orders</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection







