@extends('front.include.main')
@section('content')
<style>
    .text {
        overflow: hidden;
        text-overflow: ellipsis;
        display: -webkit-box;
        -webkit-line-clamp: 2;
        /* number of lines to show */
        -webkit-box-orient: vertical;
    }
</style>
<style type="text/css">
    .ajax-load {
        background: #e1e1e1;
        padding: 10px 0px;
        width: 100%;
    }
</style>
<style>
    .user-rating {
        direction: rtl;
        font-size: 20px;
        unicode-bidi: bidi-override;
        padding: 10px 30px;
        display: inline-block;
    }

    .user-rating input {
        opacity: 0;
        position: relative;
        left: -15px;
        z-index: 2;
        cursor: pointer;
    }

    .user-rating span.star:before {
        color: #777777;
        content: "ï€†";
        /*padding-right: 5px;*/
    }

    .user-rating span.star {
        display: inline-block;
        font-family: FontAwesome;
        font-style: normal;
        font-weight: normal;
        position: relative;
        z-index: 1;
    }

    .user-rating span {
        margin-left: -15px;
    }

    .user-rating span.star:before {
        color: #777777;
        content: "\f006";
        /*padding-right: 5px;*/
    }

    .user-rating input:hover+span.star:before,
    .user-rating input:hover+span.star~span.star:before,
    .user-rating input:checked+span.star:before,
    .user-rating input:checked+span.star~span.star:before {
        color: #ffd100;
        content: "\f005";
    }

    .selected-rating {
        color: #ffd100;
        font-weight: bold;
        font-size: 3em;
    }
</style>
<meta name="csrf-token" content="{{ csrf_token() }}" />
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
<!-- Linking -->
<div class="linking">
    <div class="container">
        <ol class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li class="active">Review</li>
        </ol>
    </div>
</div>

<!-- Content -->
<div id="content">

    <!-- Products -->
    <section class="padding-top-40 padding-bottom-60">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    @php $img=json_decode($data->image);  @endphp
                    @php if(count($img) > 0){
                    $images=$img[0];
                    }
                    else {
                    $images='';
                    }
                    @endphp
                   
                    <img src={{URL::asset('image/product/'.$images)}} style="height:80px; width:80px;border:2px solid #5c5656	;border-radius:5px;">
                    <h6>{{ $data->name }}</h6>
                </div>
            </div>
            <div class="row">
                <!-- Products -->
                <div class="col-md-12" style="align-items:center;text-align:center;">
                    <form method="post" action="{{route('submit_review')}}" enctype="multipart/form-data"
                        class="uk-form-stacked task-form">
                        @csrf
                        <div class="row">
                            <div class="col-md-12">
                                <input type="hidden" name="product_id" value="{{ $data->id }}">
                                <span class="user-rating">
                                    <input type="radio" name="rating" value="5"><span class="star"></span>

                                    <input type="radio" name="rating" value="4"><span class="star"></span>

                                    <input type="radio" name="rating" value="3"><span class="star"></span>

                                    <input type="radio" name="rating" value="2"><span class="star"></span>

                                    <input type="radio" name="rating" value="1"><span class="star"></span>
                                </span>
                            </div>
                        </div>
                        <div class="row">       
                            <div class="col-md-12">
                                <textarea class="form-control" name="review" rows="10" cols="50" required></textarea>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="col-md-6">
                                <button type="submit" class="btn-round" style="float:right;background-color:#f74b16;">Submit Review</button>
                            </div>
                        </div>
                    </form><br>
                 
                </div>
            </div>
        </div>
    </section>
</div>
<script>
    $('#user-rating-form').on('change', '[name="rating"]', function () {
        $('#selected-rating').text($('[name="rating"]:checked').val());
    });
</script>
@endsection