@extends('front.include.main')
@section('content')
<div id="content">

    <!-- Linking -->
    
    <input type="hidden" value="{{url('/')}}" id="base_url">
    <input type="hidden" id="previous_url" value="{{URL::previous()}}">
    
    <!-- Blog -->
    <section class="login-sec padding-top-30 padding-bottom-100">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                </div>
                <div class="col-md-4">
                    <!-- Login Your Account -->
                    <h5 style="text-align:center;">Reset Password</h5>
                    <!-- FORM -->
                    {{-- <div style="text-align:center;">
                    <img src="{{URL::asset('frontend/main-files/html/images/sports.jpeg')}}" style="max-width:150px;margin-left:7%;">
                    </div> --}}
                    <form enctype="multipart/form-data" id="password">
                        <ul class="row">
                             
                            <li class="col-sm-12">
                                <input type="hidden" value="{{decrypt(request()->route('id'))}}" name="id">
                                <label>Enter Password
                                <input type="password" id="txtNewPassword" placeholder="Enter passward" name="pass" class="form-control" required>
                                </label>
                                <label>Confirm Password
                                <input type="password" id="txtConfirmPassword" placeholder="Confirm Passward" name="confpass"  class="form-control" required>
                                </label>
                                <div class="registrationFormAlert" style="color:#f74b16;" id="CheckPasswordMatch">
                            </li>
                            <li class="col-sm-12 text-left">
                                <button type="submit" style="width:100%;" class="btn-round" id="sub" disabled>Submit</button><br><br>
                            </li>
                        </ul>
                    </form>
                </div>
                <div class="col-md-4">
                </div>
            </div>
    </section>

   

    
</div>
<div id="snackbar"></div>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
<script>
    //Register user
    $('#password').submit(function (e) {
        e.preventDefault();
        var form = $('#password')[0];
        var data = new FormData(form);
        var baseurl = $("#base_url").val();
        var url = baseurl + "/auth_check";
        $.ajax({
            type: "POST",
            url: baseurl + '/api/update_password',
            enctype: 'multipart/form-data',
            data: data,
            processData: false,
            contentType: false,
            cache: false,
            success: function (data) {
                if (data.status == 200) {
                    var x = document.getElementById("snackbar");
                    $("#snackbar").text(data.message);
                    x.className = "show";
                    setTimeout(function () {
                        x.className = x.className.replace("show", "");
                    }, 5000,window.location="{{ route('user_login') }}");
                  
                } else {
                    var x = document.getElementById("snackbar");
                    $("#snackbar").text(data.message);
                    x.className = "show";
                    setTimeout(function () {
                        x.className = x.className.replace("show", "");
                    }, 3000);
                    
                }
            }
        });
    });
</script>
<script>
    function checkPasswordMatch() {
        var password = $("#txtNewPassword").val();
        var confirmPassword = $("#txtConfirmPassword").val();
        if(password.length < 8){
            $("#CheckPasswordMatch").html("Passwords should be greater then or equal 8 character!");
        }
        else{

            if (password != confirmPassword){
            $("#CheckPasswordMatch").html("Passwords does not match!");
            }

        else{
            $("#CheckPasswordMatch").html("Passwords match.");
            $("#sub").attr("disabled", false);
        }
       
    }
}
    $(document).ready(function () {
       $("#txtConfirmPassword").keyup(checkPasswordMatch);
    });
    </script>
@endsection