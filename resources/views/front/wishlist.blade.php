@extends('front.include.main')
@section('content')

<meta name="csrf-token" content="{{ csrf_token() }}" />
<!-- Content -->
<div id="content">
  <!-- Products -->
  <section class="padding-top-40 padding-bottom-60">
    <div class="container">
      <div class="row">        

        <!-- Products -->
        <div class="col-md-12">
          <!-- Short List -->
          <div class="short-lst">
            <h2>Wishlist Product</h2>            
          </div>

          <div id="product_list">
            @if (count($products) > 0)          
                @foreach ($products as $product)
                @php
                $img=json_decode($product->image,true);
                if(count($img) > 0){$images=$img[0]; }
                else {$images='';}
                @endphp
            <!-- Items -->
            @php $p_id=encrypt($product->id); @endphp
            <a href="{{ route('product_info',['id'=>$product->slug_name]) }}">
              <div class="item-col-4">
                <!-- Product -->
                <div class="product">
                  <article id="row{{$product->id}}"> <img class="img-responsive inner-img" src="{{ URL::asset('image/product/'.$images) }}"
                      style="height:200px;" alt=""> {{--<span class="sale-tag">-25%</span>--}}

                    <!-- Content -->
                    <span class="tag"></span> <a href="#." class="tittle">{{ $product->name }}</a>
                    <!-- Reviews -->
                    <p class="rev">
                      @php $rating=$product->star_rating; @endphp
                      @for ($i =0 ; $i < 5 ; $i++)
                        <i class=" @if($i < $rating) fa fa-star @else fa fa-star-o @endif"></i>
                      @endfor                
                      <span class="margin-left-10">{{  $product->review_count  }}  Review(s)</span>
                  </p>
                  
                
                    @if ($product->variant_status=='yes')
                      @php $variant=json_decode($product->variant); @endphp
                      @foreach ($variant as $key => $value)
                        @foreach($value as $v=>$w)
                          @foreach($w as $k=>$p)
                            @if($k==0)
                              @php $price=$product->new_price; $quantity=$p->quantity; @endphp
                            @endif
                          @endforeach
                        @endforeach
                      @endforeach

                        <div class="price"><i class="fa fa-inr" aria-hidden="true"></i>{{number_format($price,2)  }} 
                        </div>
                    @else
                        @php $quantity=$product->quantity;  @endphp
                        <div class="price"><i class="fa fa-inr" aria-hidden="true"></i>{{ number_format($product->new_price,2) }}
                        </div>
                    @endif<br>

                    {{--quantity  --}}
                    @if($quantity>0)
                        <p class='floatL'>Availability: <span class="in-stock">In stock</span></p>
                    @else
                        <p class='floatL'>Availability: <span class="in-stock" style="color:red;">Out of stock</span></p>                       
                    @endif

                    <a href="javascript:;" class="remove floatR" onclick=remove_wishlist('{{$product->id}}','{{$product->wishlistID}}')><i class="fa fa-2x fa-trash"></i></a>

                  </article>
                </div>

              </div>
            </a>
            @endforeach

            @else 
            <div class="row" style="text-align: center;">
              <div class="col-md-12">
                <img src="{{ URL::asset('image/error-404.png') }}"><br>
                <h6>No Product Found !</h6>
              </div>
            </div>
            @endif
          </div>
          {{$products->links()}}
        </div>
      </div>
    </div>
  </section>
  

</div>


<div id="snackbar"></div>


@endsection

@section('js')
<script>

    function remove_wishlist(row,id){
     
        $.ajax({
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            url: "{{route('remove_to_wishlist')}}",
            type: 'POST',
            enctype: 'multipart/form-data',
            data: {id: id},
            success: function (data) {
                if (data.status == 'success') {                    
                    $("#snackbar").text(data.message);
                    $("#row"+row).remove();
                    window.location.reload();
                } 
                else 
                {
                    $("#snackbar").text(data.message);
                }

                var x = document.getElementById("snackbar");
                x.className = "show";
                setTimeout(function(){ x.className = x.className.replace("show", "");window.location.reload(); }, 3000);
            }
        });
    }

</script>
@endsection

