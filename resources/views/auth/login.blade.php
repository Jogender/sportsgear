<!doctype html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Remove Tap Highlight on Windows Phone IE -->
    <meta name="msapplication-tap-highlight" content="no"/>

    <link rel="icon" type="image/png" href="{{ URL::asset('/html/dist/assets/img/favicon-16x16.png')}}" sizes="16x16">
    <link rel="icon" type="image/png" href="{{ URL::asset('/html/dist/assets/img/favicon-32x32.png')}}" sizes="32x32">

    <title>{{ config('app.name') }} - Login Page</title>

    <link href='http://fonts.googleapis.com/css?family=Roboto:300,400,500' rel='stylesheet' type='text/css'>

    <!-- uikit -->
    <link rel="stylesheet" href="{{ URL::asset('/html/dist/bower_components/uikit/css/uikit.almost-flat.min.css')}}"/>

    <!-- altair admin login page -->
    <link rel="stylesheet" href="{{ URL::asset('/html/dist/assets/css/login_page.min.css')}}" />

</head>
<body class="login_page">

    <div class="login_page_wrapper">
        <div class="md-card" id="login_card">
            <div class="md-card-content large-padding" id="login_form">
                <div class="login_heading">
                    <img class="logo_regular" src="{{ URL::asset('image/sports.jpeg')}}"  height="15" width="60%"/>
                    <!-- <div class="user_avatar"></div>  -->
                    <!-- <h3>Laundry Management</h3> -->
                   
                </div>
                <form method="POST" action="{{ route('login') }}">
                    @csrf
                    <div class="uk-form-row">
                        <label for="login_username">Email</label>
                        <input class="md-input{{ $errors->has('email') ? ' is-invalid' : '' }}"  id="email" type="email"  name="email" required autofocus  />
                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                    </div>
                    <div class="uk-form-row">
                        <label for="login_password">Password</label>
                        <input class="md-input{{ $errors->has('password') ? ' is-invalid' : '' }}" id="password" type="password"  name="password" required />
                                @if ($errors->has('password'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                                @endif
                    </div>
                    <div class="uk-margin-medium-top">
                        <button type="submit"  class="md-btn md-btn-success md-btn-wave-light waves-effect waves-button waves-light">{{ __('Login') }}</button>
                    </div>
                    
                </form>
            </div>                        
        </div>        
    </div>

    <!-- common functions -->
    <script src="   assets/js/common.min.js')}}"></script>
    <!-- uikit functions -->
    <script src="{{ URL::asset('/html/dist/assets/js/uikit_custom.min.js')}}"></script>
    <!-- altair core functions -->
    <script src="{{ URL::asset('/html/dist/assets/js/altair_admin_common.min.js')}}"></script>

    <!-- altair login page functions -->
    <script src="{{ URL::asset('/html/dist/assets/js/pages/login.min.js')}}"></script>

    <script>
        // check for theme
        if (typeof(Storage) !== "undefined") {
            var root = document.getElementsByTagName( 'html' )[0],
                theme = localStorage.getItem("altair_theme");
            if(theme == 'app_theme_dark' || root.classList.contains('app_theme_dark')) {
                root.className += ' app_theme_dark';
            }
        }
    </script>

</body>
</html>