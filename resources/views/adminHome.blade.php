@extends('admin.layouts.main')
@section('content')
    <div id="page_content_inner">       
        <div class="uk-grid" data-uk-grid-margin>
            <div class="uk-width-medium-2-10">
                <div class="md-card">
                    <div class="md-card-content">
                        <h4 class="heading_a uk-margin-bottom">Total Revenue</h4>
                        <label>  <strong>₹ {{number_format($total_amount[0]->amount + $total_amount[0]->dl_charge ,2 )}}</strong>  </label>
                    </div>
                </div>
            </div>

            <div class="uk-width-medium-2-10">
                <div class="md-card">
                    <div class="md-card-content">
                        <h4 class="heading_a uk-margin-bottom">Today Revenue</h4>
                        <label>  <strong>₹ {{number_format($today_amount[0]->amount + $today_amount[0]->dl_charge ,2 )}}</strong>  </label>

                    </div>
                </div>
            </div> 

            <div class="uk-width-medium-2-10">
                <div class="md-card">
                    <div class="md-card-content">
                        <h4 class="heading_a uk-margin-bottom">Total User</h4>
                        <label> <i class="material-icons"></i>  <strong>{{$counts[0]->users}}</strong>  </label>
                    </div>
                </div>
            </div>

            <div class="uk-width-medium-2-10">
                <div class="md-card">
                    <div class="md-card-content">
                        <h4 class="heading_a uk-margin-bottom">Total Orders</h4>
                        <label> <i class="material-icons">receipt</i>  <strong>{{$counts[0]->total_order}}</strong>  </label>
                    </div>
                </div>
            </div> 

            <div class="uk-width-medium-2-10">
                <div class="md-card">
                    <div class="md-card-content">
                        <h4 class="heading_a uk-margin-bottom">Today Orders</h4>
                        <label> <i class="material-icons">receipt</i>  <strong>{{$counts[0]->today_order}}</strong>  </label>
                    </div>
                </div>
            </div>            
        </div>

        <h4 class="heading_a uk-margin-bottom">Today Orders</h4>        
        <div class="md-card uk-margin-medium-bottom">
            <div class="md-card-content">
                <div class="dt_colVis_buttons"></div>

                <table id="dt_default" class="uk-table uk-table-striped uk-table-hover" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>Order ID</th>
                            <th>Name</th>
                            <th>Mobile</th>
                            <th>Date</th>
                            <th>Payment</th>
                            <th>Amount</th>
                            <th>TXN ID</th>
                            <th> Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody id="ajaxresponse">
                        @if(!$data->isEmpty())
                            @foreach($data as $key)
                                <tr id="{{ $key->id }}">
                                    <td> <a href="{{ route('order_details',['id'=>encrypt($key->id)]) }}"><b>{{ strtoupper($key->order_id) }}</b></a> </td>
                                    <td> {{ $key->name }} </td>
                                    <td> {{ $key->phone }} </td>
                                    <td>
                                        @if(is_null($key->date))
                                            {{ date('d-m-Y') }}
                                        @else
                                            {{ date('d-m-Y',strtotime($key->date)) ?? '' }}
                                        @endif
                                    </td>
                                    <td>
                                        <span class="uk-badge">{{ $key->payment_type }}</span>
                                    </td>
                                    <td><i class="uk-icon-inr"></i>{{ $key->amount }}</td>
                                    <td>{{ $key->transaction_id }}</td>
                                    <td id="td{{$key->id}}">
                                        @if($key->tracking_status == "Pending")
                                        <span class="uk-badge uk-badge-warning" ><a href="javascript:void(0);" class="ordered" data-toggle="modal" style="cursor: pointer;"  data-id="{{ $key->id }}" data-status="{{ $key->tracking_status }}">Pending</a></span>
                                        @elseif($key->tracking_status == "Accepted")
                                        <span class="uk-badge md-bg-deep-purple-300" ><a href="javascript:void(0);" class="ordered" data-toggle="modal" style="cursor: pointer;"  data-id="{{ $key->id }}" data-status="{{ $key->tracking_status }}">Shipped</a></span>
                                        @elseif($key->tracking_status == "Packed")
                                        <span class="uk-badge md-bg-blue-300" ><a href="javascript:void(0);" class="ordered" data-toggle="modal" style="cursor: pointer;"  data-id="{{ $key->id }}" data-status="{{ $key->tracking_status }}">Processing</a></span>
                                        @elseif($key->tracking_status == "Dispatched")
                                        <span class="uk-badge md-bg-light-green-300" ><a href="javascript:void(0);" class="ordered" data-toggle="modal" style="cursor: pointer;"  data-id="{{ $key->id }}" data-status="{{ $key->tracking_status }}">Out for delivery</a></span>
                                        @elseif($key->tracking_status == "Delivered")
                                        <span class="uk-badge md-bg-green-A400" ><a href="javascript:void(0);" class="ordered" data-toggle="modal" style="cursor: pointer;"  data-id="{{ $key->id }}" data-status="{{ $key->tracking_status }}">Delivered</a></span>
                                        @elseif($key->tracking_status == "Cancelled")
                                        <span class="uk-badge md-bg-red-300" ><a href="javascript:void(0);" class="ordered" data-toggle="modal" style="cursor: pointer;"  data-id="{{ $key->id }}" data-status="{{ $key->tracking_status }}">Cancelled</a></span>
                                        @else
                                        <span class="uk-badge md-bg-blue-grey-300" ><a href="javascript:void(0);" class="ordered" data-toggle="modal" style="cursor: pointer;"  data-id="{{ $key->id }}" data-status="{{ $key->tracking_status }}">Rejected</a></span>
                                        @endif
                                    </td>
                                  
                                    <td>
                                        <a href="{{ route('order_details',['id'=>encrypt($key->id)]) }}" data-uk-tooltip title="View User Details"><i class="uk-icon-eye uk-icon-small"></i></a>
                                                                               
                                        &nbsp;&nbsp;
                                        <a href="{{ route('view_invoice',['id'=>$key->id]) }}" target="_blank" data-uk-tooltip title="View Invoice">
                                            <i class="uk-icon-file-text md-color-green-600 uk-icon-small"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="9">No Record Found</td>
                            </tr>
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
        
    </div>

    <div class="uk-modal" id="modal_header_footer">
        <div class="uk-modal-dialog">
            <div class="uk-modal-header">
                <h3 class="uk-modal-title">Update Status </h3>
            </div>
            <div class="uk-modal-body">
                <div class="uk-grid">
                    <input type="hidden" name="id" value="" id="id" />
                    <div class="uk-width-medium-1-1 select_status">
                        <select id="val_select" class="category" name="status" required  data-md-selectize>
                            <option value="">Choose Status</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="uk-modal-footer uk-text-right">
                <button type="button" class="md-btn md-btn-flat uk-modal-close">Close</button>
                <button type="button" onclick="order_status()" class="md-btn md-btn-flat md-btn-flat-primary">Save</button>
            </div>
        </div>
    </div>
@endsection
@section('js')
<script src="{{ URL::asset('backend/assets/js/common.min.js') }}"></script>

<script type="text/javascript">
    $(document).ready(function () {
        // $('#dt_default').dataTable({
        //     "bPaginate": true,
        //     "bLengthChange": false,
        //     "bFilter": true,
        //     "bInfo": false,
        //     "bAutoWidth": false
        // });

        $("#dt_default").on("click", ".ordered", function() {
            var id = $(this).data('id');
            var status = $(this).data('status');
            

            $('#id').val(id);
            var html = '    <option value="" required disabled selected>Choose Status</option>\n' +
                    '    <option>Pending</option>\n' +
                    '    <option>Accepted</option>\n' +
                    '    <option>Packed</option>\n' +
                    '    <option>Dispatched</option>\n' +
                    '    <option>Delivered</option>\n' +
                    '    <option>Cancelled</option>\n' +
                    '    <option>Rejected</option>';

            var control = $('#val_select').get(0).selectize;
              control.destroy();
            $('#val_select').html(html);
            $('#val_select').selectize();

            $('#val_select').get(0).selectize.setValue(status);
            UIkit.modal('#modal_header_footer').show();
        });
    });

    

    function order_status()
    {
        var id = $('#id').val();
        var value = $('#val_select').val(); 
        UIkit.modal('#modal_header_footer').hide();
        var track_value='';
        if(value== 'Dispatched')
        {
            UIkit.modal.prompt('Tracking No:', '', function(track){
                change_order_status(id,value,track);
            });
        }
        else{
            change_order_status(id,value,'');
        }
    }

    function change_order_status(id,value,track_value){
        $.ajax({
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            type: "POST",
            url: "{{ route('change_order_status') }}",
            data: {id: id,value:value,track:track_value,'_token': '{!!csrf_token()!!}'},
            cache: false,
            success: function (data) {
                if (data.status == "true")
                {
                    UIkit.notify({
                        message: '<i class="uk-icon-check" style="color:white;"></i>&nbsp;&nbsp;'+data.message+'!!..',
                        status: 'success',
                        timeout: 5000,
                        group: null,
                        pos: 'top-center'
                    });
                    location.reload();
                }
                else
                {
                    UIkit.notify({
                        message: '<i class="uk-icon-check" style="color:white;"></i>&nbsp;&nbsp;'+data.message+'!!..',
                        status: 'error',
                        timeout: 5000,
                        group: null,
                        pos: 'top-center'
                    });
                    location.reload();
                }
            },
            error: function () {
                UIkit.notify({
                    message: '<i class="material-icons" style="color:white;">error</i>&nbsp;&nbsp;Something Went Wrong!!..',
                    status: 'danger',
                    timeout: 5000,
                    group: null,
                    pos: 'top-center'
                });
            }
        });
    }

    function getOrders()
    {
        var status = $('#ordStatus').val(); 
        $.ajax({
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            type: "POST",
            url: "{{ route('order_status') }}",
            data: {status: status,'_token': '{!!csrf_token()!!}'},
            cache: false,
            success: function (data) {
                $("#ajaxresponse").html('');
                $("#ajaxresponse").html(data);
            },
            error: function () {
                UIkit.notify({
                    message: '<i class="material-icons" style="color:white;">error</i>&nbsp;&nbsp;Something Went Wrong!!..',
                    status: 'danger',
                    timeout: 5000,
                    group: null,
                    pos: 'top-center'
                });
            }
        });
    }

    

    $(function() {
        // date range
        altair_form_adv.date_range();
    });
    altair_form_adv = {
        date_range: function () {
            var $dp_start = $('#uk_dp_start'),
                $dp_end = $('#uk_dp_end');

            var start_date = UIkit.datepicker($dp_start, {
                format: 'DD-MM-YYYY'
            });

            var end_date = UIkit.datepicker($dp_end, {
                format: 'DD-MM-YYYY'
            });

            $dp_start.on('change', function () {
                end_date.options.minDate = $dp_start.val();
            });

            $dp_end.on('change', function () {
                start_date.options.maxDate = $dp_end.val();

                var from = $dp_start.val();
                var to = $dp_end.val();
                $.ajax({
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    type: "POST",
                    url: "{{ route('order_status') }}",
                    data: {from: from,to:to,'_token': '{!!csrf_token()!!}'},
                    cache: false,
                    success: function (data) {
                        $("#ajaxresponse").html('');
                        $("#ajaxresponse").html(data);
                    },
                    error: function () {
                        UIkit.notify({
                            message: '<i class="material-icons" style="color:white;">error</i>&nbsp;&nbsp;Something Went Wrong!!..',
                            status: 'danger',
                            timeout: 5000,
                            group: null,
                            pos: 'top-center'
                        });
                    }
                });
                // alert($dp_end.val())
            });
        }
    };
</script>
@endsection

