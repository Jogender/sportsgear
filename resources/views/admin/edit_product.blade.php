@extends('admin.layouts.main')

@section('content')
<div id="page_content_inner">
    <h2 class="heading_b uk-margin-bottom">Edit Product</h2>
    <div class="md-card uk-margin-large-bottom">
        <div class="md-card-content">
            <form method="post" action="{{route('update_product')}}" id="form_validation" enctype="multipart/form-data"
                class="uk-form-stacked task-form">
                @csrf
                
                {{-- Image remover --}}
                <input  type="hidden" name="file_id" id="file_id"  value=""  />
                <input  type="hidden" name="remove_id" id="remove_id"  value=""  />

                <input type="hidden" id="id" name="id" value="{{$data->id}}" />
                

                @if($data->variant_status == 'yes')
                    @php $variantDetail=json_decode($data->variant);  $keySize=sizeof(json_decode(json_encode($variantDetail),true));  $varientLength=0; @endphp                    
                    <input type="hidden" name="keyIds" id="keyIds" value="{{$keySize}}" /> 
                    @foreach($variantDetail as $key => $value)
                            @php $keyCount='keyCount'.++$key;  @endphp   
                        @foreach($value as $k=>$v)
                            @php $valueCount= sizeof(json_decode(json_encode($v),true));  $varientLength=$valueCount + $varientLength;  @endphp
                            <input type="hidden" name={{$keyCount}} id="key1" value={{$valueCount}} /> 
                        @endforeach
                    @endforeach

                    <input type="hidden" name="valueIds" id="valueIds" value="{{$varientLength}}" /> 
                @else 
                <input type="hidden" name="valueIds" id="valueIds" value="1" /> 
                    
                @endif    

                <div id="keyCountDiv">


                </div>

                <div class="uk-width-medium-1-1">
                    <label for="fullname">Name<span class="req">*</span></label>
                    <div class="parsley-row">
                        <input type="text" name="name" id="name"  value="{{$data->name}}" autocomplete="off" placeholder="Enter name" required class="md-input task" />
                    </div>
                </div>

                <div class="uk-width-medium-1-1">
                    <label for="fullname">Slug<span class="req">*</span></label>
                    <div class="parsley-row">
                        <input type="text" name="slug" id="slug" value="{{$data->slug_name}}" autocomplete="off" placeholder="Enter Slug" required class="md-input task" />
                    </div>
                </div>

                <div class="uk-grid" data-uk-grid-margin>
                    <div class="uk-width-medium-1-1">
                        <label for="fullname">Category<span class="req">*</span></label>
                        <div class="parsley-row">
                            <select id="category" class="category md-input" name="category_id" required>
                                <option value="">Choose Category</option>
                                @foreach($category as $keyc)
                                    <option @if($data->category == $keyc->id) selected @endif value="{{ $keyc->id }}">{{ $keyc->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="uk-width-medium-1-1">
                        <label for="fullname">SubCategory<span class="req">*</span></label>
                        <div class="parsley-row">
                            <select id="subcategory" class="subcategory md-input" name="subcategory_id" >
                                <option value="">Choose Subcategory</option>
                                @foreach($subcategory as $subValue)
                                    <option @if($data->subcategory == $subValue->id) selected @endif value="{{ $subValue->id }}">{{ $subValue->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="uk-width-medium-1-1">
                        <label for="fullname">SubSubCategory</label>
                        <div class="parsley-row">
                            <select id="subsubcategory" class="subsubcategory md-input" name="subsubcategory_id" >
                                <option value="">Choose SubSubcategory</option>
                                @foreach($subsubcategory as $subsubValue)
                                    <option @if($data->subsubcategory == $subsubValue->id) selected @endif value="{{ $subsubValue->id }}">{{ $subsubValue->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
              

                    <div class="uk-width-medium-1-1">
                        <label for="fullname">Description<span class="req">*</span></label>
                        <div class="parsley-row">
                            {{-- <textarea  class="summernote" cols="20" rows="8" name="short_description"></textarea> --}}
                            <textarea id="wysiwyg_ckeditor" name="short_description" cols="30" rows="20">{{$data->short_description}}</textarea>
                        </div>
                    </div>
                    
                    <div class="uk-width-medium-1-1" id="description" @if($data->variant_status == 'no') style="display:block;" @endif>
                        <label for="fullname">Short Description</label>
                        <div class="parsley-row">
                            <textarea id="rohi" name="description" cols="30" rows="20"> @if($data->variant_status == 'no')  {{$data->description}} @endif</textarea>
                        </div>
                    </div>   
                    

                    <div class="uk-width-medium-1-1">
                        <div class="md-card">
                            <div class="md-card-content">
                                <h3 class="heading_a uk-margin-small-bottom">
                                    Upload Image<span class="req">*</span>
                                </h3>

                                @php $files=json_decode($data->image); @endphp
                                @foreach ($files as $key=>$file)
                                    <div class="file col-sm-2 text-center" id="image_div{{ $key  }}">
                                        <a target="_blank" href="{{ URL::asset('image/'.$file) }}">
                                            <img src="{{ URL::asset('image/product/'.$file) }}"  style="width:150px"> 
                                            <a href="javascript:;" onclick=remove_image('{{ $file }}','{{ $key }}','')   title="Remove"><i class="material-icons">clear</i> Remove</a>
                                        </a>
                                    </div>
                                @endforeach

                                </br>
                                <div style="display:none;" id="image_uploader">
                                    <input type="file" name="files" id="files" accept=".png, .jpg, .jpeg"  />
                                </div>
                                <a href="javascript:;" onclick="open_file()" class="image-button"><i class="icon-picture"></i> File</a>

                            </div>
                        </div>
                    </div>                    
                    
                    <div class="row">
                        <div class="col-md-6">
                            <div class="uk-width-medium-1-1">
                                <label for="fullname">Featured<span class="req">*</span></label>
                                <div class="parsley-row">
                                    <input type="checkbox" @if($data->featured == 1) checked @endif data-switchery data-switchery-color="#76C804;" name="feature" value="1" id="switch_demo" />
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="uk-width-medium-1-1" style="float:right;">
                                <label for="fullname">Top Weekly<span class="req">*</span></label>
                                <div class="parsley-row">
                                    <input type="checkbox" name="weekly" @if($data->topweekly == 1) checked @endif data-switchery data-switchery-color="#76C804;" name="feature" value="1" id="switch_demo" />
                                </div>
                            </div>
                        </div>
                    </div> 
                   
                    <div class="uk-width-medium-1-1">
                        <label for="fullname">Brand<span class="req">*</span></label>
                        <div class="parsley-row">
                            <select id="brand" class="brand md-input" name="brand" required>
                                <option value="">Choose Brand</option>
                                @foreach($brand as $brd)
                                    <option @if($data->brand == $brd->id) selected @endif value="{{ $brd->id }}">{{ $brd->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="uk-width-medium-1-1">
                        <label for="fullname">Product of origin<span class="req">*</span></label>
                        <div class="parsley-row">
                            <input type="text" name="origin" autocomplete="off" placeholder="Enter product of origin" required class="md-input task mobile" value="{{$data->origin}}" />
                        </div>
                    </div>


                    <div class="uk-width-medium-1-1">
                        <label for="fullname">Variant<span class="req">*</span></label>
                        <div class="parsley-row">
                            <div class="uk-width-medium-3-5">
                                <input type="radio" name="variant_status" id="radio_demo_inline_1123" value="yes" @if($data->variant_status == 'yes') checked  @endif  />
                                <label for="radio_demo_inline_1123" class="inline-label">Yes</label>
                                <input type="radio" name="variant_status" id="radio_demo_inline_2123" value="no"  @if($data->variant_status == 'no') checked  @endif   />
                                <label for="radio_demo_inline_2123" class="inline-label" >No</label>
                            </div>
                        </div>
                    </div>

                    <div class="uk-width-medium-1-1" id="old_price"  @if($data->variant_status == 'yes') style="display:none;"  @endif >
                        <label for="fullname">MRP Price</label>
                        <div class="parsley-row">
                            <input type="text" name="old_price" autocomplete="off" placeholder="Enter Old Price" value="{{$data->old_price}}"  class="md-input task mobile" />
                        </div>
                    </div> 

                    <div class="uk-width-medium-1-1" id="new_price" @if($data->variant_status == 'yes') style="display:none;"  @endif>
                        <label for="fullname">Offer Price</label>
                        <div class="parsley-row">
                            <input type="text" name="new_price" autocomplete="off" placeholder="Enter New Price" value="{{$data->new_price}}" class="md-input task mobile" />
                        </div>
                    </div> 
                    <div class="uk-width-medium-1-1" id="quantity" @if($data->variant_status == 'yes') style="display:none;"  @endif>
                        <label for="fullname">Quantity</label>
                        <div class="parsley-row">
                            <input type="text" name="quantity" autocomplete="off" placeholder="Enter quantity" value="{{$data->quantity}}"  class="md-input task mobile" />
                        </div>
                    </div> 

                    <div class="uk-width-medium-1-1" id="tax">
                        <label for="tax">GST Tax(%)</label>
                        <div class="parsley-row">
                            <input type="number" name="tax" autocomplete="off" min="0" value="{{$data->tax}}"
                                placeholder="Enter Tax(%)" 
                                class="md-input task mobile" />
                        </div>
                    </div> 
                   
                   
                </div>

                @if($data->variant_status == 'yes')

                    @php $variantDetail=json_decode($data->variant);  $variantLoop=0; @endphp                    
                    @foreach($variantDetail as $key => $value)
                    @php $valuekey= ++$key;   @endphp

                    <div class="variants-wrapper" id="keyIds{{$valuekey}}">
                        <div class="variants field_wrapper">
                            <hr class="form_hr">
                            <div class="row">
                                <div class="md-card">
                                    <div class="md-card-content">
                                        @foreach($value as $k=>$v)
                                            @foreach($v as $l=>$w)
                                            <div class="uk-grid" id="variant{{++$variantLoop}}" data-uk-grid-margin>
                                              
                                                @if($l == 0 ) 
                                                    <div class="uk-width-medium-1-6">
                                                        <label for="fullname">Key<span>*</span></label>
                                                        <div class="parsley-row">
                                                            <input type="text" name="key{{$valuekey}}[]" autocomplete="off" value="{{$k}}" placeholder="Enter key"  class="md-input mobile" />
                                                        </div>
                                                    </div>
                                                @endif

                                                @if($l == 1 )  
                                                    @if($valuekey == 1)                                                             
                                                        <div class="uk-width-medium-1-6">
                                                            <a href="javascript:void(0);" class="add_value" onclick="addkey('key{{$valuekey}}')" title="Add key"><i class="fa fa-plus"> Add Keys </i></a>
                                                        </div>
                                                    @else 
                                                        <div class="uk-width-medium-1-6">
                                                            <a href="javascript:void(0);" class="add_value" onclick="removeKey('key{{$valuekey}}')" title="Remove key"><i class="fa fa-plus"> Remove Key </i></a>
                                                        </div>  
                                                    @endif
                                                @endif

                                                @if($l > 1) 
                                                    <div class="uk-width-medium-1-6">                                                                                                            
                                                    </div> 
                                                @endif

                                                <div class="uk-width-medium-1-6">
                                                    <label for="fullname">Value</label>
                                                    <div class="parsley-row">
                                                        <input type="text" name="key{{$valuekey}}value{{++$l}}[]" value="{{$w->value}}" autocomplete="off" placeholder="Enter Value"  class="md-input mobile" />
                                                    </div>                                                
                                                </div>  
        
                                                <div class="uk-width-medium-1-6">
                                                    <label for="fullname">Price</label>
                                                    <div class="parsley-row">
                                                        <input type="text" name="key{{$valuekey}}price{{$l}}[]" value="{{$w->price}}" autocomplete="off" placeholder="Enter Price"  class="md-input mobile" />
                                                    </div>                                                
                                                </div> 
                
                                                <div class="uk-width-medium-1-6">
                                                    <label for="fullname">Quantity</label>
                                                    <div class="parsley-row">
                                                        <input type="text" name="key{{$valuekey}}quantity{{$l}}[]" value="{{$w->quantity}}" autocomplete="off" placeholder="Enter Value"  class="md-input mobile" />
                                                    </div>                                                
                                                </div>  

                                                <div class="uk-width-medium-1-6">
                                                    <label for="fullname">Price changes<span class="req">*</span></label>
                                                    <div class="parsley-row">
                                                        <input type="checkbox" @if($w->pricechange == 'on') checked  @endif   name="key{{$valuekey}}pricechange{{$l}}[]" value="on" id="switch_demo" />
                                                    </div>
                                                </div>

                                                
                                                @if($l == 1)
                                                    <div class="uk-width-medium-1-6">
                                                        <a href="javascript:void(0);" class="add_value" onclick="addValue('key{{$valuekey}}','variant{{$variantLoop}}')" title="Add field"><i class="fa fa-plus"> Add Value</i></a>
                                                    </div>
                                                @else 
                                                    <div class="uk-width-medium-1-6">
                                                        <a href="javascript:void(0);" class="add_value" onclick="removeValue('key{{$valuekey}}',variant{{$variantLoop}})" title="Add field"><i class="fa fa-plus"> Remove</i></a>
                                                    </div>
                                                @endif
                                            
                                            </div></br>
                                            
                                            @endforeach
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    @endforeach

            @else 

            <div class="variants-wrapper" id="keyIds1" style="display:none;">
                <div class="variants field_wrapper">
                    <hr class="form_hr">
                    <div class="row">
                        <div class="md-card">
                            <div class="md-card-content">
                                <div class="uk-grid" id="variant1" data-uk-grid-margin>

                                   <div class="uk-width-medium-1-6">
                                        <label for="fullname">Key<span>*</span></label>
                                        <div class="parsley-row">
                                            <input type="text" name="key1[]" autocomplete="off" placeholder="Enter key"  class="md-input mobile" />
                                        </div>
                                    </div>
                                    
                                    <div class="uk-width-medium-1-6">
                                        <label for="fullname">Value<span >*</span></label>
                                        <div class="parsley-row">
                                            <input type="text" name="key1value1[]" autocomplete="off" placeholder="Enter Value"  class="md-input mobile" />
                                        </div>                                                
                                    </div>  

                                    <div class="uk-width-medium-1-6">
                                        <label for="fullname">Price<span >*</span></label>
                                        <div class="parsley-row">
                                            <input type="text" name="key1price1[]" autocomplete="off" placeholder="Enter Price"  class="md-input mobile" />
                                        </div>                                                
                                    </div> 

                                    <div class="uk-width-medium-1-6">
                                        <label for="fullname">Quantity<span >*</span></label>
                                        <div class="parsley-row">
                                            <input type="text" name="key1quantity1[]" autocomplete="off" placeholder="Enter Quantity"  class="md-input mobile" />
                                        </div>                                                
                                    </div> 

                                    <div class="uk-width-medium-1-6">
                                        <label for="fullname">Price changes<span class="req">*</span></label>
                                        <div class="parsley-row">
                                            <input type="checkbox" name="key1pricechange1[]" value="on" id="switch_demo" />
                                        </div>
                                    </div>
                                       
                                    <div class="uk-width-medium-1-6">
                                        <a href="javascript:void(0);" class="add_value" onclick="addValue('key1','variant1')" title="Add Value"><i class="fa fa-plus"> Add Value</i></a>
                                    </div>

                                    <div class="uk-width-medium-1-6">
                                        <a href="javascript:void(0);" class="add_value" onclick="addkey('key1')" title="Add Keys"><i class="fa fa-plus"> Add Keys</i></a>
                                    </div>
                                </br>
                                    
                                </div></br>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <br>
            @endif

            <br>

                <div class="uk-grid">
                    <div class="uk-width-forms_wysiwyg_ckeditor1-1">
                        <button type="submit" class="md-btn"
                            style="background-color:#e2932d;color: white;">Submit</button>
                        <a href="{{ URL::previous() }}" type="button"
                            class="md-btn md-btn-wave waves-effect waves-button">Cancel</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>


@endsection
@section('js')     
    @include('admin.productjs')
    <script>
        CKEDITOR.replace('rohi', {
            skin: 'moono',
            enterMode: CKEDITOR.ENTER_BR,
            shiftEnterMode:CKEDITOR.ENTER_P,
            toolbar: [{ name: 'basicstyles', groups: [ 'basicstyles' ], items: [ 'Bold', 'Italic', 'Underline', "-", 'TextColor', 'BGColor' ] },
             { name: 'styles', items: [ 'Format', 'Font', 'FontSize' ] },
             { name: 'scripts', items: [ 'Subscript', 'Superscript' ] },
             { name: 'justify', groups: [ 'blocks', 'align' ], items: [ 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock' ] },
             { name: 'paragraph', groups: [ 'list', 'indent' ], items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent'] },
             { name: 'links', items: [ 'Link', 'Unlink' ] },
             { name: 'insert', items: [ 'Image'] },
             { name: 'spell', items: [ 'jQuerySpellChecker' ] },
             { name: 'table', items: [ 'Table' ] }
             ],
        });
    </script>
@endsection
