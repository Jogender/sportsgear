<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
  <head>
    <title></title>

    <!--[if !mso]>
<!-- -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!--<![endif]-->
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <style type="text/css">
      #outlook a {
        padding: 0;
      }
      .ReadMsgBody {
        width: 100%;
      }
      .ExternalClass {
        width: 100%;
      }
      .ExternalClass * {
        line-height: 100%;
      }
      body {
        margin: 0;
        padding: 0;
        -webkit-text-size-adjust: 100%;
        -ms-text-size-adjust: 100%;
      }
      table,
      td {
        border-collapse: collapse;
        mso-table-lspace: 0pt;
        mso-table-rspace: 0pt;
      }
      img {
        border: 0;
        height: auto;
        line-height: 100%;
        outline: none;
        text-decoration: none;
        -ms-interpolation-mode: bicubic;
      }
      p {
        display: block;
        margin: 13px 0;
      }
    </style>

    <!--[if !mso]>
<!-->
    <style type="text/css">
      @media only screen and (max-width:480px) {
        @-ms-viewport {
          width: 320px;
        }
        @viewport {
          width: 320px;
        }
      }
    </style>

    <!--<![endif]-->

    <!--[if mso]>
        <xml>
        <o:OfficeDocumentSettings>
          <o:AllowPNG/>
          <o:PixelsPerInch>96</o:PixelsPerInch>
        </o:OfficeDocumentSettings>
        </xml>
        <![endif]-->

    <!--[if lte mso 11]>
        <style type="text/css">
          .outlook-group-fix { width:100% !important; }
        </style>
        <![endif]-->
    <style type="text/css">
      @media only screen and (min-width:480px) {
        .mj-column-per-100 {
          width: 100% !important;
          max-width: 100%;
        }
        .mj-column-px-340 {
          width: 340px !important;
          max-width: 340px;
        }
        .mj-column-px-440 {
          width: 440px !important;
          max-width: 440px;
        }
        .mj-column-per-50 {
          width: 50% !important;
          max-width: 50%;
        }
      }
    </style>
    <style type="text/css">
      /**
       * None of these CSS stylings are guaranteed to work.
       * So be sure to allow any of these styles to fail gracefully.
       */
      body {
        background: #e8e8e8;
        margin: 0;
      }
      @media (min-width:480px) {
        body {
          margin: 24px 0 !important;
        }
      }
      a {
        /* not all email clients support color: inherit; so fall back on a fixed color */
        color: #1e1e1e;
        color: inherit;
      }
      @media only screen and (max-width:480px) {
        table.full-width-mobile {
          width: 100% !important;
        }
        td.full-width-mobile {
          width: auto !important;
        }
      }
      /*
       * This outfit-formula styling won't work on a lot of email clients
       * Make sure to test any CSS changes here
       */
      .outfit-formula-image {
        border-radius: 8px;
        vertical-align: middle;
      }
      .outfit-formula-image-top-layer {
        margin-right: -16px;
        position: relative;
        box-shadow: 0 5px 16px 0 rgba(0, 0, 0, 0.12);
      }
      .order-table .item>td {
        vertical-align: top;
        padding-top: 16px;
      }
      .order-table .item-image {
        padding-right: 16px;
      }
      .order-table .item-description,
      .order-table .item-refunded {
        color: #787878;
      }
      .order-table .item-description a {
        color: #1e1e1e;
        text-decoration: none;
        display: block;
        margin-bottom: 8px;
      }
      .order-table .price {
        text-align: right;
      }
      .order-table .item-image {
        width: 50px;
      }
      .order-table .item-price {
        width: 80px;
      }
      .order-table .item-description,
      .order-table .item-price {
        padding: 20px 0;
      }
      .order-table .pricing-row {
        border-top: 1px solid #e8e8e8;
        border-bottom: 1px solid #e8e8e8;
      }
      .order-table .pricing-row td {
        padding: 12px 0;
      }
      .order-table .total-row {
        font-weight: 600;
      }
      .order-details-footer * {
        line-height: 170% !important;
      }
      .rounded-border table,
      .rounded-border td {
        /*
        * By default, MJML doesn't let you have borders with a border-radius.
        * See https://github.com/mjmlio/mjml/issues/910 for more context.
        * Adding this style can add inconsistency across clients, so be sure
        * to test across clients if doing this.
        */
        border-collapse: unset !important;
      }
    </style>
  </head>
  <body style="-ms-text-size-adjust:100%; -webkit-text-size-adjust:100%; background:#e8e8e8; margin:0; padding:0">
    <div>

      <!--[if mso | IE]><table align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600" ><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->
      <div style="background:#ffffff;background-color:#ffffff;Margin:0px auto;max-width:600px;">
        <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:#fff; background-color:#fff; border-collapse:collapse; mso-table-lspace:0; mso-table-rspace:0; width:100%" bgcolor="#fff" width="100%">
          <tbody>
            <tr>
              <td style="border-collapse:collapse; direction:ltr; font-size:0; mso-table-lspace:0; mso-table-rspace:0; padding:24px; text-align:center; vertical-align:top" align="center" valign="top">

                <!--[if mso | IE]><table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td class="" style="vertical-align:top;width:552px;" ><![endif]-->
                <div class="mj-column-per-100 outlook-group-fix" style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
                  <table border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%" style="border-collapse:collapse; mso-table-lspace:0; mso-table-rspace:0">
                    <tbody>
                      <tr>
                        <td style="border-collapse:collapse; mso-table-lspace:0; mso-table-rspace:0; padding:0; vertical-align:top" valign="top">
                          <table border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%" style="border-collapse:collapse; mso-table-lspace:0; mso-table-rspace:0">
                            <tr>
                              <td align="center" style="border-collapse:collapse; font-size:0; mso-table-lspace:0; mso-table-rspace:0; padding:0; word-break:break-word">
                                <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="border-collapse:collapse; border-spacing:0; mso-table-lspace:0; mso-table-rspace:0">
                                  <tbody>
                                    <tr>
                                      <td style="border-collapse:collapse; mso-table-lspace:0; mso-table-rspace:0;"><img alt="Thread" src="{{asset('image/sports.jpeg')}}" style="-ms-interpolation-mode:bicubic; border:0; display:block; line-height:100%; outline:none; text-decoration:none; width:100%" width="100%"></td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </table>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>

                <!--[if mso | IE]></td></tr></table><![endif]-->
              </td>
            </tr>
          </tbody>
        </table>
      </div>

      <!--[if mso | IE]></td></tr></table><![endif]-->
      <div>

        <!--[if mso | IE]><table align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600" ><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->
        <div style="background:#ffffff;background-color:#ffffff;Margin:0px auto;max-width:600px;">
          <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:#fff; background-color:#fff; border-collapse:collapse; mso-table-lspace:0; mso-table-rspace:0; width:100%" bgcolor="#fff" width="100%">
            <tbody>
              <tr>
                <td style="border-collapse:collapse; direction:ltr; font-size:0; mso-table-lspace:0; mso-table-rspace:0; padding:24px 24px 0; text-align:center; vertical-align:top" align="center" valign="top">

                  <!--[if mso | IE]><table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td class="" style="vertical-align:top;width:552px;" ><![endif]-->
                  <div class="mj-column-per-100 outlook-group-fix" style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
                    <table border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%" style="border-collapse:collapse; mso-table-lspace:0; mso-table-rspace:0">
                      <tbody>
                        <tr>
                          <td style="border-collapse:collapse; mso-table-lspace:0; mso-table-rspace:0; padding:0; vertical-align:top" valign="top">
                            <table border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%" style="border-collapse:collapse; mso-table-lspace:0; mso-table-rspace:0">
                              <tr>
                                <td align="center" class="h2" style="border-collapse:collapse; font-size:0; mso-table-lspace:0; mso-table-rspace:0; padding:0 0 16px; word-break:break-word">
                                  <div style="font-family:Arial;font-size:18px;font-weight:600;line-height:133%;text-align:center;text-transform:uppercase;color:#1e1e1e;">Order Details</div>
                                </td>
                              </tr>
                            </table>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>

                  <!--[if mso | IE]></td></tr></table><![endif]-->
                </td>
              </tr>
            </tbody>
          </table>
        </div>

        <!--[if mso | IE]></td></tr></table><table align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600" ><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->
        <div style="background:#ffffff;background-color:#ffffff;Margin:0px auto;max-width:600px;">
          <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:#fff; background-color:#fff; border-collapse:collapse; mso-table-lspace:0; mso-table-rspace:0; width:100%" bgcolor="#fff" width="100%">
            <tbody>
              <tr>
                <td style="border-collapse:collapse; direction:ltr; font-size:0; mso-table-lspace:0; mso-table-rspace:0; padding:0 24px; text-align:center; vertical-align:top" align="center" valign="top">

                  <!--[if mso | IE]><table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td class="" style="vertical-align:top;width:552px;" ><![endif]-->
                  <div class="mj-column-per-100 outlook-group-fix" style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
                    <table border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%" style="border-collapse:collapse; mso-table-lspace:0; mso-table-rspace:0">
                      <tbody>
                        <tr>
                          <td style="border-collapse:collapse; border-top:1px solid #e8e8e8; mso-table-lspace:0; mso-table-rspace:0; padding:0; vertical-align:top" valign="top">
                            <table border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%" style="border-collapse:collapse; mso-table-lspace:0; mso-table-rspace:0">
                              <tr>
                                <td align="left" class="order-table" style="border-collapse:collapse; font-size:0; mso-table-lspace:0; mso-table-rspace:0; padding:0; word-break:break-word">
                                  <table cellpadding="0" cellspacing="0" width="100%" border="0" style="border-collapse:collapse; cellspacing:0; color:#1e1e1e; font-family:Arial; font-size:13px; line-height:133%; mso-table-lspace:0; mso-table-rspace:0; table-layout:auto; width:100%">
								 

								  	@php $details=json_decode($order->product_detail); $sum = 0; @endphp
		  							@foreach($details as $key)
									  @foreach($products as $product) @if($key->product_id==$product->id) @php $prod=$product; @endphp @endif @endforeach
									  @php $images=json_decode($prod->image); @endphp
									<tr class="item">
                                      <td class="item-image" style="border-collapse:collapse; mso-table-lspace:0; mso-table-rspace:0; padding-right:16px; padding-top:16px; vertical-align:top; width:50px" width="50" valign="top"><img src="{{ asset('image/product/'.$images[0]) }}" width="50" style="-ms-interpolation-mode:bicubic; border:0; height:auto; line-height:100%; outline:none; text-decoration:none" height="auto"></td>
                                      <td class="item-description" style="border-collapse:collapse; color:#787878; mso-table-lspace:0; mso-table-rspace:0; padding:20px 0; padding-top:16px; vertical-align:top" valign="top">
                                        <a href="#" style="color:#1e1e1e; display:block; margin-bottom:8px; text-decoration:none">{{ ucfirst($prod->name) }}</a>
                                        <div>Quantity: {{ $key->quantity }}</div>
                                      </td>
                                      <td class="item-price price" style="border-collapse:collapse; mso-table-lspace:0; mso-table-rspace:0; padding:20px 0; padding-top:16px; text-align:right; vertical-align:top; width:50px" width="50" valign="top" align="right">₹{{ $key->quantity*$key->price }}</td>
                                      <td style="border-collapse:collapse; mso-table-lspace:0; mso-table-rspace:0; padding-top:16px; vertical-align:top" valign="top"></td>
                                    </tr>
									@php $sum = $sum + $key->quantity*$key->price; @endphp
									@endforeach


                                    <tr class="pricing-row" style="border-bottom:1px solid #e8e8e8; border-top:1px solid #e8e8e8">
                                      <td colspan="2" style="border-collapse:collapse; mso-table-lspace:0; mso-table-rspace:0; padding:12px 0">Delivery</td>
                                      <td class="price" style="border-collapse:collapse; mso-table-lspace:0; mso-table-rspace:0; padding:12px 0; text-align:right" align="right">₹{{$order->dl_charge}}</td>
                                    </tr>
                                    <tr class="pricing-row total-row" style="border-bottom:1px solid #e8e8e8; border-top:1px solid #e8e8e8; font-weight:600">
                                      <td colspan="2" style="border-collapse:collapse; mso-table-lspace:0; mso-table-rspace:0; padding:12px 0">Total</td>
                                      <td class="price" style="border-collapse:collapse; mso-table-lspace:0; mso-table-rspace:0; padding:12px 0; text-align:right" align="right"> ₹{{$sum+$order->dl_charge}} </td>
                                    </tr>
                                  </table>
                                </td>
                              </tr>
                            </table>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>

                  <!--[if mso | IE]></td></tr></table><![endif]-->
                </td>
              </tr>
            </tbody>
          </table>
        </div>

        <!--[if mso | IE]></td></tr></table><![endif]-->
      </div>
      <div>

        <!--[if mso | IE]><table align="center" border="0" cellpadding="0" cellspacing="0" class="order-details-footer-outlook" style="width:600px;" width="600" ><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->
        <div class="order-details-footer" style="background:#ffffff;background-color:#ffffff;Margin:0px auto;max-width:600px;">
          <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:#fff; background-color:#fff; border-collapse:collapse; line-height:170%; mso-table-lspace:0; mso-table-rspace:0; width:100%" width="100%" bgcolor="#fff">
            <tbody style="line-height: 170%;">
              <tr style="line-height: 170%;">
                <td style="border-collapse:collapse; direction:ltr; font-size:0; line-height:170%; mso-table-lspace:0; mso-table-rspace:0; padding:24px 24px 0; text-align:center; vertical-align:top" align="center" valign="top">

                  <!--[if mso | IE]><table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td class="" style="vertical-align:top;width:276px;" ><![endif]-->
                  <div class="mj-column-per-50 outlook-group-fix" style="font-size: 13px; text-align: left; direction: ltr; display: inline-block; vertical-align: top; width: 100%; line-height: 170%;">
                    <table border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%" style="border-collapse:collapse; line-height:170%; mso-table-lspace:0; mso-table-rspace:0">
                      <tbody style="line-height: 170%;">
                        <tr style="line-height: 170%;">
                          <td style="border-collapse:collapse; line-height:170%; mso-table-lspace:0; mso-table-rspace:0; padding:0 16px 16px 0; vertical-align:top" valign="top">
                            <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="border-collapse:collapse; line-height:170%; mso-table-lspace:0; mso-table-rspace:0" width="100%">
                              <tr style="line-height: 170%;">
                                <td align="left" style="border-collapse:collapse; font-size:0; line-height:170%; mso-table-lspace:0; mso-table-rspace:0; padding:4px 0; word-break:break-word">
                                  <div style="font-family: Arial; font-size: 13px; font-weight: 600; text-align: left; color: #1e1e1e; line-height: 170%;">Delivery address:</div>
                                </td>
                              </tr>
                              <tr style="line-height: 170%;">
                                <td align="left" style="border-collapse:collapse; font-size:0; line-height:170%; mso-table-lspace:0; mso-table-rspace:0; padding:0; word-break:break-word">
                                  <div style="font-family: Arial; font-size: 13px; text-align: left; color: #787878; line-height: 170%;">
                                    <div style="line-height: 170%;">{{ $order->dl_address }}</div>
                                  </div>
                                </td>
                              </tr>
                            </table>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>

                  <!--[if mso | IE]></td><td class="" style="vertical-align:top;width:276px;" ><![endif]-->
                  <div class="mj-column-per-50 outlook-group-fix" style="font-size: 13px; text-align: left; direction: ltr; display: inline-block; vertical-align: top; width: 100%; line-height: 170%;">
                    <table border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%" style="border-collapse:collapse; line-height:170%; mso-table-lspace:0; mso-table-rspace:0">
                      <tbody style="line-height: 170%;">
                        <tr style="line-height: 170%;">
                          <td style="border-collapse:collapse; line-height:170%; mso-table-lspace:0; mso-table-rspace:0; padding:0; padding-bottom:16px; vertical-align:top" valign="top">
                            <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="border-collapse:collapse; line-height:170%; mso-table-lspace:0; mso-table-rspace:0" width="100%">
                              <tr style="line-height: 170%;">
                                <td align="left" style="border-collapse:collapse; font-size:0; line-height:170%; mso-table-lspace:0; mso-table-rspace:0; padding:4px 0; word-break:break-word">
                                  <div style="font-family: Arial; font-size: 13px; font-weight: 600; text-align: left; color: #1e1e1e; line-height: 170%;">Order Details:</div>
                                </td>
                              </tr>
                              <tr style="line-height: 170%;">
                                <td align="left" style="border-collapse:collapse; font-size:0; line-height:170%; mso-table-lspace:0; mso-table-rspace:0; padding:0; word-break:break-word">
                                  <div style="font-family: Arial; font-size: 13px; text-align: left; color: #787878; line-height: 170%;">
                                    <div style="line-height: 170%;">Order ID: {{$order->order_id}}</div>
                                    <div style="line-height: 170%;">Transaction ID: {{$order->transaction_id}}</div>
                                  </div>
                                </td>
                              </tr>
                            </table>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>

                  <!--[if mso | IE]></td></tr></table><![endif]-->
                </td>
              </tr>
            </tbody>
          </table>
        </div>

        <!--[if mso | IE]></td></tr></table><![endif]-->

        <!--[if mso | IE]></td></tr></table><![endif]-->

        <!--[if mso | IE]><table align="center" border="0" cellpadding="0" cellspacing="0" class="order-details-footer-outlook" style="width:600px;" width="600" ><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->
        <div class="order-details-footer" style="background:#ffffff;background-color:#ffffff;Margin:0px auto;max-width:600px;">
          <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:#fff; background-color:#fff; border-collapse:collapse; line-height:170%; mso-table-lspace:0; mso-table-rspace:0; width:100%" width="100%" bgcolor="#fff">
            <tbody style="line-height: 170%;">
              <tr style="line-height: 170%;">
                <td style="border-collapse:collapse; direction:ltr; font-size:0; line-height:170%; mso-table-lspace:0; mso-table-rspace:0; padding:0 24px 24px; text-align:center; vertical-align:top" align="center" valign="top">

                  <!--[if mso | IE]><table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td class="" style="vertical-align:top;width:552px;" ><![endif]-->
                  <div class="mj-column-per-100 outlook-group-fix" style="font-size: 13px; text-align: left; direction: ltr; display: inline-block; vertical-align: top; width: 100%; line-height: 170%;">
                    <table border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%" style="border-collapse:collapse; line-height:170%; mso-table-lspace:0; mso-table-rspace:0">
                      <tbody style="line-height: 170%;">
                        <tr style="line-height: 170%;">
                          <td style="border-collapse:collapse; line-height:170%; mso-table-lspace:0; mso-table-rspace:0; padding:0; vertical-align:top" valign="top">
                            <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="border-collapse:collapse; line-height:170%; mso-table-lspace:0; mso-table-rspace:0" width="100%">
                              <tr style="line-height: 170%;">
                                <td align="left" style="border-collapse:collapse; font-size:0; line-height:170%; mso-table-lspace:0; mso-table-rspace:0; padding:4px 0; word-break:break-word">
                                  <div style="font-family: Arial; font-size: 13px; font-weight: 600; text-align: left; color: #1e1e1e; line-height: 170%;">Got a question about your order?</div>
                                </td>
                              </tr>
                              <tr style="line-height: 170%;">
                                <td align="left" style="border-collapse:collapse; font-size:0; line-height:170%; mso-table-lspace:0; mso-table-rspace:0; padding:0; word-break:break-word">
                                  <div style="font-family: Arial; font-size: 13px; text-align: left; color: #787878; line-height: 170%;">Contact our team <a href="mailto:sportsgearind@gmail.com" style="color:inherit; line-height:170%">sportsgearind@gmail.com</a>, and quote your order reference number.</div>
                                </td>
                              </tr>
                            </table>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>

                  <!--[if mso | IE]></td></tr></table><![endif]-->
                </td>
              </tr>
            </tbody>
          </table>
        </div>

        <!--[if mso | IE]></td></tr></table><![endif]-->
      </div>

      <!--[if mso | IE]><table align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:600px;" width="600" ><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->
      <div style="background:#f7f7f7;background-color:#f7f7f7;Margin:0px auto;max-width:600px;">
        <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:#f7f7f7; background-color:#f7f7f7; border-collapse:collapse; mso-table-lspace:0; mso-table-rspace:0; width:100%" bgcolor="#f7f7f7" width="100%">
          <tbody>
            <tr>
              <td style="border-collapse:collapse; direction:ltr; font-size:0; mso-table-lspace:0; mso-table-rspace:0; padding:0 24px; text-align:center; vertical-align:top" align="center" valign="top">

                <!--[if mso | IE]><table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td class="" style="vertical-align:top;width:552px;" ><![endif]-->
                <div class="mj-column-per-100 outlook-group-fix" style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
                  <table border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%" style="border-collapse:collapse; mso-table-lspace:0; mso-table-rspace:0">
                    <tbody>
                      <tr>
                        <td style="border-collapse:collapse; border-top:1px solid #e8e8e8; mso-table-lspace:0; mso-table-rspace:0; padding:0; vertical-align:top" valign="top">
                          <table border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%" style="border-collapse:collapse; mso-table-lspace:0; mso-table-rspace:0"></table>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>

                <!--[if mso | IE]></td></tr></table><![endif]-->
              </td>
            </tr>
          </tbody>
        </table>
      </div>

      <!--[if mso | IE]></td></tr></table><![endif]-->
      <div>

        <!--[if mso | IE]></td></tr></table><![endif]-->
      </div>

      <!--[if mso | IE]><table align="center" border="0" cellpadding="0" cellspacing="0" class="footer-outlook" style="width:600px;" width="600" ><tr><td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;"><![endif]-->
      <div class="footer" style="background:#ffffff;background-color:#ffffff;Margin:0px auto;max-width:600px;">
        <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:#fff; background-color:#fff; border-collapse:collapse; mso-table-lspace:0; mso-table-rspace:0; width:100%" bgcolor="#fff" width="100%">
          <tbody>
            <tr>
              <td style="border-collapse:collapse; direction:ltr; font-size:0; mso-table-lspace:0; mso-table-rspace:0; padding:0 24px 24px; text-align:center; vertical-align:top" align="center" valign="top">

                <!--[if mso | IE]><table role="presentation" border="0" cellpadding="0" cellspacing="0"><tr><td class="" style="vertical-align:top;width:552px;" ><![endif]-->
                <div class="mj-column-per-100 outlook-group-fix" style="font-size:13px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
                  <table border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%" style="border-collapse:collapse; mso-table-lspace:0; mso-table-rspace:0">
                    <tbody>
                      <tr>
                        <td style="border-collapse:collapse; border-top:none; mso-table-lspace:0; mso-table-rspace:0; padding:24px 0 0; vertical-align:top" valign="top">
                          <table border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%" style="border-collapse:collapse; mso-table-lspace:0; mso-table-rspace:0">
                            <tr>
                              <td align="center" style="border-collapse:collapse; font-size:0; mso-table-lspace:0; mso-table-rspace:0; padding:0; word-break:break-word">
                                <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="border-collapse:collapse; border-spacing:0; mso-table-lspace:0; mso-table-rspace:0">
                                  <tbody>
                                    <tr>
                                      <td style="border-collapse:collapse; mso-table-lspace:0; mso-table-rspace:0; width:125px" width="125"><img alt="Thread" height="50" src="{{asset('image/sports.jpeg')}}" style="-ms-interpolation-mode:bicubic; border:0; display:block; height:50px; line-height:100%; outline:none; text-decoration:none; width:100%" width="100%"></td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                            <tr>
                              <td align="center" style="border-collapse:collapse; font-size:0; mso-table-lspace:0; mso-table-rspace:0; padding:0; word-break:break-word">
                                <div style="font-family:Arial;font-size:11px;line-height:133%;text-align:center;color:#787878;">
                                  <p style="display:block; margin:13px 0">Address: Hisar Haryana, Phone: +91-79999 95190, Email: Support@sportsgear.com</p>
                                </div>
                              </td>
                            </tr>
                          </table>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>

                <!--[if mso | IE]></td></tr></table><![endif]-->
              </td>
            </tr>
          </tbody>
        </table>
      </div>

      <!--[if mso | IE]></td></tr></table><![endif]-->
    </div>
    </body>
</html>