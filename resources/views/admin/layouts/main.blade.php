<!doctype html>
<!--[if lte IE 9]> <html class="lte-ie9" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--> <html lang="en"> <!--<![endif]-->

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Remove Tap Highlight on Windows Phone IE -->
    <meta name="msapplication-tap-highlight" content="no"/>

    <link rel="icon" type="image/png" href="{{ URL::asset('/html/dist/assets/img/favicon-16x16.png')}}" sizes="16x16">
    <link rel="icon" type="image/png" href="{{ URL::asset('/html/dist/assets/img/favicon-32x32.png')}}" sizes="32x32">

    <title> {{config('app.name')}} @if(isset($title))  @if($title !='') - {{$title}} @endif @endif    </title>
    
    @include('admin.includes.css')

    @yield('css')
    


</head>
<body class="disable_transitions sidebar_main_open sidebar_main_swipe">
    
    <!-- BEGIN Header File  -->
    @include('admin.includes.header')
    <!-- END Header File  -->

    <div id="page_content">
        @yield('content')
    </div>

    <!-- Begin Script -->
    @include('admin.includes.js')
         <!-- End Script -->   
    @yield('js') 
   
</body>
</html>