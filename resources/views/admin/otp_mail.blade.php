<!DOCTYPE html>
<style>
  body {
    padding: 0;
    margin: 0;
  }
</style>
<div style="display:none" hidden="">Get started on something amazing</div>
<div style="background-color:#fff;color:#172b4d;font-family:-apple-system, BlinkMacSystemFont, Segoe UI, Roboto, Noto Sans, Oxygen, Ubuntu, Droid Sans, Helvetica Neue, sans-serif;font-size:14px;font-style:normal;font-weight:400;line-height:1.42857142857143;letter-spacing:-.005em;text-decoration-skip:ink">
  <table cellPadding="0" cellSpacing="0" style="width:100%;align:center" class="main table_shrink">
    <tbody>
      <tr>
        <td>
          <table style="width:520px;margin:auto;text-align:center" cellPadding="0" cellSpacing="0" class="table_shrink">
            <tbody>
              <tr>
                <td style="vertical-align:top;text-align:left" class="message-body">
                  <div>
                    <table style="width:100%;border:0;border-collapse:collapse" cellPadding="0" cellSpacing="0" class="table_shrink">
                      <tbody>
                        <tr>
                          <td style="padding-top:30px;padding-bottom:10px;vertical-align:top;text-align:center"><a href="{{url('/')}}"><img src="{{asset('image/sports.jpeg')}}"  alt="SportGear" style="align:center;border:0" /></a></td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                   <hr style="border:none;border-bottom:1px solid #444" />
                  <p style="margin-top:16px">
                  <h1 style="font-size:2.07142857em;font-style:normal;font-weight:bolder;letter-spacing:-.01em;line-height:1.10344828;margin:auto;text-align:center">You've attempted to regsiter in using your SportGear account</h1>
                  </p>
                  <p style="margin-top:16px">Hi,</p>
                  <p style="margin-top:16px">We noticed that you recently regsiter to sport gear account.</p>
                  <p style="margin-top:16px">Please verify by pasting the code below:</p>

                  <h2 style="margin-top:16px;text-align:center;">{{$otp}}</h2>
                  <hr style="border:none;border-bottom:1px solid #444" />

                  <p style="margin-top:16px">Cheers,<br />The SportGear</p>
                  <div style="color:#707070;font-size:13px;line-height:19px;text-align:center;margin-top:32px">
                    <p style="margin-top:16px">Copyright 2021 SportGear Pty Ltd. All rights reserved. We are located at <br />Hisar, Haryana</p>
                    {{-- <p style="margin-top:16px"><a style="text-decoration:none;color:#707070" href="{{url('/')}}"><img src="{{asset('image/sports.jpeg')}}" height="15px" alt="sportGear" /></a></p> --}}
                  </div>
                </td>
              </tr>
            </tbody>
          </table>
        </td>
      </tr>
    </tbody>
  </table>
</div>