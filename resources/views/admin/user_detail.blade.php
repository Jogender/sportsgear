@extends('admin.layouts.main')
@section('content')
    <div id="page_content_inner">
        <!-- User Data -->
        <h4 class="heading_a uk-margin-bottom">User Detail</h4>        
        <div class="md-card uk-margin-medium-bottom">
            <div class="md-card-content large-padding">
                <div class="uk-grid" data-uk-grid-margin>
                    <div class="uk-width-1-2 ">
                        <span class=" uk-text-muted uk-text-upper uk-text-small uk-float-left">Name: &nbsp;&nbsp;&nbsp;&nbsp;{{ucfirst($data->name) }}</span><br>
                        <span class=" uk-text-muted uk-text-upper uk-text-small uk-float-left">Email: &nbsp;&nbsp;&nbsp;&nbsp;<b>{{ $data->email }}</b></span><br>
                        <span class=" uk-text-muted uk-text-upper uk-text-small uk-float-left">Mobile: &nbsp;&nbsp;&nbsp;&nbsp;
                            {{ $data->phone }}
                        </span><br>
                        {{--<span class=" uk-text-muted uk-text-upper uk-text-small uk-float-left">Payment Status: &nbsp;&nbsp;&nbsp;&nbsp;
                            @if($data->pay_status==0)
                                <span class="uk-badge">Pending</span>
                            @elseif($data->pay_status ==1)
                                <span class="uk-badge uk-badge-success">Accepted</span>
                            @elseif($data->pay_status ==2)
                                <span class="uk-badge uk-badge-danger">Cancelled</span>
                            @endif
                        </span><br>--}}                
                    </div>
                    {{-- <div class="uk-width-1-2">
                        <h2 class="uk-float-right"><i class="uk-icon-rupee"></i>{{ $data->total_amount }}</h2>
                    </div> --}}
                </div>
                <br><br>
                
            </div> 
           
            <div class="md-card-content">
                <h4 class="heading_a uk-margin-bottom">User Address</h4>  
                <div class="dt_colVis_buttons"></div>
                <table id="dt_default" class="uk-table" cellspacing="0" width="100%">
                    <thead>
                        <tr> 
                            <th>Name</th>
                            <th>Email</th>
                            <th>Mobile</th>
                            <th>Address</th>
                            <th>City</th>
                            <th>state</th>
                        </tr>
                    </thead>                        

                    <tbody>
                        @foreach($useraddress as $d)
                        <tr>
                           <td>{{ $d->firstname }} {{ $d->lastname }}</td>
                           <td>{{ $d->email }} </td>
                           <td>{{ $d->mobile }} </td>
                           <td>{{ $d->address }} </td>
                           <td>{{ $d->city }} </td>
                           <td>{{ $d->state }} </td>
                        </tr>
                        @endforeach  
                    </tbody>
                </table>
            </div>
            <div class="md-fab-wrapper">
            </div>

           
            <div class="md-card-content" style="margin-top:10%;">
                <h4 class="heading_a uk-margin-bottom">Orders</h4>  
                <div class="dt_colVis_buttons"></div>
                <table id="dt_default" class="uk-table" cellspacing="0" width="100%">
                    <thead>
                        <tr> 
                            <th>Order Id</th>
                            <th>Date</th>
                            <th>Price</th>
                            <th>Payment Method</th>
                            <th>Shiping</th>
                            <th>Status</th>
                           
                        </tr>
                    </thead>                        

                    <tbody>
                        @foreach($order as $o)
                         <tr>
                            <td><a href="{{ route('order_details',['id'=>encrypt($o->id)]) }}">{{ $o->order_id }}</a></td>
                             <td>
                                @if(is_null($o->date))
                                    {{ date('d-m-Y') }}
                                @else
                                    {{ date('d-m-Y',strtotime($o->date)) ?? '' }}
                                @endif
                            </td>
                             <td>{{ $o->amount }}</td>
                             <td>{{ $o->payment_type }}</td>
                             <td>{{ $o->delivery_charge }}</td>
                             <td id="td{{$o->id}}">
                                @if($o->status == 0)
                                <span class="uk-badge uk-badge-warning" ><a href="javascript:void(0);" class="ordered" data-toggle="modal" style="cursor: pointer;"  data-id="{{ $o->id }}" data-status="{{ $o->status }}">Pending</a></span>
                                @elseif($o->status == 1)
                                <span class="uk-badge md-bg-deep-purple-300" ><a href="javascript:void(0);" class="ordered" data-toggle="modal" style="cursor: pointer;"  data-id="{{ $o->id }}" data-status="{{ $o->status }}">Shipped</a></span>
                                @elseif($o->status == 2)
                                <span class="uk-badge md-bg-blue-300" ><a href="javascript:void(0);" class="ordered" data-toggle="modal" style="cursor: pointer;"  data-id="{{ $o->id }}" data-status="{{ $o->status }}">Processing</a></span>
                                @elseif($o->status == 3)
                                <span class="uk-badge md-bg-light-green-300" ><a href="javascript:void(0);" class="ordered" data-toggle="modal" style="cursor: pointer;"  data-id="{{ $o->id }}" data-status="{{ $o->status }}">Out for delivery</a></span>
                                @elseif($o->status == 4)
                                <span class="uk-badge md-bg-green-A400" ><a href="javascript:void(0);" class="ordered" data-toggle="modal" style="cursor: pointer;"  data-id="{{ $o->id }}" data-status="{{ $o->status }}">Delivered</a></span>
                                @elseif($o->status == 5)
                                <span class="uk-badge md-bg-red-300" ><a href="javascript:void(0);" class="ordered" data-toggle="modal" style="cursor: pointer;"  data-id="{{ $o->id }}" data-status="{{ $o->status }}">Cancelled</a></span>
                                @else
                                <span class="uk-badge md-bg-blue-grey-300" ><a href="javascript:void(0);" class="ordered" data-toggle="modal" style="cursor: pointer;"  data-id="{{ $o->id }}" data-status="{{ $o->status }}">Rejected</a></span>
                                @endif
                            </td>
                         </tr>
                        @endforeach  
                    </tbody>
                </table>
            </div>
        </div>

        <div class="md-fab-wrapper">
          
        </div>
    </div>
    @endsection