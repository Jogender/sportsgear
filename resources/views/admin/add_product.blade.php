@extends('admin.layouts.main')

@section('content')
<div id="page_content_inner">
    <h2 class="heading_b uk-margin-bottom">Add Product</h2>
    <div class="md-card uk-margin-large-bottom">
        <div class="md-card-content">
            <form method="post" action="{{route('add_product')}}" id="form_validation" enctype="multipart/form-data"
                class="uk-form-stacked task-form">
                @csrf
                
                <input type="hidden" name="valueIds" id="valueIds" value="1" /> 
                <input type="hidden" name="keyIds" id="keyIds" value="1" /> 
                <input type="hidden" name="keyCount1" id="key1" value="1" /> 

                <div id="keyCountDiv"></div>

                <div class="uk-width-medium-1-1">
                    <label for="fullname">Name<span class="req">*</span></label>
                    <div class="parsley-row">
                        <input type="text" name="name" id="name" autocomplete="off" placeholder="Enter name" required
                            class="md-input task" />
                    </div>
                </div>

                <div class="uk-width-medium-1-1">
                    <label for="fullname">Slug<span class="req">*</span></label>
                    <div class="parsley-row">
                        <input type="text" name="slug"  id="slug" autocomplete="off" placeholder="Enter Slug" required
                            class="md-input task" />
                    </div>
                </div>

                <div class="uk-grid" data-uk-grid-margin>
                    <div class="uk-width-medium-1-1">
                        <label for="fullname">Category<span class="req">*</span></label>
                        <div class="parsley-row">
                            <select id="category" class="category md-input" name="category_id" required>
                                <option value="">Choose Category</option>
                                @foreach($category as $keyc)
                                    <option value="{{ $keyc->id }}">{{ $keyc->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="uk-width-medium-1-1">
                        <label for="fullname">SubCategory<span class="req">*</span></label>
                        <div class="parsley-row">
                            <select id="subcategory" class="subcategory md-input" name="subcategory_id" >
                            </select>
                        </div>
                    </div>
                    <div class="uk-width-medium-1-1">
                        <label for="fullname">SubSubCategory</label>
                        <div class="parsley-row">
                            <select id="subsubcategory" class="subsubcategory md-input" name="subsubcategory_id" >
                            </select>
                        </div>
                    </div>

                    <div class="uk-width-medium-1-1">
                        <label for="fullname">Description<span class="req">*</span></label>
                        <div class="parsley-row">
                            <textarea id="wysiwyg_ckeditor" name="short_description" cols="30" rows="20"></textarea>
                        </div>
                    </div>
                  
                    <div class="uk-width-medium-1-1" id="description">
                        <label for="fullname">Short Description</label>
                        <div class="parsley-row">
                            <textarea id="rohi" name="description" cols="30" rows="20"> </textarea>
                        </div>
                    </div>                   

                    <div class="uk-width-medium-1-1">
                        <div class="md-card">
                            <div class="md-card-content">
                                <h3 class="heading_a uk-margin-small-bottom">
                                    Upload Image<span class="req">*</span>
                                </h3>

                                <div style="display:none;" id="image_uploader">
                                    <input type="file" name="files" id="files" accept=".png, .jpg, .jpeg" required />
                                </div>
                                <a href="javascript:;" onclick="open_file()" class="image-button"><i class="icon-picture"></i> File</a>
                            </div>
                        </div>
                    </div>
                    
                    
                    <div class="row">
                        <div class="col-md-6">
                            <div class="uk-width-medium-1-1">
                                <label for="fullname">Featured<span class="req">*</span></label>
                                <div class="parsley-row">
                                    <input type="checkbox" checked data-switchery data-switchery-color="#76C804;" name="feature"
                                        value="1" id="switch_demo" />
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="uk-width-medium-1-1" style="float:right;">
                                <label for="fullname">Top Weekly<span class="req">*</span></label>
                                <div class="parsley-row">
                                    <input type="checkbox" name="weekly" checked data-switchery data-switchery-color="#76C804;" name="feature"
                                        value="1" id="switch_demo" />
                                </div>
                            </div>
                        </div>
                    </div> 

                    <div class="uk-width-medium-1-1">
                        <label for="fullname">Brand<span class="req">*</span></label>
                        <div class="parsley-row">
                            <select id="brand" class="brand md-input" name="brand" required>
                                <option value="">Choose Brand</option>
                                @foreach($brand as $brd)
                                    <option value="{{ $brd->id }}">{{ $brd->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="uk-width-medium-1-1">
                        <label for="fullname">Product of origin<span class="req">*</span></label>
                        <div class="parsley-row">
                            <input type="text" name="origin" autocomplete="off" placeholder="Enter product of origin" required class="md-input task mobile" />
                        </div>
                    </div>


                    <div class="uk-width-medium-1-1">
                        <label for="fullname">Variant<span class="req">*</span></label>
                        <div class="parsley-row">
                            <div class="uk-width-medium-3-5">
                                <input type="radio" name="variant_status" id="radio_demo_inline_1123"
                                    value="yes"   />
                                <label for="radio_demo_inline_1123" class="inline-label">Yes</label>
                                <input type="radio" name="variant_status" id="radio_demo_inline_2123"
                                    value="no" checked  />
                                <label for="radio_demo_inline_2123" class="inline-label" >No</label>
                            </div>
                        </div>
                    </div>


                    <div class="uk-width-medium-1-1" id="old_price">
                        <label for="fullname">MRP Price</label>
                        <div class="parsley-row">
                            <input type="text" name="old_price" autocomplete="off"
                                placeholder="Enter MRP Price" 
                                class="md-input task mobile" />
                        </div>
                    </div> 
                  

                    <div class="uk-width-medium-1-1" id="new_price">
                        <label for="fullname">Offer Price</label>
                        <div class="parsley-row">
                            <input type="text" name="new_price" autocomplete="off"
                                placeholder="Enter Offer Price" 
                                class="md-input task mobile" />
                        </div>
                    </div> 

                    <div class="uk-width-medium-1-1" id="quantity">
                        <label for="fullname">Quantity</label>
                        <div class="parsley-row">
                            <input type="text" name="quantity" autocomplete="off"
                                placeholder="Enter quantity" 
                                class="md-input task mobile" />
                        </div>
                    </div> 

                    <div class="uk-width-medium-1-1" id="tax">
                        <label for="tax">GST Tax(%)</label>
                        <div class="parsley-row">
                            <input type="number" name="tax" autocomplete="off" min="0"
                                placeholder="Enter Tax(%)" 
                                class="md-input task mobile" />
                        </div>
                    </div> 
                   
                </div>

                <div class="variants-wrapper" id="keyIds1" style="display:none;">
                    <div class="variants field_wrapper">
                        <hr class="form_hr">
                        <div class="row">
                            <div class="md-card">
                                <div class="md-card-content">
                                    <div class="uk-grid" id="variant1" data-uk-grid-margin>

                                       <div class="uk-width-medium-1-6">
                                            <label for="fullname">Key<span>*</span></label>
                                            <div class="parsley-row">
                                                <input type="text" name="key1[]" autocomplete="off" placeholder="Enter key"  class="md-input mobile" />
                                            </div>
                                        </div>
                                        
                                        <div class="uk-width-medium-1-6">
                                            <label for="fullname">Value<span >*</span></label>
                                            <div class="parsley-row">
                                                <input type="text" name="key1value1[]" autocomplete="off" placeholder="Enter Value"  class="md-input mobile" />
                                            </div>                                                
                                        </div>  

                                        <div class="uk-width-medium-1-6">
                                            <label for="fullname">Price<span >*</span></label>
                                            <div class="parsley-row">
                                                <input type="text" name="key1price1[]" autocomplete="off" placeholder="Enter Price"  class="md-input mobile" />
                                            </div>                                                
                                        </div> 

                                        <div class="uk-width-medium-1-6">
                                            <label for="fullname">Quantity<span >*</span></label>
                                            <div class="parsley-row">
                                                <input type="text" name="key1quantity1[]" autocomplete="off" placeholder="Enter Quantity"  class="md-input mobile" />
                                            </div>                                                
                                        </div> 

                                        <div class="uk-width-medium-1-6">
                                            <label for="fullname">Price changes<span class="req">*</span></label>
                                            <div class="parsley-row">
                                                <input type="checkbox" name="key1pricechange1[]" value="on" id="switch_demo" />
                                            </div>
                                        </div>
                                           
                                        <div class="uk-width-medium-1-6">
                                            <a href="javascript:void(0);" class="add_value" onclick="addValue('key1','variant1')" title="Add Value"><i class="fa fa-plus"> Add Value</i></a>
                                        </div>

                                        <div class="uk-width-medium-1-6">
                                            <a href="javascript:void(0);" class="add_value" onclick="addkey('key1')" title="Add Keys"><i class="fa fa-plus"> Add Keys</i></a>
                                        </div>
                                    </br>
                                        
                                    </div></br>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <br>
                <div class="uk-grid">
                    <div class="uk-width-forms_wysiwyg_ckeditor1-1">
                        <button type="submit" class="md-btn"
                            style="background-color:#e2932d;color: white;">Submit</button>
                        <a href="{{ URL::previous() }}" type="button"
                            class="md-btn md-btn-wave waves-effect waves-button">Cancel</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
@section('js')
    @include('admin.productjs')
    <script>
        CKEDITOR.replace('rohi', {
            skin: 'moono',
            enterMode: CKEDITOR.ENTER_BR,
            shiftEnterMode:CKEDITOR.ENTER_P,
            toolbar: [{ name: 'basicstyles', groups: [ 'basicstyles' ], items: [ 'Bold', 'Italic', 'Underline', "-", 'TextColor', 'BGColor' ] },
             { name: 'styles', items: [ 'Format', 'Font', 'FontSize' ] },
             { name: 'scripts', items: [ 'Subscript', 'Superscript' ] },
             { name: 'justify', groups: [ 'blocks', 'align' ], items: [ 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock' ] },
             { name: 'paragraph', groups: [ 'list', 'indent' ], items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent'] },
             { name: 'links', items: [ 'Link', 'Unlink' ] },
             { name: 'insert', items: [ 'Image'] },
             { name: 'spell', items: [ 'jQuerySpellChecker' ] },
             { name: 'table', items: [ 'Table' ] }
             ],
        });
    </script>
    
@endsection
