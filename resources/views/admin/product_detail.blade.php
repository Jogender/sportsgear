@extends('admin.layouts.main')
@section('content')
<div id="page_content_inner">
    <h2 class="heading_b uk-margin-bottom"> Product Detail </h2>
    <div class="md-card uk-margin-large-bottom">

        <div class="md-card-content">

            <div class="uk-grid" data-uk-grid-margin>
                <div class="uk-width-medium-1-2">
                    <label for="fullname"> Name </label>
                    <div class="parsley-row">
                        <input type="text" readonly value="{{$data->name}}"    class="md-input task" />
                    </div>
                </div>                
                <div class="uk-width-medium-1-2">
                    <label for="fullname"> Category </label>
                    <div class="parsley-row">
                        <input type="text" readonly value="{{$data->category_name}}"  class="md-input task" />
                    </div>
                </div>
            </div>

            <div class="uk-grid" data-uk-grid-margin>
                <div class="uk-width-medium-1-2">
                    <label for="fullname"> Sub Category </label>
                    <div class="parsley-row">
                        <input type="text" readonly value="{{$data->subcategory_name}}"    class="md-input task" />
                    </div>
                </div>                
                <div class="uk-width-medium-1-2">
                    <label for="fullname">Sub Sub Category </label>
                    <div class="parsley-row">
                        <input type="text" readonly value="{{$data->subsubcategory_name}}"  class="md-input task" />
                    </div>
                </div>
            </div>

                      
            <div class="md-input-wrapper md-input-filled">
                <label  for="fullname">Short Description</label>
                <textarea cols="30" rows="4" class="md-input no_autosize"> {!! $data->short_description !!} </textarea>
                <span class="md-input-bar "></span>
            </div>
           

            <div class="uk-width-medium-1-1" >
                <label for="fullname">Images</label>
                <div class="parsley-row">
                    @php $images=json_decode($data->image);  @endphp
                    @foreach($images as $key => $value)
                      <a href="{{URL::asset('image/product/'.$value)}}" target="_blank"> <img  src="{{URL::asset('image/product/'.$value)}}" height="100" width="100"> </a>  
                    @endforeach                                        
                </div>
            </div>  </br>  

            <div class="uk-grid" data-uk-grid-margin>
                <div class="uk-width-medium-1-2">
                    <label for="fullname"> Featured </label>
                    <div class="parsley-row">
                        <input type="text" readonly  @if($data->featured == 1) value="ON" @else value="Off"  @endif class="md-input task" />
                    </div>
                </div>             
                
                <div class="uk-width-medium-1-2">
                    <label for="fullname">Top Weekly </label>
                    <div class="parsley-row">
                        <input type="text" readonly @if($data->topweekly == 1) value="ON" @else value="Off"  @endif class="md-input task" />
                    </div>
                </div>
            </div>

            <div class="uk-grid" data-uk-grid-margin>
                <div class="uk-width-medium-1-2">
                    <label for="fullname"> Brand </label>
                    <div class="parsley-row">
                        <input type="text" readonly  value="{{$data->brand}}"  class="md-input task" />
                    </div>
                </div>             
                
                <div class="uk-width-medium-1-2">
                    <label for="fullname">Product of origin </label>
                    <div class="parsley-row">
                        <input type="text" readonly value="{{$data->origin}}"   class="md-input task" />
                    </div>
                </div>
            </div>

            @if($data->variant_status == 'no')
                <div class="uk-grid" data-uk-grid-margin>
                    <div class="uk-width-medium-1-2">
                        <label for="fullname"> Old Price </label>
                        <div class="parsley-row">
                            <input type="text" readonly  value="{{$data->old_price}}"  class="md-input task" />
                        </div>
                    </div>             
                    
                    <div class="uk-width-medium-1-2">
                        <label for="fullname"> New price </label>
                        <div class="parsley-row">
                            <input type="text" readonly value="{{$data->new_price}}"   class="md-input task" />
                        </div>
                    </div>
                </div>
            @endif

            <div class="uk-width-medium-1-1">
                <label for="fullname">Variant Status<span class="req">*</span></label>
                <div class="parsley-row">
                    <input type="text" readonly value="{{ucfirst($data->variant_status) }}"  class="md-input task" />
                </div>
            </div>

            @if($data->variant_status == 'yes')
                @php $variantDetail=json_decode($data->variant);   @endphp
                <div class="variants-wrapper">
                    <div class="variants field_wrapper">
                        <hr class="form_hr">
                        <div class="row">
                            <div class="md-card">
                                <div class="md-card-content">
                                    <div class="uk-grid" id="variant1" data-uk-grid-margin>
                                        @foreach($variantDetail as $key => $value)


                                            @foreach($value as $k=>$v)

                                                <div class="uk-width-medium-1-4">
                                                    <label for="fullname">Key</label>
                                                    <div class="parsley-row">
                                                        <input type="text" readonly value="{{$k}}"  class="md-input mobile" />
                                                    </div>
                                                </div>

                                                @foreach($v as $l=>$w)

                                                    @if($l > 0)
                                                        <div class="uk-width-medium-1-4">
                                                            
                                                        </div>
                                                    @endif


                                                    <div class="uk-width-medium-1-4">
                                                        <label for="fullname">Value</label>
                                                        <div class="parsley-row">
                                                            <input type="text" readonly value="{{$w->value}}"  class="md-input mobile" />
                                                        </div>                                                
                                                    </div>  
            
                                                    <div class="uk-width-medium-1-4">
                                                        <label for="fullname">Price</label>
                                                        <div class="parsley-row">
                                                            <input type="text"  readonly value="{{$w->price}}" class="md-input mobile" />
                                                        </div>                                                
                                                    </div> 
            
                                                    <div class="uk-width-medium-1-4">
                                                        <label for="fullname">Quantity</label>
                                                        <div class="parsley-row">
                                                            <input type="text" readonly value="{{$w->quantity}}" class="md-input mobile" />
                                                        </div>                                                
                                                    </div>  </br>

                                                @endforeach

                                            @endforeach
                                            
                                        @endforeach
                                                                    
                                    </div></br>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endif




        </div>

    </div>
</div>



@endsection
