<!-- common functions -->
<script src="{{ URL::asset('/html/dist/assets/js/common.min.js')}}"></script>
<!-- uikit functions -->
<script src="{{ URL::asset('/html/dist/assets/js/uikit_custom.min.js')}}"></script>
<!-- altair common functions/helpers -->
<script src="{{ URL::asset('/html/dist/assets/js/altair_admin_common.min.js')}}"></script>

     <!-- kendo UI -->
     <script src="{{ URL::asset('/html/dist/assets/js/kendoui_custom.min.js')}}"></script>

    <!--  kendoui functions -->
    <script src="{{ URL::asset('/html/dist/assets/js/pages/kendoui.min.js')}}"></script>

<!-- page specific plugins -->
    <!-- Dashboard  -->        
       <!-- d3 -->
       <script src="{{ URL::asset('/html/dist/bower_components/d3/d3.min.js')}}"></script>
       <!-- metrics graphics (charts) -->
       <script src="{{ URL::asset('/html/dist/bower_components/metrics-graphics/dist/metricsgraphics.min.js')}}"></script>
       <!-- chartist (charts) -->
       <script src="{{ URL::asset('/html/dist/bower_components/chartist/dist/chartist.min.js')}}"></script>
       
       <script src="{{ URL::asset('/html/dist/bower_components/maplace-js/dist/maplace.min.js')}}"></script>
       <!-- peity (small charts) -->
       <script src="{{ URL::asset('/html/dist/bower_components/peity/jquery.peity.min.js')}}"></script>
       <!-- easy-pie-chart (circular statistics) -->
       <script src="{{ URL::asset('/html/dist/bower_components/jquery.easy-pie-chart/dist/jquery.easypiechart.min.js')}}"></script>
       <!-- countUp -->
       <script src="{{ URL::asset('/html/dist/bower_components/countUp.js/dist/countUp.min.js')}}"></script>
       <!-- handlebars.js -->
       <script src="{{ URL::asset('/html/dist/bower_components/handlebars/handlebars.min.js')}}"></script>
       <script src="{{ URL::asset('/html/dist/assets/js/custom/handlebars_helpers.min.js')}}"></script>
       <!-- CLNDR -->
       <script src="{{ URL::asset('/html/dist/bower_components/clndr/clndr.min.js')}}"></script>

       <!--  dashbord functions -->
       <script src="{{ URL::asset('/html/dist/assets/js/pages/dashboard.min.js')}}"></script>

        <!--  notifications functions -->
        <script src="{{ URL::asset('/html/dist/assets/js/pages/components_notifications.min.js')}}"></script>

        <!-- ckeditor -->
            <script src="{{ URL::asset('/html/dist/bower_components/ckeditor/ckeditor.js') }}"></script>
            <script src="{{ URL::asset('/html/dist/bower_components/ckeditor/adapters/jquery.js') }}"></script>
    
        <!--  wysiwyg editors functions -->
            <script src="{{ URL::asset('/html/dist/assets/js/pages/forms_wysiwyg.min.js') }}"></script>
            <script src="{{ URL::asset('/html/dist/assets/js/pages/components_preloaders.min.js') }}"></script>

 <!-- page specific plugins -->
    <!-- User -->
        <!-- datatables -->
        <script src="{{ URL::asset('/html/dist/bower_components/datatables/media/js/jquery.dataTables.min.js')}}"></script>
        <!-- datatables buttons-->
        <script src="{{ URL::asset('/html/dist/bower_components/datatables-buttons/js/dataTables.buttons.js')}}"></script>
        <script src="{{ URL::asset('/html/dist/assets/js/custom/datatables/buttons.uikit.js')}}"></script>
        <script src="{{ URL::asset('/html/dist/bower_components/jszip/dist/jszip.min.js')}}"></script>
        <script src="{{ URL::asset('/html/dist/bower_components/pdfmake/build/pdfmake.min.js')}}"></script>
        <script src="{{ URL::asset('/html/dist/bower_components/pdfmake/build/vfs_fonts.js')}}"></script>
        <script src="{{ URL::asset('/html/dist/bower_components/datatables-buttons/js/buttons.colVis.js')}}"></script>
        <script src="{{ URL::asset('/html/dist/bower_components/datatables-buttons/js/buttons.html5.js')}}"></script>
        <script src="{{ URL::asset('/html/dist/bower_components/datatables-buttons/js/buttons.print.js')}}"></script>

        <!-- datatables custom integration -->
        <script src="{{ URL::asset('/html/dist/assets/js/custom/datatables/datatables.uikit.min.js')}}"></script>

        <!--  datatables functions -->
        <script src="{{ URL::asset('/html/dist/assets/js/pages/plugins_datatables.min.js')}}"></script>

 <!--  dropify -->
    <script src="{{ URL::asset('/html/dist/bower_components/dropify/dist/js/dropify.min.js')}}"></script>

    <!--  form file input functions -->
    <script src="{{ URL::asset('/html/dist/assets/js/pages/forms_file_input.min.js')}}"></script>

<!--  Multiple File upload -->
   <script src="{{ URL::asset('/html/dist/fileupload/js/jquery.fileuploader.min.js')}}"></script>
   <script src="{{ URL::asset('/html/dist/fileupload/js/custom.js')}}"></script>

   {{-- CK editor --}}
   <script src="{{ URL::asset('/html/dist/bower_components/ckeditor/custom_ckeditor.js') }}"></script>

<script>
    // jQuery ".Class" SELECTOR.
    $(document).ready(function() {
        $('.onlynumber').keypress(function (event) {
            return isNumber(event, this)
        });
    });

    // THE SCRIPT THAT CHECKS IF THE KEY PRESSED IS A NUMERIC OR DECIMAL VALUE.
    function isNumber(evt, element) {

        var charCode = (evt.which) ? evt.which : event.keyCode

        if (
            (charCode != 45 || $(element).val().indexOf('-') != -1) &&      // “-” CHECK MINUS, AND ONLY ONE.
            (charCode != 46 || $(element).val().indexOf('.') != -1) &&      // “.” CHECK DOT, AND ONLY ONE.
            (charCode < 48 || charCode > 57))
            return false;

        return true;
    } 

    // multiple Fileupload Script
    function open_file(){
        $('#files').click();
        setInterval(function(){ 
            $('#image_uploader').show(500);
        }, 500);        
    }

    //model for image remove
    function remove_image(image,image_name){
        var name="#image_div"+image_name;
        $('#file_id').val(image);
        $('#remove_id').val(name);            
        $('#myModal').modal('toggle');
    }
    

    
    var msg = '{{Session::get('error')}}';
    var error = '{{Session::has('error')}}';

    var successmsg = '{{Session::get('success')}}';
    var success = '{{Session::has('success')}}';

    if(error){
        UIkit.notify({ message:msg, status: 'warning', timeout: 5000, group: null, pos: 'bottom-center' });
    }

    if(successmsg){
        UIkit.notify({ message:successmsg, status: 'success', timeout: 5000, group: null, pos: 'bottom-center' });
    }


    // Slug name
    function slugName(Text)
    {
        return Text
            .toLowerCase()
            .replace(/ /g,'-')
            .replace(/[^\w-]+/g,'')
            ;
    }
  
</script>

        