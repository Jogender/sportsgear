 <!-- main header -->
 <header id="header_main">
    <div class="header_main_content">
        <nav class="uk-navbar">
                                
            <!-- main sidebar switch -->
            <a href="#" id="sidebar_main_toggle" class="sSwitch sSwitch_left">
                <span class="sSwitchIcon"></span>
            </a>
            
            <!-- secondary sidebar switch -->
            <a href="#" id="sidebar_secondary_toggle" class="sSwitch sSwitch_right sidebar_secondary_check">
                <span class="sSwitchIcon"></span>
            </a>            
            
            <div class="uk-navbar-flip">
                <ul class="uk-navbar-nav user_actions">                    
                    <li data-uk-dropdown="{mode:'click',pos:'bottom-right'}">
                        <a href="#" class="user_action_image"><img class="md-user-image" src="{{URL::asset('image/user/').'/'.Auth::user()->profile }}"  alt=""/></a>
                        <div class="uk-dropdown uk-dropdown-small">
                            
                            <ul class="uk-nav js-uk-prevent">                            
                                <li><a class="dropdown-item" href="{{ route('logout') }}"onclick="event.preventDefault();document.getElementById('logout-form').submit();">Logout</a></li>
                            </ul>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </div>
                    </li>
                </ul>
            </div>
        </nav>
    </div>
    
</header>
<!-- main header end -->

<!-- main sidebar -->
<aside id="sidebar_main">
        
    <div class="sidebar_main_header">
        <div class="sidebar_logo">
            <a href="{{route('admin.home')}}" class="sSidebar_hide sidebar_logo_large">
                <img class="logo_regular" src="{{ URL::asset('/image/logo/logo.png')}}" alt="" height="15" width="60%"/>                
                <h3>{{config('app.name')}} </h3>
            </a>            
        </div>            
    </div>
     
    <div class="menu_section">
        <ul>
            {{-- Dashboard --}}
            <li @if(isset($title)) @if($title == 'Dashboard')   class="current_section" @endif @endif title="Dashboard">
                <a href="{{route('admin.home')}}">
                    <span class="menu_icon"><i class="material-icons">&#xE871;</i></span>
                    <span class="menu_title">Dashboard</span>
                </a>                
            </li>
            {{-- User --}}            
            <li @if(isset($title)) @if($title == 'User')   class="current_section" @endif @endif title="User Profile">
                <a href="{{ route('admin.users') }}">
                    <span class="menu_icon"><i class="material-icons">&#xE87C;</i></span>
                    <span class="menu_title">Users</span>
                </a>                
            </li>

            <li @if(isset($title)) @if($title == 'banner')   class="current_section" @endif @endif title="Banner">
                <a href="{{route('banner')}}">
                    <span class="menu_icon"><i class="material-icons"> wallpaper </i></span>
                    <span class="menu_title">Banner</span>
                </a>                
            </li>

            <li @if(isset($title)) @if($title == 'Brand')   class="current_section" @endif @endif title="Brand">
                <a href="{{route('brand')}}">
                    <span class="menu_icon"><i class="material-icons"> wallpaper </i></span>
                    <span class="menu_title">Brand</span>
                </a>                
            </li>

            <li @if(isset($title)) @if($title == 'Bulk Image')   class="current_section" @endif @endif title="Bulk Image">
                <a href="{{route('bulk_image')}}">
                    <span class="menu_icon"><i class="material-icons"> wallpaper </i></span>
                    <span class="menu_title">Bulk Image</span>
                </a>                
            </li>

            <li @if(isset($title)) @if($title == 'category')   class="current_section" @endif @endif title="Banner">
                <a href="{{route('category')}}">
                    <span class="menu_icon"><i class="material-icons"> croporiginal </i></span>
                    <span class="menu_title">Category</span>
                </a>                
            </li>

            <li @if(isset($title)) @if($title == 'subcategory')   class="current_section" @endif @endif title="Banner">
                <a href="{{route('subcategory')}}">
                    <span class="menu_icon"><i class="material-icons">line_weight</i></span>
                    <span class="menu_title">Subcategory</span>
                </a>                
            </li>

            <li @if(isset($title)) @if($title == 'Sub-subcategory')   class="current_section" @endif @endif title="Banner">
                <a href="{{route('subsubcategory')}}">
                    <span class="menu_icon"><i class="material-icons">line_style</i></span>
                    <span class="menu_title">Subsubcategory</span>
                </a>                
            </li>

            <li @if(isset($title)) @if($title == 'product')   class="current_section" @endif @endif title="Banner">
                <a href="{{route('view_product')}}">
                    <span class="menu_icon"><i class="material-icons">store</i></span>
                    <span class="menu_title">Product</span>
                </a>                
            </li>

            <li @if(isset($title)) @if($title == 'order')   class="current_section" @endif @endif title="Banner">
                <a href="{{route('order')}}">
                    <span class="menu_icon"><i class="material-icons">dns</i></span>
                    <span class="menu_title">Order</span>
                </a>                
            </li>

            <li @if(isset($title)) @if($title == 'states')   class="current_section" @endif @endif title="States">
                <a href="{{route('view_states')}}">
                    <span class="menu_icon"><i class="material-icons">home</i></span>
                    <span class="menu_title">States</span>
                </a>                
            </li>
            

        </ul>
    </div>
</aside>


<script>window.pageData={}; window.pageData.baseUrl = "{{ url('/') }}";</script>
<!-- main sidebar end -->