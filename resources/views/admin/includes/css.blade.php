<!-- additional styles for plugins -->
<!-- kendo UI -->
<link rel="stylesheet" href="{{ URL::asset('/html/dist/bower_components/kendo-ui/styles/kendo.common-material.min.css')}}"/>
<link rel="stylesheet" href="{{ URL::asset('/html/dist/bower_components/kendo-ui/styles/kendo.material.min.css')}}" id="kendoCSS"/>

<!-- htmleditor (codeMirror) -->
<link rel="stylesheet" href="{{ URL::asset('/html/dist/bower_components/codemirror/lib/codemirror.css')}}">
<!-- select2 -->
<link rel="stylesheet" href="{{ URL::asset('/html/dist/bower_components/select2/dist/css/select2.min.css')}}">


<!-- weather icons -->
<link rel="stylesheet" href="{{ URL::asset('/html/dist/bower_components/weather-icons/css/weather-icons.min.css')}}" media="all">
<!-- metrics graphics (charts) -->
<link rel="stylesheet" href="{{ URL::asset('/html/dist/bower_components/metrics-graphics/dist/metricsgraphics.css')}}">
<!-- chartist -->
<link rel="stylesheet" href="{{ URL::asset('/html/dist/bower_components/chartist/dist/chartist.min.css')}}">

<!-- uikit -->
<link rel="stylesheet" href="{{ URL::asset('/html/dist/bower_components/uikit/css/uikit.almost-flat.min.css')}}" media="all">

<!-- flag icons -->
<link rel="stylesheet" href="{{ URL::asset('/html/dist/assets/icons/flags/flags.min.css')}}" media="all">

<!-- style switcher -->
<link rel="stylesheet" href="{{ URL::asset('/html/dist/assets/css/style_switcher.min.css')}}" media="all">

<!-- altair admin -->
<link rel="stylesheet" href="{{ URL::asset('/html/dist/assets/css/main.min.css')}}" media="all">

<!-- themes -->
<link rel="stylesheet" href="{{ URL::asset('/html/dist/assets/css/themes/themes_combined.min.css')}}" media="all">

<!-- dropify -->
<link rel="stylesheet" href="{{ URL::asset('/html/dist/assets/skins/dropify/css/dropify.css')}}">

{{-- Multiple FileUpload --}}
<link href="{{ URL::asset('/html/dist/fileupload/css/jquery.fileuploader.min.css')}}"  rel="stylesheet" />
<link href="{{ URL::asset('/html/dist/fileupload/css/jquery.fileuploader-theme-thumbnails.css')}}" rel="stylesheet" />
<link href="{{ URL::asset('/html/dist/fileupload/css/font-fileuploader.css')}}" rel="stylesheet" />


<style>
.uk-table td, .uk-table th
{
    padding: 10px 10px !important;
}
.hide
{
    display: none !important;
}
.float-right
{
    float: right !important;
}

.md-user-image
{
    height: 39px !important;
}

.width-10
{
    width: 10% !important;
}

.width-50
{
    width: 50% !important;
}

.text-red 
{
    color:#e53935 !important;
}

.text-yellow 
{
    color:#ffa000 !important;
}

.text-info 
{
    color:#0097a7 !important;
}

.text-green 
{
    color:#7cb342 !important;
}

/* File upload CSS */

.fileuploader {
        max-width: 100%;
    }

.image-button{
    color:black;
    cursor:pointer;
    font-weight: 500;
    text-decoration: none;
    padding: 9px;
    background-color: #EBEDF0;border-radius: 20px;
}


</style>