@extends('admin.layouts.main')
@section('content')
        <div id="page_content_inner">
            <h3 class="heading_b uk-margin-bottom">Users</h3>
            <div class="md-card uk-margin-medium-bottom">
                <div class="md-card-content">
                    <br />
                    <div class="uk-width-medium-1-1">
                        <div class="uk-overflow-container">
                            <table id="dt_default" class="uk-table uk-table-striped uk-table-hover" cellspacing="0"
                                width="100%">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Mobile</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody id="ajaxresponse">
                                @if(!$data->isEmpty())
                                    @foreach($data as $d)
                                        <tr>
                                            <td><a href="{{ route('admin.user_detail',['id'=>encrypt($d->id)]) }}"> {{ ucfirst($d->name) }} </a></td>
                                            <td> {{ $d->email }} </td>
                                            <td> {{ $d->phone }}</td>
                                            <td>
                                                @if($d->verify_status ==1)
                                                    <span class="uk-badge">Verified</span>
                                                @else
                                                <span class="uk-badge" style="background-color:red;">Unverified</span>
                                                @endif 
                                            </td>
                                            <td>
                                                <a href="{{ route('admin.user_detail',['id'=>encrypt($d->id)]) }}" data-uk-tooltip title="View User Details"><i class="uk-icon-eye  uk-text-primary uk-icon-small"></i></a>    
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="8">No Record Found</td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')
 
@endsection
