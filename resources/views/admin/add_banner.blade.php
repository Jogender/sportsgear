@extends('admin.layouts.main')
@section('content')
    <div id="page_content_inner">
        <!-- User Data -->
        
        <h4 class="heading_a uk-margin-bottom">Banner</h4>        
        <div class="md-card uk-margin-medium-bottom">
            <div class="md-card-content">
                <div class="dt_colVis_buttons"></div>
                <input type="hidden" value="" id="video_detail" />
                <table id="dt_default" class="uk-table" cellspacing="0" width="100%">
                    <thead>
                        <tr> 
                            <th>Image</th>   
                            <th>Image Linking</th> 
                            <th>Banner enable</th> 
                            <th>Publish</th>                          
                            <th>Action </th>
                            <th class="hide">Action </th>
                            <th class="hide">Action </th>
                            <th class="hide">Action </th>
                            <th class="hide">Action </th>

                        </tr>
                    </thead>                        

                    <tbody>
                        @foreach($data as $d)
                            <tr id="row{{$d->id}}">  
                                <td class="width-50" ><img style="height: 100px; width: 25%;" src="{{URL::asset('image/'.$d->image)}}"> </td> 
                                <td><span class="uk-badge @if($d->image_link == 'yes') uk-badge-success @else uk-badge-danger  @endif "> {{ucfirst($d->image_link)}} </span> </td>
                                <td><span class="uk-badge @if($d->banner_enable == 'yes') uk-badge-success @else uk-badge-danger  @endif "> {{ucfirst($d->banner_enable)}}</span> </td>

                                <td><a href="javascript:void(0);" @if($d->publish == 0) onClick=publish('{{$d->id}}','1') @else onClick=publish('{{$d->id}}','0')  @endif ><span class="uk-badge @if($d->publish == 0) uk-badge-success @else uk-badge-danger  @endif "> @if($d->publish == 0) Publish  @else Unpublish @endif</span><a/> </td>
                                <td>                 
                                    <a href="javascript:void(0);" onClick="edit_video({{$d->id}})" title="Edit " ><i class="uk-icon-edit uk-icon-small"></i></a>
                                    <a href="javascript:void(0);" onClick="delete_video({{$d->id}})" title="Delete " ><i class="uk-icon-trash uk-icon-small"></i></a>
                                </td> 
                                <td class="hide">{{$d->link_type}}</td>
                                <td class="hide">{{$d->link_id}}_{{$d->slug_name}}</td>
                                <td class="hide">@if($d->image_link == 'yes')  1 @else 0 @endif</td>
                                <td class="hide">@if($d->banner_enable == 'yes')  1 @else 0 @endif</td>

                            </tr>
                        @endforeach  
                    </tbody>
                </table>
            </div>
        </div>

        <div class="md-fab-wrapper">
            <a  onclick="add('0')" title="Add Topic" class="md-fab md-fab-accent" href="javascript:;" id="recordAdd">
                <i class="material-icons">&#xE145;</i>
            </a>
        </div>
    </div>

    {{-- Edit User model --}}
   
        <div id="modal_overflow" class="uk-modal">
            <div class="uk-modal-dialog">
                <button type="button" class="uk-modal-close uk-close"></button>
                <h2 class="heading_a ui-dialog-title"> <strong>Banner </strong></h2><br>
                <form enctype="multipart/form-data" id="add_post">
                {{ csrf_field() }}
                    <input type="hidden"   name="id" id="id"  value="">                   

                    <div class="jtable-input-field-container">
                        <div class="jtable-input-label">Image</div>
                        <div class="jtable-input jtable-text-input">
                            <div class="md-input-wrapper md-input-filled">
                                <input accept="image/*"  type="file" id="image" required  value="" name="image"  class="dropify" />
                                <span class="md-input-bar"></span>
                            </div>
                        </div>
                    </div> <br/>

                    <div class="uk-width-medium-1-1">
                        <label for="fullname">Banner Enable<span class="req">*</span></label>
                        <div class="parsley-row">
                            <div class="uk-width-medium-3-5">
                                <input type="radio" name="banner_enable" id="radio_112" value="yes" checked />
                                <label for="radio_112" class="inline-label">Yes</label>

                                <input type="radio" name="banner_enable" id="radio_113" value="no" />
                                <label for="radio_113" class="inline-label" >No</label>
                            </div>
                        </div>
                    </div> <br/>

                    <div class="uk-width-medium-1-1">
                        <label for="fullname">Image Link<span class="req">*</span></label>
                        <div class="parsley-row">
                            <div class="uk-width-medium-3-5">
                                <input type="radio" name="link" id="radio_demo_inline_1123" value="yes"/>
                                <label for="radio_demo_inline_1123" class="inline-label">Yes</label>

                                <input type="radio" name="link" id="radio_demo_inline_2123" value="no" checked />
                                <label for="radio_demo_inline_2123" class="inline-label" >No</label>
                            </div>
                        </div>
                    </div> <br/>

                    <div class="bannerLinking">

                        <div class="uk-width-medium-1-1">
                            <label for="fullname">Link via<span class="req">*</span></label>
                            <div class="parsley-row">
                                <div class="uk-width-medium-3-5">
                                    <input type="radio" name="link_type" id="radio_demo_inline_101" value="brand"  checked />
                                    <label for="radio_demo_inline_101" class="inline-label">Brand</label>

                                    <input type="radio" name="link_type" id="radio_demo_inline_102" value="category"/>
                                    <label for="radio_demo_inline_102" class="inline-label">Category</label>

                                    <input type="radio" name="link_type" id="radio_demo_inline_103" value="product"/>
                                    <label for="radio_demo_inline_103" class="inline-label">Product</label>
                                </div>
                            </div>
                        </div> <br/>

                        <div class="brandDiv">
                            <div class="jtable-input-label">Brand</div>
                            <div class="jtable-input jtable-text-input">
                                <div class="md-input-wrapper md-input-filled">
                                    <select name="brand" id="brand" class="md-input form-control">
                                    <option value="">Select One.</option>
                                    @foreach($brand as $b)
                                        <option value="{{$b->id}}_{{$b->slug_name}}">{{$b->name}}</option>
                                    @endforeach
                                    </select>
                                    <span class="md-input-bar"></span>
                                </div>
                            </div>
                        </div>

                        <div class="categoryDiv">
                            <div class="jtable-input-label ">Category</div>
                            <div class="jtable-input jtable-text-input">
                                <div class="md-input-wrapper md-input-filled">
                                    <select name="category" id="category" class="md-input form-control">
                                    <option value="">Select One.</option>
                                    @foreach($category as $c)
                                        <option value="{{$c->id}}_{{$c->slug_name}}">{{$c->name}}</option>
                                    @endforeach
                                    </select>
                                    <span class="md-input-bar"></span>
                                </div>
                            </div>
                        </div>

                        <div class="productDiv">
                            <div class="jtable-input-label ">Product</div>
                            <div class="jtable-input jtable-text-input">
                                <div class="md-input-wrapper md-input-filled">
                                    <select name="product" id="product" class="md-input form-control">
                                    <option value="">Select One.</option>
                                    @foreach($product as $p)
                                        <option value="{{$p->id}}_{{$p->slug_name}}">{{$p->name}}</option>
                                    @endforeach
                                    </select>
                                    <span class="md-input-bar"></span>
                                </div>
                            </div>
                        </div>

                    </div>


                    <div class="uk-modal-footer uk-text-right">
                        <button type="button" class="md-btn md-btn-flat uk-modal-close">Close</button>
                        <button  type="submit" id="save"  class="md-btn md-btn-flat md-btn-flat-primary">Save</button>
                        <button  style="display:none;" id="wait"  class="md-btn md-btn-flat md-btn-flat-primary"><i class="uk-icon-spinner uk-icon-medium uk-icon-spin"></i>Please Wait...</button>
                    </div>
                </form>                
            </div>
        </div>    
    {{-- End model --}}


@endsection
@section('js')
    <script>

        //Edit video  model 
    function edit_video(id)
    {
        var modal = UIkit.modal("#modal_overflow");
        if ( modal.isActive() ) {
            modal.hide();
        } else {
            modal.show();
        }
        var ids="#row"+id; 
        var currentRow=$(ids).closest("tr"); 

        $("#id").val(id);
        $('#image').prop('required',false);

        var bannerEnable=currentRow.find("td:eq(8)").text();
        var type=currentRow.find("td:eq(5)").text();
        var id=currentRow.find("td:eq(6)").text();
        
        var imageLinking=currentRow.find("td:eq(7)").text();

        if(imageLinking=="1" || imageLinking==1){
            $("#radio_demo_inline_1123").attr('checked', 'checked');
            $(".bannerLinking").css('display','block');
        }else{
            $("#radio_demo_inline_2123").attr('checked', 'checked');
            $(".bannerLinking").css('display','none');
        } 


        //banner enable
        if(bannerEnable == '1' || bannerEnable == 1){            
            $("#radio_112").attr('checked', 'checked');
        }else{            
            $("#radio_113").attr('checked', 'checked');
        }


        

        if(type == 'brand'){
            $("#radio_demo_inline_101").attr('checked', 'checked');

            $(".brandDiv").css('display','block');
            $(".categoryDiv").css('display','none');
            $(".productDiv").css('display','none');
        }else if(type == 'category'){
            $("#radio_demo_inline_102").attr('checked', 'checked');
            
            $(".categoryDiv").css('display','block');
            $(".brandDiv").css('display','none');
            $(".productDiv").css('display','none');
        }else if(type = 'product'){
            $("#radio_demo_inline_103").attr('checked', 'checked');

            $(".productDiv").css('display','block');
            $(".categoryDiv").css('display','none');
            $(".brandDiv").css('display','none');
        }

        $('select[name^="'+type+'"] option[value="'+id+'"]').attr("selected","selected");

    }


    //Banner link 
    $('input[type=radio][name=link]').change(function() {
        if (this.value == 'yes') {
            $(".bannerLinking").css('display','block');
        }
        else if (this.value == 'no') {
            $(".bannerLinking").css('display','none');
        }
    });

    //Brand category product listing
    $('input[type=radio][name=link_type]').change(function() {
        if (this.value == 'brand') {            
            $(".brandDiv").css('display','block');

            $(".categoryDiv").css('display','none');
            $(".productDiv").css('display','none');
        }
        else if (this.value == 'category') {
            $(".categoryDiv").css('display','block');

            $(".brandDiv").css('display','none');
            $(".productDiv").css('display','none');
        }
        else if (this.value == 'product'){
            $(".productDiv").css('display','block');

            $(".categoryDiv").css('display','none');
            $(".brandDiv").css('display','none');
        }
    });



    //add video model 
    function add(id)
    {
        var modal = UIkit.modal("#modal_overflow");

        if ( modal.isActive() ) {
            modal.hide();
        } else {
            modal.show();
        }
    }

    

    //Publish
    function publish(id,type)
    {
        if(type == 0)
        {
            $msg="Are you sure want to publish Banner";
        }
        else{
            $msg="Are you sure want to unpublish Banner";
        }

        UIkit.modal.confirm($msg, 
            function(){                
                $.ajax({		            	
                    type: "POST",
                    url: `${window.pageData.baseUrl}/api/publish_banner`,
                    data: {
                            "_token": "{{ csrf_token() }}",
                            "id": id,
                            "type":type
                            } ,                                                         
                    success: function(data)
                    {
                        if(data.status == 'success')
                        {
                            UIkit.modal.alert('Record has been Updated!'); 
                            setInterval(function() {
                                location.reload();
                            }, 2000);
                        }
                        else
                        {
                            UIkit.modal.alert(data.message); 
                        }
                        
                    }
                });
             });
    }

    //Delete video
    function delete_video(id)
    {       
        UIkit.modal.confirm('Are you sure to delete Banner?', 
            function(){                
                $.ajax({		            	
                    type: "POST",
                    url: `${window.pageData.baseUrl}/api/delete_banner`,
                    data: {
                            "_token": "{{ csrf_token() }}",
                            "id": id
                            } ,                                                         
                    success: function(data)
                    {
                        if(data.status == 'success')
                        {
                            $('#row' + id).remove();
                            UIkit.modal.alert('Record has been successfully deleted !'); 
                        }
                        else
                        {
                            UIkit.modal.alert(data.message); 
                        }
                        
                    }
                });
             });
    }
   
    //ajax for add video
    $(document).ready(function() {

        // hide bannerLinkingDiv

        $(".bannerLinking").css('display','none');
        $(".categoryDiv").css('display','none');
        $(".productDiv").css('display','none');



        $('#add_post').submit(function(e){
            e.preventDefault();
            var form = $('#add_post')[0];
            var data = new FormData(form);            
            $("#save").hide(); 
            $("#wait").show(); 
            $.ajax({		            	
                    type: "POST",
                    url: `${window.pageData.baseUrl}/api/banner`,
                    enctype: 'multipart/form-data',
                    data: data,
                    processData: false,
                    contentType: false,
                    cache: false,                                       
                    success: function(data)
                    {
                        if(data.status == 'success')
                        {
                            UIkit.notify({ message: data.message, status: 'success', timeout: 5000, group: null, pos: 'bottom-center' });
                            setInterval(function() {
                                location.reload();
                            }, 2000);

                            var modal = UIkit.modal("#modal_overflow");
                            modal.hide();
                        }
                        else
                        {
                            UIkit.notify({ message: data.message, status: 'warning', timeout: 5000, group: null, pos: 'bottom-center' });
                        }
                        
                        $("#save").show(); 
                        $("#wait").hide();

                    }
                });
        });



        
    });
       
    </script>


@endsection
