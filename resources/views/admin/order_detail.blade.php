@extends('admin.layouts.main')

@section('content')
    <div id="page_content">
        <!-- <div id="top_bar">
            <ul id="breadcrumbs">
                <li><a href="#">Dashboard</a></li>
                <li><a href="#">Order</a></li>
                <li><span>Order Details</span></li>
            </ul>
        </div> -->
        <div id="page_content_inner">


            <div class="md-card uk-width-large-12-12 uk-container-center" style="margin:0px;padding:0px;">

                <div class="md-card-toolbar">
                    <h3 class="heading_b uk-margin-bottom uk-margin-top uk-text-center">Invoice</h3>
                </div>

                <div class="md-card-content large-padding">
                    <div class="uk-grid" data-uk-grid-margin>
                        <div class="uk-width-1-2 ">
                            <h4>Invoice: &nbsp;&nbsp;&nbsp;&nbsp;<a href="{{ route('order_details',['id'=>encrypt($data->id)]) }}">{{ strtoupper($data->order_id) }}</a></h4>
                            <span class=" uk-text-muted uk-text-upper uk-text-small uk-float-left">Date: &nbsp;&nbsp;&nbsp;&nbsp;{{ date('d-m-Y',strtotime($data->created_at)) }}</span><br>
                            <span class=" uk-text-muted uk-text-upper uk-text-small uk-float-left">Payment Mode: &nbsp;&nbsp;&nbsp;&nbsp;<b>{{ $data->payment_type }}</b></span><br>
                            <span class=" uk-text-muted uk-text-upper uk-text-small uk-float-left">Order Status: &nbsp;&nbsp;&nbsp;&nbsp;
                                @if($data->status==0)
                                    <span class="uk-badge">Pending</span>
                                @elseif($data->status ==1)
                                    <span class="uk-badge uk-badge-info">Accepted</span>
                                @elseif($data->status ==2)
                                    <span class="uk-badge uk-badge-primary">Packed</span>
                                @elseif($data->status ==3)
                                    <span class="uk-badge uk-badge-default">Dispatched</span>
                                @elseif($data->status ==4)
                                    <span class="uk-badge uk-badge-success">Delivered</span>
                                @elseif($data->status ==5)
                                    <span class="uk-badge uk-badge-warning">Cancelled</span>
                                @else
                                    <span class="uk-badge uk-badge-danger">Rejected</span>
                                @endif
                            </span><br>
                            {{--<span class=" uk-text-muted uk-text-upper uk-text-small uk-float-left">Payment Status: &nbsp;&nbsp;&nbsp;&nbsp;
                                @if($data->pay_status==0)
                                    <span class="uk-badge">Pending</span>
                                @elseif($data->pay_status ==1)
                                    <span class="uk-badge uk-badge-success">Accepted</span>
                                @elseif($data->pay_status ==2)
                                    <span class="uk-badge uk-badge-danger">Cancelled</span>
                                @endif
                            </span><br>--}}

                            <span class=" uk-text-muted uk-text-upper uk-text-small uk-float-left">Transaction ID: &nbsp;&nbsp;&nbsp;&nbsp;{{ $data->transaction_id }}</span><br>
                        </div>
                        <div class="uk-width-1-2">
                            <h2 class="uk-float-right"><i class="uk-icon-rupee"></i>{{ $data->amount }}</h2>
                        </div>
                    </div>
                    <br><br>
                    <div class="uk-grid" data-uk-grid-margin>
                        <div class="uk-width-medium-3-10 uk-width-large-4-10 ">
                            <div class="md-card " style="height: 250px;">
                                <div class="md-card-toolbar">
                                    <h3 class="md-card-toolbar-heading-text">
                                        User Details
                                    </h3>
                                </div>
                                <div class="md-card-content">
                                    <h2>{{ $user->name }}</h2>
                                    <p style="word-break:break-all;">
                                        Email:&nbsp;&nbsp;&nbsp;&nbsp; {{ $user->email }}</p><br><br> 
                                    <p> Mobile:&nbsp;&nbsp;&nbsp;&nbsp;{{ $user->phone }}<br>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="uk-width-medium-2-10">
                        </div>
                        <div class="uk-width-medium-3-10 uk-width-large-4-10 ">
                            <div class="md-card " style="height: 250px;">
                                <div class="md-card-toolbar">
                                    <h3 class="md-card-toolbar-heading-text">
                                        Shipping Address
                                    </h3>
                                </div>
                                <div class="md-card-content">
                                   <p>
                                        {{ $data->dl_address }}
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br>
                    <br>
                   <div class="uk-grid" data-uk-grid-margin>
                        <h3>Product Details</h3>
                        <div class="uk-width-medium-1-1">
                            <table class="uk-table uk-table-striped uk-table-hover">
                                <thead>
                                <tr>
                                    <th>Image</th>
                                    <th>Product</th>
                                    <th>Variant</th>
                                    <th>Quantity</th>
                                    <th>Price</th>
                                    <th>Total</th>
                                </tr>
                                </thead>
                                @php $product_details= json_decode($data->product_detail);
                                $sum = 0;
                                @endphp
                                <tbody>
                                @foreach($product_details as $key)
                                    @foreach($products as $product) @if($key->product_id==$product->id) @php $prod=$product; @endphp @endif @endforeach
                                    <tr>
                                        @php $images=json_decode($prod->image); @endphp
                                        <td><img src="{{ asset('image/product/'.$images[0]) }}" height="60" width="80"></td>
                                        <td>{{ ucfirst($prod->name) }}</td>
                                        <td>@if($data->variant_status=='yes') @php $prod_details=json_decode($key->details); @endphp {{ implode(", ",$prod_details) }} @endif</td>
                                        <td>{{ $key->quantity }}</td>
                                        <td><i class="uk-icon-rupee"></i>{{ $prod->new_price }}</td>
                                        <td><i class="uk-icon-rupee"></i>{{ $key->quantity*$key->price }}</td>
                                    </tr>
                                    @php $sum = $sum + $key->quantity*$key->price; @endphp
                                @endforeach
                                <tr>
                                    <td colspan="4"><b>Total</b></td>
                                    <td colspan="2"><i class="uk-icon-rupee"></i>{{$sum}}</td>
                                </tr>
                                <tr>
                                    <td colspan="4"><b>Shipping Charge</b></td>
                                    <td colspan="2"><i class="uk-icon-rupee">{{$data->dl_charge}}</i></td>
                                </tr>
                                <tr>
                                    <td colspan="4"><b>Net Amount Payable</b></td>
                                    <td colspan="2"><i class="uk-icon-rupee"></i>{{$sum+$data->dl_charge}}</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection
