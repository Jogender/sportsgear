
<script> 





    //remove image in edit page
      //model for image remove
        function remove_image(image,image_name)
        {
            var name="#image_div"+image_name;
            var id=$('#id').val();

            $.ajax({

                type:'post',
                url: `${window.pageData.baseUrl}/api/remove_product_file`,
                data: {id:id,image:image},
                success: function(data) {
                    if(data.status == 'success')
                    {
                        $(name).remove();
                        toast(data.message,'success');   
                    }   
                    else
                    {
                        toast(data.message,'warning');
                    } 
                }

            });
            

        }



    $(function(){
        $('#category').on('change', function(){
            var value = $('#category').val();
            $('#subsubcategory').empty();
                $.ajax({
                type:'post',
                    url: `${window.pageData.baseUrl}/api/get_subcategory`,
                    data: {
                        cat_id:value,
                    },
                    success: function(data) {
                        $("#subcategory").empty();

                        var appenddata1='';
                        if(data.data.length > 0){
                            for (var i = 0; i < data.data.length; i++) {                                
                                appenddata1 += "<option value = '" + data.data[i].id + " '>" + data.data[i].name + " </option>";
                            }
                       
                            $('#subcategory').prepend('<option value="">Select Subcategory</opton>');
                            $("#subcategory").append(appenddata1);
                        }

                    }
                });
            });
    });

    $(function(){
        $('#subcategory').on('change', function(){
            var cat = $('#category').val();
            var subcat = $('#subcategory').val();
            $('#subsubcategory').empty();
                $.ajax({
                type:'post',
                    url: `${window.pageData.baseUrl}/api/get_subsubcategory`,
                    data: {
                        cat_id:cat,
                        subcat_id:subcat,
                    },
                    success: function(data) {
                        $("#subsubcategory").empty();
                        var appenddata1='';
                        if(data.data.length > 0){

                            for (var i = 0; i < data.data.length; i++) {
                                console.log(data.data[i].name);
                                appenddata1 += "<option value = '" + data.data[i].id + " '>" + data.data[i].name + " </option>";
                            }
                            $('#subsubcategory').prepend('<option value="">Select Subsubcategory</opton>');
                            $("#subsubcategory").append(appenddata1);
                        }

                    }
                });
            });
    });

    //show variant wrapper on chnage radio button
    $('input[type=radio][name=variant_status]').change(function() {

    if (this.value == 'yes') {
        $(".variants-wrapper").css('display','block');
        $("#old_price").css('display','none');
        $("#new_price").css('display','none');
        $("#quantity").css('display','none');
        $("#description").css('display','none');
    }
    else if (this.value == 'no') {
        $(".variants-wrapper").css('display','none');
        $("#old_price").css('display','block');
        $("#new_price").css('display','block');
        $("#quantity").css('display','block');
        $("#description").css('display','block');
    }
});


//Add key feature
    function addkey(keyId){

        
        //Key ids
            var currentkeyId=parseInt($('#keyIds').val());
            var increaseKeyID=currentkeyId +1;   
            $('#keyIds').val(increaseKeyID);
            var appendkeyId="keyIds"+increaseKeyID;

        //Value Ids
            var currentvalueId=parseInt($('#valueIds').val());
            var increaseValueID=currentvalueId +1;   
            $('#valueIds').val(increaseValueID);
            var appendId="variant"+increaseValueID;



        var fieldHTML= "<div class='variants-wrapper' id='keyIds"+increaseKeyID+"'>"+
                            "<div class='variants field_wrapper'>"+
                                "<hr class='form_hr'>"+
                                "<div class='row'>"+
                                    "<div class='md-card'>"+
                                        "<div class='md-card-content'>"+
                                            "<div class='uk-grid' id='variant"+increaseValueID+"'  data-uk-grid-margin>"+

                                            "<div class='uk-width-medium-1-6'>"+
                                                    "<label for='fullname'>Key<span>*</span></label>"+
                                                    "<div class='parsley-row'>"+
                                                        "<input type='text' name='key"+increaseKeyID+"[]' autocomplete='off' placeholder='Enter key'  class='md-input mobile' />"+
                                                    "</div>"+
                                                "</div>"+
                                                
                                                "<div class='uk-width-medium-1-6'>"+
                                                    "<label for='fullname'>Value<span >*</span></label>"+
                                                    "<div class='parsley-row'>"+
                                                        "<input type='text' name='key"+increaseKeyID+"value1[]' autocomplete='off' placeholder='Enter Value'  class='md-input mobile' />"+
                                                    "</div>"+              
                                                "</div> "+

                                                "<div class='uk-width-medium-1-6'>"+
                                                    "<label for='fullname'>Price<span >*</span></label>"+
                                                    "<div class='parsley-row'>"+
                                                        "<input type='text' name='key"+increaseKeyID+"price1' autocomplete='off' placeholder='Enter Price'  class='md-input mobile' />"+
                                                    "</div>"+                                                
                                                "</div>"+

                                                "<div class='uk-width-medium-1-6'>"+
                                                    "<label for='fullname'>Quantity<span >*</span></label>"+
                                                    "<div class='parsley-row'>"+
                                                        "<input type='text' name='key"+increaseKeyID+"quantity1[]' autocomplete='off' placeholder='Enter Quantity'  class='md-input mobile' />"+
                                                    "</div> "+                                               
                                                "</div> "+


                                                "<div class='uk-width-medium-1-6'>"+
                                                    "<label for='fullname'>Price changes<span class='req'>*</span></label>"+
                                                    " <div class='parsley-row'>"+
                                                        " <input type='checkbox'  name='key"+increaseKeyID+"pricechange1[]'  value='on' id='switch_demo' />"+
                                                    "</div>"+
                                                " </div>"+
                                                
                                                "<div class='uk-width-medium-1-6'>"+
                                                    "<a href='javascript:void(0);' class='add_value' onclick=addValue('key"+increaseKeyID+"','"+appendId+"') title='Add field'><i class='fa fa-plus'> Add Value</i></a>"+
                                                "</div>"+

                                                "<div class='uk-width-medium-1-6'>"+
                                                    "<a href='javascript:void(0);' class='add_value' onclick=removeKey("+increaseKeyID+") title='Add field'><i class='fa fa-plus'> Remove</i></a>"+
                                                "</div>"+
                                            "</br>"+
                                                
                                            "</div></br>"+
                                        "</div>"+
                                    "</div>"+
                                "</div>"+
                            "</div>"+
                        "</div>";

        $('#keyIds1').append(fieldHTML);

        //Add new Input
        var newIDValue="keyCount"+increaseKeyID;

        var newInput="<input type='hidden'  name='"+newIDValue+"' id='key" + increaseKeyID + "' value='1' >";

        $('#keyCountDiv').append(newInput);                                            
    }


//Add value feature
    function addValue(keyID,valueId){

        var currentvalueId=parseInt($('#valueIds').val());
        var increaseValueID=currentvalueId +1;
   
        $('#valueIds').val(increaseValueID);
        var appendId="variant"+increaseValueID;
        
        var keyCount=parseInt($('#'+keyID).val());        
        $('#'+keyID).val(keyCount+1);

        var keyValueAdd=parseInt($('#'+keyID).val());

       

        var fieldHTML=  "<div class='uk-grid' id='variant"+increaseValueID+"' data-uk-grid-margin>"+
                            "<div class='uk-width-medium-1-6'>"+
                            
                            "</div>"+
                                            
                            "<div class='uk-width-medium-1-6'>"+
                                "<label for='fullname'>Value<span >*</span></label>"+
                                "<div class='parsley-row'>"+
                                    "<input type='text' name='"+keyID+"value"+keyValueAdd+"[]' autocomplete='off' placeholder='Enter Value'  class='md-input mobile' />"+
                                "</div>"+                                                
                            "</div>"+

                            "<div class='uk-width-medium-1-6'>"+
                                "<label for='fullname'>Price<span >*</span></label>"+
                                "<div class='parsley-row'>"+
                                    "<input type='text' name='"+keyID+"price"+keyValueAdd+"[]' autocomplete='off' placeholder='Enter Price'  class='md-input mobile' />"+
                                "</div>"+                                               
                            "</div>"+

                            "<div class='uk-width-medium-1-6'>"+
                                "<label for='fullname'>Quantity<span >*</span></label>"+
                                "<div class='parsley-row'>"+
                                    "<input type='text' name='"+keyID+"quantity"+keyValueAdd+"[]' autocomplete='off' placeholder='Enter Quantity'  class='md-input mobile' />"+
                                "</div> "+                                               
                            "</div> "+

                            "<div class='uk-width-medium-1-6'>"+
                                "<label for='fullname'>Price changes<span class='req'>*</span></label>"+
                                " <div class='parsley-row'>"+
                                    " <input type='checkbox'  name='"+keyID+"pricechange"+keyValueAdd+"[]' value='on' id='switch_demo' />"+
                                "</div>"+
                            " </div>"+
                                            
                            "<div class='uk-width-medium-1-6'>"+
                                "<a href='javascript:void(0);' class='add_value' onclick=removeValue('"+keyID+"',"+appendId+") title='Add field'><i class='fa fa-plus'> Remove</i></a>"+
                            "</div></br>"+

                        "</div>";

        $('#'+valueId).append(fieldHTML);
                 
        
    }


    function removeValue(keyID,removeID){	

        var keyCount=parseInt($('#'+keyID).val());                              
        $('#'+keyID).val(keyCount-1); 
        
		$(removeID).remove();
    }

    function removeKey(removeID){	
        var keyCount=parseInt($('#keyIds').val());                              
        $('#keyIds').val(keyCount-1);
      	
		$('#keyIds'+removeID).remove();
        $('#key'+removeID).remove();
    }


</script>

<script type="text/javascript">
$(document).ready(function(){

    //Slug name for category
    $("#name").change(function(){        
        const name=$('#name').val();
        $('#slug').val(slugName(name));            
    });


    var maxField = 10; //Input fields increment limitation
    var addButton = $('.add_button'); //Add button selector
    var wrapper = $('.field_wrapper'); //Input field wrapper
    var fieldHTML = '<div><input type="text" name="field_name[]" value=""/><a href="javascript:void(0);" class="remove_button"><img src="remove-icon.png"/></a></div>'; //New input field html 
    var fieldHTML ="<div class='variants-wrapper'><div class='variants field_wrapper'><hr class='form_hr'>"+
                        +"<div class='row'><div class='md-card'><div class='md-card-content'>"+
                                    "<div class='uk-grid' data-uk-grid-margin'><div class='uk-width-medium-1-2'>"+
                                            "<label for='fullname'>Driver Hand<span class='req'>*</span></label>"+
                                            "<div class='parsley-row'>"+
                                                "<div class='uk-width-medium-3-5'>"+
                                                    "<span class='icheck-inline'>"+
                                                        "<input type='radio' name='driver[]' id='radio_demo_inline_1dfdg' value='L' checked/>"+
                                                        "<label for='radio_demo_inline_1dfdg' class='inline-label'>Left</label></span>"+
                                                    "<span class='icheck-inline'>"+
                                                        "<input type='radio' name='driver[]' id='radio_demo_inline_2gdfg' value='R'/>"+
                                                        "<label for='radio_demo_inline_2gdfg' class='inline-label'>Right</label></span>"+
                                                "</div>"+
                                            "</div>"+
                                        "</div>"+ 
                                        "<div class='uk-width-medium-1-2'>"+
                                            "<label for='fullname'>Weight<span class='req'>*</span></label>"+
                                            "<div class='parsley-row'>"+
                                                "<input type='text' name='weight[]' autocomplete='off' placeholder='Enter weight' required class='md-input mobile' />"+
                                            "</div>"+
                                        "</div>"+
                                        "<div class='uk-width-medium-1-2'>"+
                                            "<label for='fullname'>Quantity<span class='req'>*</span></label>"+
                                            "<div class='parsley-row'>"+
                                                "<input type='text' name='quantity[]' autocomplete='off' placeholder='Enter quantity' required class='md-input mobile'/>"+
                                            "</div>"+
                                        "</div>"+
                                        "<div class='uk-width-medium-1-2'>"+
                                            "<label for='fullname'>Discounted Price<span class='req'>*</span></label>"+
                                            "<div class='parsley-row'>"+
                                                    "<input type='text' name='max_price[]' autocomplete='off' class='md-input mobile'/>"+
                                            "</div>"+
                                        "</div>"+
                                        "<div class='uk-width-medium-1-2'>"+
                                            "<label for='fullname'>New Price<span class='req'>*</span></label>"+
                                            "<div class='parsley-row'>"+
                                                    "<input type='text' name='retail_price[]' autocomplete='off' placeholder='Enter retail price' class='md-input mobile'/>"+
                                            "</div>"+
                                        "</div>"+
                                    "</div>"+
                                    "<a href='javascript:void(0);' style='float:right;' class='remove_button' title='Add field'><i class='fa fa-minus'></i></a>"+
                                "</div>"+
                            "</div>"+
                        "</div>"+
                    "</div>"+
                "</div><br>";
    var x = 1; //Initial field counter is 1
    
    //Once add button is clicked
    $(addButton).click(function(){
        //Check maximum number of input fields
        if(x < maxField){ 
            x++; //Increment field counter
            $(wrapper).append(fieldHTML); //Add field html
        }
    });
    
    //Once remove button is clicked
    $(wrapper).on('click', '.remove_button', function(e){
        e.preventDefault();
        $(this).parent('div').remove(); //Remove field html
        x--; //Decrement field counter
    });
});



</script>