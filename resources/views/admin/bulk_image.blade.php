@extends('admin.layouts.main')

@section('content')
<div id="page_content_inner">
    <h2 class="heading_b uk-margin-bottom">Bulk image upload</h2>
    <div class="md-card uk-margin-large-bottom">
        <div class="md-card-content">
            <form method="post" action="{{route('image_upload')}}" id="form_validation" enctype="multipart/form-data"
                class="uk-form-stacked task-form">
                @csrf

                <div class="uk-width-medium-1-1">
                    <label for="fullname">Image<span class="req">*</span></label>
                    <div class="parsley-row">
                        <input type="file" name="filenames[]" multiple id="name" autocomplete="off" placeholder="Enter name" required class="md-input task" />
                    </div>
                </div>

                <br>
                <div class="uk-grid">
                    <div class="uk-width-forms_wysiwyg_ckeditor1-1">
                        <button type="submit" class="md-btn" style="background-color:#e2932d;color: white;">Submit</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
@section('js')
   
@endsection
