@extends('admin.layouts.main')
@section('css')

@endsection
@section('content')
    <div id="page_content_inner">
        <!-- User Data -->
        <h4 class="heading_a uk-margin-bottom">SubCategory</h4>        
        <div class="md-card uk-margin-medium-bottom">
            <div class="md-card-content"> 
                <div class="dt_colVis_buttons"></div>
                <input type="hidden" value="" id="video_detail" />
                <table id="dt_default" class="uk-table" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>  
                            <th>Slug</th>  
                            <th>Category</th> 
                            <th>Image</th>   
                            <th>Publish</th>                          
                            <th>Action </th>
                        </tr>
                    </thead>                        

                    <tbody>
                        @foreach($data as $d)
                            <tr id="row{{$d->id}}">  
                                <td>{{$d->id}}</td>
                                <td>{{$d->name}}</td>
                                <td>{{$d->slug_name}}</td>
                                <td>{{$d->catname}}</td>
                                <td class="width-50" >@if($d->image != '') <img style="height: 100px; width: 25%;" src="{{URL::asset('image/'.$d->image)}}"> @endif </td> 
                                <td><a href="javascript:void(0);" @if($d->publish == 0) onClick=publish('{{$d->id}}','1') @else onClick=publish('{{$d->id}}','0')  @endif ><span class="uk-badge @if($d->publish == 0) uk-badge-success @else uk-badge-danger  @endif "> @if($d->publish == 0) Publish  @else Unpublish @endif</span><a/> </td>
                                <td>                 
                                    <a href="javascript:void(0);" onClick="edit_video({{$d->id}})" title="Edit " ><i class="uk-icon-edit uk-icon-small"></i></a>
                                    <a href="javascript:void(0);" onClick="delete_video({{$d->id}})" title="Delete " ><i class="uk-icon-trash uk-icon-small"></i></a>
                                </td> 
                            </tr>
                        @endforeach  
                    </tbody>
                </table>
            </div>
        </div>

        <div class="md-fab-wrapper">
            <a  onclick="add('0')" title="Add Topic" class="md-fab md-fab-accent" href="javascript:;" id="recordAdd">
                <i class="material-icons">&#xE145;</i>
            </a>
        </div>
    </div>

    {{-- Edit User model --}}
   
        <div id="modal_overflow" class="uk-modal">
            <div class="uk-modal-dialog">
                <button type="button" class="uk-modal-close uk-close"></button>
                <h2 class="heading_a ui-dialog-title"> <strong>SubCategory </strong></h2><br>
                <form enctype="multipart/form-data" id="add_post">
                {{ csrf_field() }}
                    <input type="hidden"   name="id" id="id"  value="">    

                        <div class="jtable-input-label">Name</div>
                            <div class="jtable-input jtable-text-input">
                                <div class="md-input-wrapper md-input-filled">
                                    <input class="md-input" required  id="name" type="text" value=""  name="name">
                                    <span class="md-input-bar"></span>
                                </div>
                        </div> 
                        
                        <div class="jtable-input-label">Slug Name</div>
                        <div class="jtable-input jtable-text-input">
                            <div class="md-input-wrapper md-input-filled">
                                <input class="md-input" required  id="slug" type="text" value=""  name="slug">
                                <span class="md-input-bar"></span>
                            </div>
                        </div>

                        <div class="jtable-input-label">Category</div>
                        <div class="jtable-input jtable-text-input">
                            <div class="md-input-wrapper md-input-filled">
                                <select name="category" id="category" class="md-input form-control" required>
                                    <option value="">Select One.</option>
                                    @foreach($category as $c)
                                    <option value="{{$c->id}}">{{$c->name}}</option>
                                    @endforeach
                                    </select>
                                <span class="md-input-bar"></span>
                            </div>
                        </div>   

                        
                        <div class="jtable-input-field-container">
                            <div class="jtable-input-label">Image</div>
                            <div class="jtable-input jtable-text-input">
                                <div class="md-input-wrapper md-input-filled">
                                    <input accept="image/*"  type="file" id="image"   value="" name="image"  class="dropify" />
                                    <span class="md-input-bar"></span>
                                </div>
                            </div>
                        </div>

                    <div class="uk-modal-footer uk-text-right">
                        <button type="button" class="md-btn md-btn-flat uk-modal-close">Close</button>
                        <button  type="submit" id="save"  class="md-btn md-btn-flat md-btn-flat-primary">Save</button>
                        <button  style="display:none;" id="wait"  class="md-btn md-btn-flat md-btn-flat-primary"><i class="uk-icon-spinner uk-icon-medium uk-icon-spin"></i>Please Wait...</button>
                    </div>
                </form>                
            </div>
        </div>    
    {{-- End model --}}


@endsection
@section('js')
    <script>
    //add video model 
    function add(id)
    {
        var modal = UIkit.modal("#modal_overflow");

        if ( modal.isActive() ) {
            modal.hide();
        } else {
            modal.show();
        }
    }

    //Edit video  model 
    function edit_video(id)
    {
        var modal = UIkit.modal("#modal_overflow");
        if ( modal.isActive() ) {
            modal.hide();
        } else {
            modal.show();
        }
        var ids="#row"+id; 
        var currentRow=$(ids).closest("tr"); 

        $("#id").val(id);
        $("#name").val(currentRow.find("td:eq(1)").text());
        $("#slug").val(currentRow.find("td:eq(2)").text());

        const categoryValue=currentRow.find("td:eq(3)").text();
        
        $('select[name="category"]').find('option:contains("'+categoryValue+'")').attr("selected",true);
      
        $('#image').prop('required',false);

    }

    //Publish
    function publish(id,type)
    {
        if(type == 0)
        {
            $msg="Are you sure want to publish Category";
        }
        else{
            $msg="Are you sure want to unpublish Category";
        }

        UIkit.modal.confirm($msg, 
            function(){                
                $.ajax({		            	
                    type: "POST",
                    url: `${window.pageData.baseUrl}/api/publish_subcategory`,
                    data: {
                            "_token": "{{ csrf_token() }}",
                            "id": id,
                            "type":type
                            } ,                                                         
                    success: function(data)
                    {
                        if(data.status == 'success')
                        {
                            UIkit.modal.alert('Record has been Updated!'); 
                            setInterval(function() {
                                location.reload();
                            }, 2000);
                        }
                        else
                        {
                            UIkit.modal.alert(data.message); 
                        }
                        
                    }
                });
             });
    }

    //Delete video
    function delete_video(id)
    {       
        UIkit.modal.confirm('Are you sure to delete Category?', 
            function(){                
                $.ajax({		            	
                    type: "POST",
                    url: `${window.pageData.baseUrl}/api/delete_subcategory`,
                    data: {
                            "_token": "{{ csrf_token() }}",
                            "id": id
                            } ,                                                         
                    success: function(data)
                    {
                        if(data.status == 'success')
                        {
                            $('#row' + id).remove();
                            UIkit.modal.alert('Record has been successfully deleted !'); 
                        }
                        else
                        {
                            UIkit.modal.alert(data.message); 
                        }
                        
                    }
                });
             });
    }
   
    //ajax for add video
    $(document).ready(function() {

        //Slug name for category
        $("#name").change(function(){        
            const name=$('#name').val();
            $('#slug').val(slugName(name));            
        });

        $('#add_post').submit(function(e){
            e.preventDefault();
            var form = $('#add_post')[0];
            var data = new FormData(form);            
            $("#save").hide(); 
            $("#wait").show(); 
            $.ajax({		            	
                    type: "POST",
                    url: `${window.pageData.baseUrl}/api/subcategory`,
                    enctype: 'multipart/form-data',
                    data: data,
                    processData: false,
                    contentType: false,
                    cache: false,                                       
                    success: function(data)
                    {
                        if(data.status == 'success')
                        {
                            UIkit.notify({ message: data.message, status: 'success', timeout: 5000, group: null, pos: 'bottom-center' });
                            setInterval(function() {
                                location.reload();
                            }, 2000);

                            var modal = UIkit.modal("#modal_overflow");
                            modal.hide();
                        }
                        else
                        {
                            UIkit.notify({ message: data.message, status: 'warning', timeout: 5000, group: null, pos: 'bottom-center' });
                        }
                        
                        $("#save").show(); 
                        $("#wait").hide();

                    }
                });
        });



        
    });
       
    </script>


@endsection
