@extends('admin.layouts.main')
@section('content')
        <div id="page_content_inner">
            <h3 class="heading_b uk-margin-bottom">Product</h3>
            <div class="md-card uk-margin-medium-bottom">
                <div class="md-card-content">
                    <br />
                    <div class="uk-width-medium-1-1">
                        <div class="uk-overflow-container">
                            <table id="dt_default" class="uk-table uk-table-striped uk-table-hover" cellspacing="0"
                                width="100%">
                                <thead>
                                <tr>                                   
                                    <th>Product Name</th>
                                    <th>Image</th>
                                    <th>Category</th>
                                    <th>Old Price</th>
                                    <th>New <br>Price</th>
                                    <th>Quantity</th>
                                    <th>Featured</th>
                                    <th>Top Weekly</th>
                                    <th>Status</th>   
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody id="ajaxresponse">
                                @if(!$data->isEmpty())
                                    @foreach($data as $key)
                                        @php $Productimage=json_decode($key->image); @endphp

                                        <tr id="row{{ $key->id }}">
                                            <td> {{$key->name}} </td>
                                            <td> @if(count($Productimage) > 0)  <img src="{{URL::asset('image/product/'.$Productimage[0])}}" height="50" width="50"> @endif  </td>
                                            <td> {{$key->category_name}} </td>
                                            <td>
                                               {{$key->old_price}}
                                            </td>
                                            <td>
                                                {{$key->new_price}}
                                            </td>
                                            <td>{{ $key->quantity }}</td>
                                            <td id="td{{$key->id}}">
                                                @if($key->featured == 1)
                                                <span class="uk-badge uk-badge-warning" >Yes</span>
                                                @else
                                                <span class="uk-badge uk-badge-danger" >No</span>
                                                @endif
                                            </td>
                                            <td id="td{{$key->id}}">
                                                @if($key->topweekly == 1)
                                                <span class="uk-badge uk-badge-warning" >Yes</span>
                                                @else
                                                <span class="uk-badge uk-badge-danger" >No</span>
                                                @endif
                                            </td>
                                            <td><a href="javascript:void(0);" @if($key->status == 0) onClick=publish('{{$key->id}}','1') @else onClick=publish('{{$key->id}}','0')  @endif ><span class="uk-badge @if($key->status == 0) uk-badge-success @else uk-badge-danger  @endif "> @if($key->status == 0) Publish  @else Unpublish @endif</span><a/> </td>
                                            
                                            <td> 
                                                <a href="{{ route('product_detail',['id'=>encrypt($key->id)])}}" > <i class="uk-icon-eye  uk-text-primary uk-icon-small "></i>  </a> 
                                                <a href="{{ route('edit_product',['id'=>encrypt($key->id)])}}" > <i class="uk-icon-pencil  uk-text-primary uk-icon-small "></i>  </a> 
                                                <a href="javascript:void(0);" onClick="delete_video({{$key->id}})" title="Delete" ><i class="uk-icon-trash uk-text-primary uk-icon-small"></i></a>
                                           
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="10">No Record Found</td>
                                    </tr>
                                @endif
                                </tbody>
                               
                            </table>
                            
                        </div>
                        
                    </div>
                    
                </div>
            </div>
        </div>
    </div>

    <div class="md-fab-wrapper">
        <a  title="Add Product" class="md-fab md-fab-accent" href="{{route('product')}}" id="recordAdd">
            <i class="material-icons">&#xE145;</i>
        </a>
    </div>

    
@endsection

@section('js')
    <script src="{{ URL::asset('backend/assets/js/common.min.js') }}"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            // $('#dt_default').dataTable({
            //     "bPaginate": true,
            //     "bLengthChange": false,
            //     "bFilter": true,
            //     "bInfo": false,
            //     "bAutoWidth": false
            // });

            $("#dt_default").on("click", ".ordered", function() {
                var id = $(this).data('id');
                var status = $(this).data('status');
                

                $('#id').val(id);
                var html = '    <option value="">Choose Status</option>\n' +
                        '    <option value="0" >Pending</option>\n' +
                        '    <option value="1" >Accepted</option>\n' +
                        '    <option value="2" >Packed</option>\n' +
                        '    <option value="3" >Dispatched</option>\n' +
                        '    <option value="4" >Delivered</option>\n' +
                        '    <option value="5" >Cancelled</option>\n' +
                        '    <option value="6" >Rejected</option>';

                var control = $('#val_select').get(0).selectize;
		  	    control.destroy();
			    $('#val_select').html(html);
                $('#val_select').selectize();

                $('#val_select').get(0).selectize.setValue(status);
                UIkit.modal('#modal_header_footer').show();
            });
        });


//Publish
function publish(id,type)
    {
        if(type == 0)
        {
            $msg="Are you sure want to publish Product";
        }
        else{
            $msg="Are you sure want to unpublish Product";
        }

        UIkit.modal.confirm($msg, 
            function(){                
                $.ajax({		            	
                    type: "POST",
                    url: `${window.pageData.baseUrl}/api/publish_product`,
                    data: {
                            "_token": "{{ csrf_token() }}",
                            "id": id,
                            "type":type
                            } ,                                                         
                    success: function(data)
                    {
                        if(data.status == 'success')
                        {
                            UIkit.modal.alert('Record has been Updated!'); 
                            setInterval(function() {
                                location.reload();
                            }, 2000);
                        }
                        else
                        {
                            UIkit.modal.alert(data.message); 
                        }
                        
                    }
                });
            });
    }

     //Delete video
     function delete_video(id)
    {       
        UIkit.modal.confirm('Are you sure to delete Product?', 
        function(){                
            $.ajax({		            	
                type: "POST",
                url: `${window.pageData.baseUrl}/api/delete_product`,
                data: {
                        "_token": "{{ csrf_token() }}",
                        "id": id
                        } ,                                                         
                success: function(data)
                {
                    if(data.status == 'success')
                    {
                        $('#row' + id).remove();
                        UIkit.modal.alert('Record has been successfully deleted !'); 
                    }
                    else
                    {
                        UIkit.modal.alert(data.message); 
                    }
                    
                }
            });
        });
    }

    </script>
@endsection
