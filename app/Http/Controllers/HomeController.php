<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\States;
use Auth;
use App\Models\Order;
use App\Models\Product;
use App\Models\UserAddress;
use App\Models\User;
use View;
use Mail;
use DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if (Auth::User()->role=='admin') {
            return view('adminHome');
        } else {
            return redirect()->route('index');
        }
    }

    public function adminHome()
    {

        $title='Dashboard';
        $today=date('Y-m-d');

        $counts = DB::select("SELECT (SELECT COUNT(*) FROM users) as users, (SELECT COUNT(*) FROM orders where status='success') as total_order , (SELECT COUNT(*) FROM orders where status='success' and date='$today' ) as today_order");
        $total_amount=DB::select("SELECT SUM(amount) as amount,SUM(dl_charge) as dl_charge FROM orders where status='success' ");
        $today_amount=DB::select("SELECT SUM(amount) as amount,SUM(dl_charge) as dl_charge FROM orders where status='success' and date='$today' ");

                
        $data=Order::where('status','success')->where('date',$today)->orderBy('id','DESC')->limit(10)->get();
        foreach($data as $d){
            $user=User::where('id',$d->user_id)->select('name','phone')->first();
            $d->name=$user->name;
            $d->phone=$user->phone;
        }

        return View::make('adminHome',compact('data','title','counts','total_amount','today_amount'));
    }

    public function view_states()
    {
        $title='states';
        $data=States::where('status',0)->get();
        return view('admin.view_states',compact('data','title'));
    }

    public function update_state(Request $r)
    {
        if ($r->id) {
            $update=States::find($r->id);
        }
        else {
            $update=new States();
        }
        $update->name=$r->name;
        $update->dl_charge=$r->dl_charge;
        $update->save();
        if($update){
            return response()->json(['message'=>'States Updated Successfully','status'=>'success']);
       }
       else{
            return response()->json(['message'=>'Something Went Wrong!','status'=>'fail']);
       }
    }


    public function delete_state(Request $r)
    {
        $update=States::find($r->id);
        if ($update->status==0) {
            $update->status=1;
        }
        else {
            $update->status=0;
        }
        $update->save();
        if($update){
            return response()->json(['message'=>'State deleted Successfully','status'=>'success']);
       }
       else{
            return response()->json(['message'=>'Something Went Wrong!','status'=>'fail']);
       }
    }

}
