<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\brand;
use Auth;


class BrandController extends Controller
{
    // Brand
    public function brand(Request $r)
    {
        if($r->id != '' )
            {
                $insert=brand::find($r->id);
                $opration="Update";
            }
        else 
            {
                $insert=new brand;
                $opration="add";
            }  
  
        $files = $r->file('image');        
        if($files!='')
            {
                $imageName = date("YmdHis").'.'.$r->image->getClientOriginalExtension();
                $success = $r->image->move(public_path('image/brand'), $imageName);                
                $insert->image='brand/'.$imageName;                            
            }
  
        $insert->name=$r->name;
        $insert->slug_name=$r->slug;
        $save=$insert->save();
        if($save)
            {  
                return response()->json(['message'=>'Successfully '.$opration.' Brand image.','status'=>'success']);
            }
        else 
            {
                return response()->json(['message'=>'Server not response,please try again later!','status'=>'fail']);
            }
              
         
    }
  
    //Delete Brand
    public function delete_brand(Request $r)
    {
        $check=brand::find($r->id);
        if($check)
            {
                $save=$check->delete();
                if($save)
                {
                    return response()->json(['message'=>'Record has been deleted.','status'=>'success']);
                }
                else 
                {
                    return response()->json(['message'=>'Server not response,please try again later!','status'=>'fail']);
                }              
            }
        else 
            {
              return response()->json(['message'=>'Record not found!','status'=>'fail']);
            }
    }

    //Publish Brand
    public function publish_brand(Request $r)
    {
        $check=brand::find($r->id);
        if($check)
        {
            $check->publish=$r->type;
            $save=$check->save();
            if($save)
            {
                return response()->json(['message'=>'Record has been deleted.','status'=>'success']);
            }
            else 
            {
                return response()->json(['message'=>'Server not response,please try again later!','status'=>'fail']);
            }            
        }
        else 
        {
            return response()->json(['message'=>'Record not found!','status'=>'fail']);
        }
    }
}
