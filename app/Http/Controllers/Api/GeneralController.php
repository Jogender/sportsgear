<?php
namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\banner;
use App\Models\Category;
use App\Models\SubCategory;
use App\Models\Subsubcategory;
use App\Models\User;
use App\Models\Product;
use App\Models\Cart;
use App\Models\wishlist;
use App\Models\Order;

use Auth;
use Hash;
use Mail;
use DB;

class GeneralController extends Controller
{
    //Change password
    public function change_password(Request $r)
    {
        $user=User::where('id',Auth::user()->id)->first();
        if ($user) {
            # code...
            if(Hash::check($r->old_password,$user->password)){
                # code...
                $user->password=Hash::make($r->new_password);
                $save=$user->save();
                return response()->json(['message'=>'Password changed successfully','status'=>200]);
            }
            else {
                # code...
                return response()->json(['message'=>'Incorrect old password','status'=>400]);
            }
        }
        else {
            # code...
            return response()->json(['message'=>'User not found','status'=>404]);
        }
    }


    //update password
    public function update_password(Request $r){
       $password=Hash::make($r->pass);
       $user=User::where('id',$r->id)->first();
       $user->password=$password;
       $save=$user->save();
       if($save){
            return response()->json(['message'=>'Password Update Successfully','status'=>200]);
       }
       else{
            return response()->json(['message'=>'Something Went Wrong!','status'=>500]);
       }
    }

    //Check Email
    public function check_email(Request $r){
        $data=User::where('email',$r->email)->where('verify_status',1)->first();
        if($data!=''){
            $url=url('/');
            $link=$url.'/password-reset/'.encrypt($data->id);
            
           
            Mail::send('front.password_reset_mail',['data'=>$r->email,'link'=>$link],function($message) use ($data,$link){
                $message->to($data['email'])->subject('Password Reset Link');
                $message->from('sportsgear@gmail.com');
            });
            return response()->json(['message'=>'Password reset link send on your email address','status'=>200]);
        }
        else{
            return response()->json(['message'=>'Email does not exist!','status'=>500]);
        }
    }

  
    //Register User
    public function register(Request $r){
        $check=User::where('email',$r->email)->first();
        if($check && $check->verify_status==1){
            return response()->json(['message'=>'Email Already Exist!','status'=>400]);
        }
        elseif($check && $check->verify_status==0){
            $insert=$check;
        }
        else{
            $insert=new User();
        }
        $otp=mt_rand(1000,9999);
        $insert->name=$r->username;
        $insert->email=$r->email;
        $insert->otp=$otp;
        $insert->password=Hash::make($r->password);
        $save=$insert->save();
        $save=1;
        if($save){
            $data=array('email'=>$r->email);
            Mail::send('admin.otp_mail',['data'=>$data,'otp'=>$otp],function($message) use ($data,$otp){
                $message->to($data['email'])->subject('One Time Password');
                $message->from('sportsgear@gmail.com');
            });
            return response()->json(['message'=>'Success','status'=>200,'email'=>$r->email,'data'=>$insert]);
        }
        else{
            return response()->json(['message'=>'Error! Please try again.','status'=>500]);
        }

    }

    //Login
    public function login(Request $r)
    {
        $check=User::where('email',$r->username)->first();
        if($check!='')
        {
            if($check->verify_status!=1){
                return response()->json(['message'=>'Account not found!','status'=>400]);
            }   
            $user_password=$r->password;
            if(Hash::check($user_password,$check->password)){
                return response()->json(['message'=>'login successfull','status'=>200,'data'=>$check]);
            }
            else{
                return response()->json(['message'=>'You have enter wrong password','status'=>500]);
            }
       }
       else{
         return response()->json(['message'=>'Email not registered','status'=>404]);
       }
    }

    ##Verify_otp
    public function verify_otp(Request $r){
        $check=User::where('email',$r->email)->first();
        if($check->otp==$r->email_otp){
            $check->verify_status=1;
            $save=$check->save();
            return response()->json(['message'=>'Success','status'=>200,'data'=>$check]);
        }
        else{
            return response()->json(['message'=>'Invalid Otp','status'=>400]);
        }
    }

    ##Banner Image
    public function banner(Request $r)
    {
         
            if($r->id != '' )
            {
                $insert=banner::find($r->id);
                $opration="Update";
            }
            else 
            {
                $insert=new banner;
                $opration="add";
            }  

            $files = $r->file('image');
            if($files!='')
            {
                $imageName = date("YmdHis").'.'.$r->image->getClientOriginalExtension();
                $success = $r->image->move(public_path('image/banner'), $imageName);   
                $insert->image='banner/'.$imageName;                            
            }

            $insert->image_link=$r->link;

            if($r->link == 'yes'){
                $insert->link_type=$r->link_type;
                $string=$r[$r->link_type];
                $arr=explode("_",$string);

                $insert->link_id= $arr[0];
                $insert->slug_name= $arr[1];
            }else{
                $insert->link_type=null;
                $insert->link_id=null;
            }

            $insert->banner_enable=$r->banner_enable;
            $save=$insert->save();
            if($save)
            {  
                return response()->json(['message'=>'Successfully '.$opration.' Banner image.','status'=>'success']);
            }
            else 
            {
                return response()->json(['message'=>'Server not response,please try again later!','status'=>'fail']);
            }
            
        
    }

    //Delete Banner
    public function delete_banner(Request $r)
    {
        $check=banner::find($r->id);
        if($check)
        {
            $save=$check->delete();
            if($save)
            {
                return response()->json(['message'=>'Record has been deleted.','status'=>'success']);
            }
            else 
            {
                return response()->json(['message'=>'Server not response,please try again later!','status'=>'fail']);
            }
            
        }
        else 
        {
            return response()->json(['message'=>'Record not found!','status'=>'fail']);
        }
    }

    //Publish Banner
    public function publish_banner(Request $r)
    {
        $check=banner::find($r->id);
        if($check)
        {
            $check->publish=$r->type;
            $save=$check->save();
            if($save)
            {
                return response()->json(['message'=>'Record has been deleted.','status'=>'success']);
            }
            else 
            {
                return response()->json(['message'=>'Server not response,please try again later!','status'=>'fail']);
            }
            
        }
        else 
        {
            return response()->json(['message'=>'Record not found!','status'=>'fail']);
        }
    }

      ##Category Image
    public function category(Request $r)
    {
        if($r->id != '' )
            {
                $insert=Category::find($r->id);
                $opration="Update";
            }
        else 
            {
                $insert=new Category;
                $opration="add";
            }  

        $files = $r->file('image');        
        if($files!='')
            {
                $imageName = date("YmdHis").'.'.$r->image->getClientOriginalExtension();
                $success = $r->image->move(public_path('image/category'), $imageName);
                
                $insert->image='category/'.$imageName;                            
            }

        $insert->name=$r->name;
        $insert->slug_name=$r->slug;
        $save=$insert->save();
            if($save)
            {  
                return response()->json(['message'=>'Successfully '.$opration.' Category image.','status'=>'success']);
            }
            else 
            {
                return response()->json(['message'=>'Server not response,please try again later!','status'=>'fail']);
            }
            
       
    }

    //Delete Category
    public function delete_category(Request $r)
    {
        $check=Category::find($r->id);
        if($check)
        {
            $save=$check->delete();
            if($save)
            {
                $sub_delete=SubCategory::where('category',$r->id)->delete();
                $sub_sub_delete=Subsubcategory::where('category',$r->id)->delete();
                return response()->json(['message'=>'Record has been deleted.','status'=>'success']);
            }
            else 
            {
                return response()->json(['message'=>'Server not response,please try again later!','status'=>'fail']);
            }
            
        }
        else 
        {
            return response()->json(['message'=>'Record not found!','status'=>'fail']);
        }
    }

    //Delete Category
    public function delete_subcategory(Request $r)
    {
        $check=SubCategory::find($r->id);
        if($check)
        {
            $save=$check->delete();
            if($save)
            {
                $delete=Subsubcategory::where('subcategory',$r->id)->delete();
                return response()->json(['message'=>'Record has been deleted.','status'=>'success']);
            }
            else 
            {
                return response()->json(['message'=>'Server not response,please try again later!','status'=>'fail']);
            }
            
        }
        else 
        {
            return response()->json(['message'=>'Record not found!','status'=>'fail']);
        }
    }

    public function publish_subcategory(Request $r)
    {
        $check=SubCategory::find($r->id);
        if($check)
        {
            $check->publish=$r->type;
            $save=$check->save();
            if($save)
            {
                return response()->json(['message'=>'Record has been '.$r->type,'status'=>'success']);
            }
            else 
            {
                return response()->json(['message'=>'Server not response,please try again later!','status'=>'fail']);
            }
            
        }
        else 
        {
            return response()->json(['message'=>'Record not found!','status'=>'fail']);
        }
    }

    //Publish Category
    public function publish_category(Request $r)
    {
        $check=Category::find($r->id);
        if($check)
        {
            $check->publish=$r->type;
            $save=$check->save();
            if($save)
            {
                return response()->json(['message'=>'Record has been deleted.','status'=>'success']);
            }
            else 
            {
                return response()->json(['message'=>'Server not response,please try again later!','status'=>'fail']);
            }
            
        }
        else 
        {
            return response()->json(['message'=>'Record not found!','status'=>'fail']);
        }
    }

    public function subcategory(Request $r)
    {
         
        if($r->id != '' )
        {
            $insert=SubCategory::find($r->id);
            $opration="Update";
        }
        else 
        {
            $insert=new SubCategory;
            $opration="add";
        }  

        $files = $r->file('image');            
        if($files!='')
        {
            $imageName = date("YmdHis").'.'.$r->image->getClientOriginalExtension();
            $success = $r->image->move(public_path('image/category'), $imageName);            
            $insert->image='category/'.$imageName;                            
        }

        $insert->name=$r->name;
        $insert->slug_name=$r->slug;
        $insert->category=$r->category;
        $save=$insert->save();
        if($save)
        {  
            return response()->json(['message'=>'Successfully '.$opration.' SubCategory image.','status'=>'success']);
        }
        else 
        {
            return response()->json(['message'=>'Server not response,please try again later!','status'=>'fail']);
        }
            
      
    }



    //get subcategory according category
    public function get_subcategory(Request $r){
        if(!empty($r->cat_id)){
            $data=SubCategory::where('category',$r->cat_id)->where('publish',0)->get();
            return response()->json(['status'=>'success','data'=>$data]);

        }
        else{
            return response()->json(['status'=>'fail','message'=>'Param missing']);
        }

    }

    public function get_subsubcategory(Request $r){
        if(!empty($r->cat_id) && !empty($r->subcat_id)){
            $data=Subsubcategory::where('category',$r->cat_id)->where('subcategory',$r->subcat_id)->where('publish',0)->get();
            return response()->json(['status'=>'success','data'=>$data]);
        }
        else{
            return response()->json(['status'=>'fail','message'=>'Param missing']);
        }

    }

    public function subsubcategory(Request $r)
    {
       
        if($r->id != '' )
        {
            $insert=Subsubcategory::find($r->id);
            $opration="Update";
        }
        else 
        {
            $insert=new Subsubcategory;
            $opration="add";
        }  

        $files = $r->file('image');        
        if($files!='')
        {
            $imageName = date("YmdHis").'.'.$r->image->getClientOriginalExtension();
            $success = $r->image->move(public_path('image/category'), $imageName);            
            $insert->image='category/'.$imageName;                            
        }

            $insert->name=$r->name;
            $insert->slug_name=$r->slug;
            $insert->category=$r->category;
            $insert->subcategory=$r->subcategory;
            $save=$insert->save();
        if($save)
            {  
                return response()->json(['message'=>'Successfully '.$opration.' SubSubCategory image.','status'=>'success']);
            }
        else 
            {
                return response()->json(['message'=>'Server not response,please try again later!','status'=>'fail']);
            }
            
       
    }

    public function delete_subsubcategory(Request $r)
    {
        $delete=Subsubcategory::where('id',$r->id)->delete();
        return response()->json(['message'=>'Record has been deleted.','status'=>'success']);
    }

    public function publish_subsubcategory(Request $r)
    {
        $check=Subsubcategory::find($r->id);
        if($check)
        {
            $check->publish=$r->type;
            $save=$check->save();
            if($save)
            {
                return response()->json(['message'=>'Record has been '.$r->type,'status'=>'success']);
            }
            else 
            {
                return response()->json(['message'=>'Server not response,please try again later!','status'=>'fail']);
            }
            
        }
        else 
        {
            return response()->json(['message'=>'Record not found!','status'=>'fail']);
        }
    }


    public function remove_product_file(Request $r)
    {

        $check=Product::find($r->id);
        if($check)
        {
            $file=json_decode($check->image,true);
            if (($key = array_search($r->image, $file)) !== false) {
                unset($file[$key]);
            }

            //Re-arrnge array
            $image=array_values($file);            
            $check->image=json_encode($image);
            $save=$check->save();
            if($save)
            {
                return response()->json(['message'=>'File has been Deleted successfully!','status'=>'success']);
            }
            else
            {
                return response()->json(['message'=>'something went wrong,Please try again later.','status'=>'fail']);
            }
        }
        else
        {
            return response()->json(['message'=>'Sorry,Product not found','status'=>'fail']);
        }
    }

    //Publish Product
    public function publish_product(Request $r)
    {
        $check=Product::find($r->id);
        if($check)
        {
            $check->status=$r->type;
            $save=$check->save();
            if($save)
            {
                return response()->json(['message'=>'Record has been deleted.','status'=>'success']);
            }
            else 
            {
                return response()->json(['message'=>'Server not response,please try again later!','status'=>'fail']);
            }
            
        }
        else 
        {
            return response()->json(['message'=>'Record not found!','status'=>'fail']);
        }
    }

    //Delete Category
    public function delete_product(Request $r)
    {
        $check=Product::find($r->id);
        if($check)
        {
            $save=$check->delete();
            if($save)
            {
                $cartdelete=Cart::where('productid',$r->id)->delete();
                $wishlistdelete=wishlist::where('product_id',$r->id)->delete();
                $Orderdelete=order::where('product_id',$r->id)->delete();
                return response()->json(['message'=>'Record has been deleted.','status'=>'success']);
            }
            else 
            {
                return response()->json(['message'=>'Server not response,please try again later!','status'=>'fail']);
            }
            
        }
        else 
        {
            return response()->json(['message'=>'Record not found!','status'=>'fail']);
        }
    }

    //Add to cart
    public function add_to_cart(Request $r){
        $insert=new Cart;
        // $insert->userid=Auth::user()->id;
        $insert->userid=38;
        $insert->productid=$r->product;
        $insert->quantity=$r->quantity;
        $insert->price=$r->price;
        $insert->variant_type=$r->type;
        $save=$insert->save();
        if($save){
            return response()->json(['message'=>'success','status'=>200]);
        }
        else{
            return response()->json(['message'=>'Something Went Wrong , Please try again','status'=>500]);
        }
    }

    
    public function remove_product(Request $r){
        $delete=Cart::where('id',$r->id)->where('userid',$r->userid)->delete();
        if($delete){
            return response()->json(['message'=>'success','status'=>200]);
        }
        else{
            return response()->json(['message'=>'Something Went Wrong , Please try again','status'=>500]);
        }
    }
    
     

}
