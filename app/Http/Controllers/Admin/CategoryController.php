<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\brand;
use App\Models\SubCategory;
use View;
use DB;

class CategoryController extends Controller
{
   //Category
      public function category(Request $r){

        $title='category';
        $data=Category::orderBy('id','DESC')->get();
        return View::make('admin.add_category',compact('data','title'));
      }

      public function subcategory(Request $r){

        $title='subcategory';
        $category=Category::orderBy('id','DESC')->get();
        $data = DB::table('sub_categories')
            ->join('categories', 'categories.id', '=', 'sub_categories.category')
            ->select('categories.name as catname', 'sub_categories.*')
            ->where('sub_categories.publish',0)
            ->get();

        return View::make('admin.add_subcategory',compact('data','title','category'));
      }

      public function subsubcategory(Request $r){

         $title='Sub-subcategory';
         $data= DB::table('subsubcategories')
         ->join('categories', 'categories.id', '=', 'subsubcategories.category')
         ->join('sub_categories', 'sub_categories.id', '=', 'subsubcategories.subcategory')
         ->select('categories.name as catname', 'sub_categories.name as subcategoryname','subsubcategories.*')             
         ->get();
         return View::make('admin.add_subsubcategory',compact('data','title'));
      }  


      //Brand
      public function brand(Request $r){
         $title='Brand';
         $data=brand::orderBy('id','DESC')->get();
         return View::make('admin.add_brand',compact('data','title'));
      }

      //Brand
      public function bulk_image(Request $r){
         $title='Bulk Image';
         return View::make('admin.bulk_image',compact('title'));
      }

      public function image_upload(Request $r){

         try {
            $file = $r->file('filenames');           
            if($file!='')
            {
               foreach($file as $f)
               {                         
                  $nme=str_replace(" ","",$f->getClientOriginalName());    
                  $imageName = date("YmdHis").'_'.$nme;
                  $success = $f->move(public_path('image/product'), $imageName); 
               }
               return redirect()->back()->with('success','Image has been uploaded successfully.');
 
            }else{
               return redirect()->back()->with('error','Something went wrong,Please try again later.');
            }
         } catch (\Exception $e) {
            return redirect()->back()->with('error',$e->getMessage());                     

         }
      }

  
}
