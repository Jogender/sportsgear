<?php
namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use View;
use App\Models\banner;
use App\Models\brand;
use App\Models\Product;


class BannerController extends Controller
{
   public function banner(Request $r){
      $title='banner';
      $data=banner::orderBy('id','DESC')->get();
      $brand=brand::select('id','name','slug_name')->where('publish',0)->orderBy('name','ASC')->get();
      $product=Product::select('id','name','slug_name')->orderBy('name','ASC')->get();

      return View::make('admin.add_banner',compact('data','title','brand','product'));
   }

}
