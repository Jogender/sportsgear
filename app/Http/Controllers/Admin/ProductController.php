<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\Product; 
use App\Models\SubCategory;
use App\Models\Subsubcategory;
use App\Models\brand;
use View;
use DB;



class ProductController extends Controller
{
    //product blade
    public function product(){
        $title='product';
        $category=Category::where('publish',0)->select('id','name')->get();
        $brand=brand::where('publish',0)->get();
        return View::make('admin.add_product',compact('title','category','brand'));
     }


    //add product
    public function add_product(Request $r){

        try {

            $title='add_product';
            $varient=[];
            //Check keyCount
            $key=$r->keyIds;
            $firstPrice=0;
            $qty=0;

            for ($i=1; $i <=  $key ; $i++) {

                $keyValueMake='key'.(string)$i;            
                $keyValue=$r[$keyValueMake];
                $keyCount='keyCount'.$i;
                
                $countValue=1;
                $looplimit=$r[$keyCount];            

                //add key in array
                $keyArr=[];
                $varients=[] ;

                //getvalue count according to key
                for ($v=1; $v <= $r[$keyCount]; $v++) { 

                    $valueMake= $keyValueMake."value".$v;
                    $priceMake=$keyValueMake."price".$v;
                    $quantityMake=$keyValueMake."quantity".$v;
                    $pricechangeMake=$keyValueMake."pricechange".$v;
                    

                    $value=$r[$valueMake];
                    $price=$r[$priceMake];
                    $quantity=$r[$quantityMake];
                    $pricechange=$r[$pricechangeMake];

                    $pricechangeValue='off';
                    if(isset($pricechange)){
                        if( $pricechange[0] != '')
                        {
                            $pricechangeValue='on';
                        }
                    }

                    if($v == 1){
                        $firstPrice=$price[0];
                        $qty=$quantity[0];
                    }
                    
                    $keyArr['value']= $value[0];
                    $keyArr['price']=$price[0];
                    $keyArr['quantity']=$quantity[0];
                    $keyArr['pricechange']=$pricechangeValue;
                    //add value
                    $varients[]=$keyArr;
                    
                }
                //insert key+value
                $varient[][$keyValue[0]]=$varients;
                
            }

            $feature=0;
            if($r->feature != ''){
                $feature=$r->feature;
            }

            $weekly=0;
            if($r->weekly != ''){
                $weekly=$r->weekly;
            }


            $insert=new Product;
            $insert->name=$r->name; 
            $insert->slug_name=$r->slug; 
            $insert->tax=$r->tax; 
            $insert->category=$r->category_id;
            $insert->subcategory=$r->subcategory_id;
            $insert->subsubcategory=$r->subsubcategory_id;
            $insert->short_description=$r->short_description;
            $insert->featured=$feature;
            $insert->topweekly=$weekly;

            if($r->variant_status == 'yes'){
                $insert->old_price=$firstPrice;
                $insert->new_price=$firstPrice;
                $insert->quantity=$qty;
            }else{
                $insert->old_price=$r->old_price;
                $insert->new_price=$r->new_price;
                $insert->quantity=$r->quantity;
            }

            $insert->description=$r->description;
            $insert->brand=$r->brand;
            $insert->origin=$r->origin;
            $insert->variant_status=$r->variant_status;
            $insert->variant=json_encode($varient);

            $file = $r->file('files');
            $image=array();        
            if($file!='')
            {
                foreach($file as $f)
                {                         
                    $nme=str_replace(" ","",$f->getClientOriginalName());    
                    $imageName = date("YmdHis").'_'.$nme;
                    $success = $f->move(public_path('image/product'), $imageName);
                    $image[]=$imageName;  
                }

                $insert->image=json_encode($image);                            
            }
          

            $save=$insert->save();                
            if($save){
                return redirect()->route('view_product')->with('success','Product has been added successfully.');
            }
            else{
                return redirect()->route('view_product')->with('error','Something went wrong,Please try again later.');
            }
          
        } catch (\Exception $e) {    
            return redirect()->route('view_product')->with('error',$e->getMessage());                     
        }
    }


     //view product
     public function view_product(){
       $title='product';
       $data=DB::table('products')->join('categories','products.category','=','categories.id')->select('products.*','categories.name as category_name')
       ->orderBy('products.id','DESC')->get();
       return View::make('admin.view_product',compact('title','data'));
    }

    //view product DEtail
    public function product_detail(Request $r){
        $title='Product Detail';
        $id=decrypt($r->id);

        $data=DB::table('products')
        ->join('categories', 'products.category', '=', 'categories.id')        
        ->select('products.*', 'categories.name as category_name')
        ->where('products.id',$id)
        ->first(); 

        //Subcategory
        $data->subcategory_name='';
        if($data->subcategory != ''){
            $subCat=SubCategory::select('name')->where('id',$data->subcategory)->first();
            if($subCat){
                $data->subcategory_name=$subCat->name;
            }
        }

        //SubSubCategory
        $data->subsubcategory_name='';
        if($data->subsubcategory != ''){
            $subsubCat=Subsubcategory::select('name')->where('id',$data->subsubcategory)->first();
            if($subsubCat){
                $data->subsubcategory_name=$subsubCat->name;
            }
        }

        return View::make('admin.product_detail',compact('title','data'));
    }

    public function edit_product(Request $r){
        $title='Edit Product Detail';
        $id=decrypt($r->id);        
        $data=product::find($id);
        $category=Category::where('publish',0)->select('id','name')->get();
        $subcategory=SubCategory::where('category',$data->category)->get();
        $subsubcategory=Subsubcategory::where('category',$data->category)->where('subcategory',$data->subcategory)->get();
        $brand=brand::where('publish',0)->get();
        
        return View::make('admin.edit_product',compact('title','data','category','subcategory','subsubcategory','brand'));
    
    }


    public function update_product(Request $r){

        try {

            $title='add_product';
            $varient=[];
            //Check keyCount
            $key=$r->keyIds;
            $firstPrice=0;
            $qty=0;
            for ($i=1; $i <=  $key ; $i++) {

                $keyValueMake='key'.(string)$i;            
                $keyValue=$r[$keyValueMake];
                $keyCount='keyCount'.$i;
                
                $countValue=1;
                $looplimit=$r[$keyCount];            

                //add key in array
                $keyArr=[];
                $varients=[] ;

                //getvalue count according to key
                for ($v=1; $v <= $r[$keyCount]; $v++) { 

                    $valueMake= $keyValueMake."value".$v;
                    $priceMake=$keyValueMake."price".$v;
                    $quantityMake=$keyValueMake."quantity".$v;
                    $pricechangeMake=$keyValueMake."pricechange".$v;

                    $value=$r[$valueMake];
                    $price=$r[$priceMake];
                    $quantity=$r[$quantityMake];
                    $pricechange=$r[$pricechangeMake];

                    $pricechangeValue='off';
                    if(isset($pricechange)){
                        if( $pricechange[0] != '')
                        {
                            $pricechangeValue='on';
                        }
                    }

                    if($i == 1 && $v == 1){
                        $firstPrice=$price[0];
                        $qty=$quantity[0];
                    }
                    
                    $keyArr['value']= $value[0];
                    $keyArr['price']=$price[0];
                    $keyArr['quantity']=$quantity[0];
                    $keyArr['pricechange']=$pricechangeValue;

                    //add value
                    $varients[]=$keyArr;
                    
                }
                //insert key+value
                $varient[][$keyValue[0]]=$varients;
                
            }

            $feature=0;
            if($r->feature != ''){
                $feature=$r->feature;
            }

            $weekly=0;
            if($r->weekly != ''){
                $weekly=$r->weekly;
            }

          
            $insert=Product::find($r->id);
            $insert->name=$r->name; 
            $insert->slug_name=$r->slug; 
            $insert->tax=$r->tax; 
            $insert->category=$r->category_id;
            $insert->subcategory=$r->subcategory_id;
            $insert->subsubcategory=$r->subsubcategory_id;
            $insert->short_description=$r->short_description;
            $insert->featured=$feature;
            $insert->topweekly=$weekly;
            
            if($r->variant_status == 'yes'){
                $insert->old_price=$firstPrice;
                $insert->new_price=$firstPrice;
                $insert->quantity=$qty;
            }else{
                $insert->old_price=$r->old_price;
                $insert->new_price=$r->new_price;
                $insert->quantity=$r->quantity;
            }

            $insert->brand=$r->brand;
            $insert->origin=$r->origin;
            $insert->variant_status=$r->variant_status;
            $insert->variant=json_encode($varient);

            $file = $r->file('files');
            $image=array();        
            if($file!='')
            {
                foreach($file as $f)
                {                         
                    $nme=str_replace(" ","",$f->getClientOriginalName());    
                    $imageName = date("YmdHis").'_'.$nme;
                    $success = $f->move(public_path('image/product'), $imageName);
                    $image[]=$imageName;  
                }

                $db_image=json_decode($insert->image);
                if($db_image != '')
                {
                    $image=array_merge($image,$db_image);
                    $image=array_unique($image);
                }

                if($this->check_count($image) <= 0){
                    return redirect()->back()->with('error','Sorry, Image can not be blank.');  
                }
                
                $insert->image=json_encode($image);                            
            }

            if($this->check_count(json_decode($insert->image)) <= 0){
                return redirect()->back()->with('error','Sorry, Image can not be blank.');    
            }

            $save=$insert->save();
            
            if($save){
                return redirect()->route('view_product')->with('success','Product has been updated successfully.');
            }
            else{
                return redirect()->route('view_product')->with('error','Something went wrong,Please try again later.');
            }
          
        } catch (\Exception $e) {
            return redirect()->route('view_product')->with('error',$e->getMessage());         
        }
    
    }
    

    

}
