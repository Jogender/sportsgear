<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Order;
use App\Models\States;
use View;
use App\Models\UserAddress;

class UserController extends Controller
{
    //
    public function users(){
        $data=User::where('role','member')->get();
        return view::make('admin.users',compact('data'));
    }

    public function user_detail($id){
        $data=User::where('id',decrypt($id))->first();
        $useraddress=Useraddress::where('user_id',decrypt($id))->get();
        foreach($useraddress as $u){
        $state=States::where('id',$u->state)->select('name')->first();
        $u->state=$state->name;
        }
        $order=Order::where('user_id',decrypt($id))->get();
        return view::make('admin.user_detail',compact('data','useraddress','order'));
    }
}
