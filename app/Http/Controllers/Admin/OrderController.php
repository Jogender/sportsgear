<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Order;
use App\Models\Product;
use App\Models\UserAddress;
use App\Models\User;
use View;
use Mail;

class OrderController extends Controller
{
    public function view_invoice($id){
        $order=Order::find($id);
        $products=Product::whereIn('id',json_decode($order->product_id))->get();
        return View::make('admin.order_invoice',compact('order','products'));
    }

    public function change_order_status(Request $r){
        $update=Order::find($r->id);
        $update->tracking_status=$r->value;
        $update->tracking_id=$r->track;
        $update->save();
        if ($update) {

            $user=User::where('id',$update->user_id)->select('name','email')->first();
            $product=Product::whereIn('id',json_decode($update->product_id))->get();


            $data=array('email'=>$user->email);
            $status=$r->value;

            Mail::send('front.order_status_mail',['data'=>$user,'status'=>$status,'order_detail'=>$update,'products'=>$product],function($message) use ($data,$status){
                $message->to($data['email'])->subject('Order has been '.$status.'.');
                $message->from('sportsgear@gmail.com');
            });

            return response()->json(['status'=>'true','message'=>'Order status updated successfully!!']);
        }else {
            return response()->json(['status'=>'false','message'=>'Try again, Somthing went wrong!!']);
        }
    }

    //Get orders
    public function order(){
        $title='order';
        $data=Order::where('status','success')->orderBy('id','DESC')->get();
        foreach($data as $d){
            $user=User::where('id',$d->user_id)->select('name','phone')->first();
            $d->name=$user->name;
            $d->phone=$user->phone;
        }
        return View::make('admin.order',compact('data','title'));
    }

    //order detail
    public function order_details($id){
        $title='order';
        $data=Order::where('id',decrypt($id))->first();
        $user=User::where('id',$data->user_id)->select('name','email','phone')->first();
        $products=Product::whereIn('id',json_decode($data->product_id))->select('id','name','image','new_price','variant_status')->get();
        return View::make('admin.order_detail',compact('data','title','user','products'));
    }

    public function order_status(Request $req)
    {
        $data = Order::where('id',$req->id)->with('users')->with('addresses')->first();
    }
}
