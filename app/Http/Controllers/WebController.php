<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\banner;
use App\Models\brand;

use App\Models\Cart;
use App\Models\User;
use App\Models\UserAddress;
use App\Models\Product;
use App\Models\Review;
use App\Models\Order;
use App\Models\SubCategory;
use App\Models\Subsubcategory;

use App\Models\States;
use App\Models\wishlist;
use View;
use Auth;
use DB;
use Hash;
use Mail;
use Redirect;


class WebController extends Controller
{ 

    public function mailTemp(){
        return View::make('mail.shipmentemp');
    }

    public function test(){
        $order=DB::table('orders')
                    ->join('users','users.id','orders.user_id')
                    ->where('orders.id',52)
                    ->select('orders.*','users.name as username','users.email')
                    ->orderBy('orders.id','DESC')->first();
        $order->products=Product::whereIn('id',json_decode($order->product_id))->select('name','id','image')->get();
        return View::make('test',compact('order'));
    }   

     //Category Product
     public function brand_product($id,$type=null){
        $b_data=Brand::where('slug_name',$id)->first();
        $brand=Brand::select('name','id')->get();
        foreach($brand as $b){
            $count=Product::where('brand',$b->id)->count();
            $b->count=$count;
        }

        $products=Product::orderBy('id','DESC')->where('brand',$b_data->id)->paginate(10);

        foreach($products as $p){
            $rating_sum=Review::where('product_id',$p->id)->sum('rating');
            $review_count=Review::where('product_id',$p->id)->count();
            if($review_count!=0){
                $p->star_rating=round($rating_sum/$review_count);
            }
            else{
                $p->star_rating=0;
            }
            $p->review_count=$review_count;
        }
        // dd($products);
            return view::make('front.brand_product',compact('products','brand'));
    }

    public function payment_status(Request $r){

        $update=Order::find($r->productinfo);
        if ($r->status=='success') {

            if (intval($r->amount)!=$update->amount) {
                $update->status='Fraud';
            }
            else{
                $items=$update->cart_id;
                if ($items) {
                    $items=json_decode($items);
                    foreach ($items as $id) {
                        $cart=Cart::find($id);
                        $cart->delete();
                    }
                }

                // send mail on successfull transaction
                $order=DB::table('orders')
                    ->join('users','users.id','orders.user_id')
                    ->where('orders.id',$update->id)
                    ->select('orders.*','users.name as username','users.email')
                    ->orderBy('orders.id','DESC')->first();
                $order->products=Product::whereIn('id',json_decode($order->product_id))->select('name','id','image')->get();

                $data=array('email'=>$order->email);
                Mail::send('front.order_receipt',['data'=>$data,'order'=>$order],function($message) use ($data){
                    $message->to($data['email'])->subject('Order Placed Successfully');
                    $message->from('sportsgear@gmail.com');
                });
                // send mail on successfull transaction

                $update->status=$r->status;
            }
        }
        else{
            $update->status=$r->status;
        }
        $update->payment_type=$r->mode;
        $update->save();

        $order=$update;
        return View::make('front.payment_status',compact('order'));
    }

    public function place_order(Request $r){

        $items=json_decode($r->items);
        $details=array();
        $products_ids=array();
        $amount=0;
        foreach ($items as $id) {
            $cart=Cart::find($id);
            $my_cart['product_id']=$cart->productid;
            $my_cart['quantity']=$cart->quantity;

            $products_ids[]=$cart->productid;
            $product=Product::where('id',$cart->productid)->select('new_price')->first();
            $my_cart['price']=$product->new_price;
            $my_cart['details']=$cart->variant_type;
            $details[]=$my_cart;

            $amount=$amount+$product->new_price*$cart->quantity;
        }

        $order=new Order();
        $order->order_id='SPORTSGEAR'.date('dmYhis').'_'.$cart->userid;
        $order->product_id=json_encode($products_ids);

        $user_addresses=DB::table('user_addresses')
                    ->join('states','states.id','user_addresses.state')
                    ->where('user_addresses.id',$r->address_id)
                    ->select('user_addresses.firstname','user_addresses.lastname','user_addresses.mobile','user_addresses.address','user_addresses.city','user_addresses.pincode','states.name as state','states.dl_charge')
                    ->first();

        $order->dl_address=$user_addresses->firstname.' '.$user_addresses->lastname.','.$user_addresses->mobile.','.$user_addresses->address.','.$user_addresses->city.'('.$user_addresses->pincode.'),'.$user_addresses->state;
        $order->dl_charge=$user_addresses->dl_charge;

        if ($amount<2001) {
            $amount=$amount+$user_addresses->dl_charge;
        }

        $order->user_id=$cart->userid;
        $order->date=date('Y-m-d');
        $order->amount=$amount;
        if ($r->ajax()) {
            $order->cart_id=$r->items;
        }
        else{
            $order->payment_type='COD';
        }
        $order->product_detail=json_encode($details);
        $order->transaction_id=$r->txn_id;
        $order->status='success';
        $order->save();
        if ($order) {
            if (!$r->ajax()) {
                foreach ($items as $id) {
                    $cart=Cart::find($id);
                    $cart->delete();
                }

                // send mail on successfull transaction
                $ord=DB::table('orders')
                    ->join('users','users.id','orders.user_id')
                    ->where('orders.id',$order->id)
                    ->select('orders.*','users.name as username','users.email')
                    ->orderBy('orders.id','DESC')->first();
                $ord->products=Product::whereIn('id',json_decode($ord->product_id))->select('name','id','image')->get();

                $data=array('email'=>$ord->email);
                Mail::send('front.order_receipt',['data'=>$data,'order'=>$ord],function($message) use ($data){
                    $message->to($data['email'])->subject('Order Placed Successfully');
                    $message->from('sportsgear@gmail.com');
                });
                // send mail on successfull transaction
            }

            if ($r->ajax()) {
                return response()->json(['status'=>'success','order_id'=>$order->id,'amount'=>$amount]);
            }
            return redirect()->route('my_orders')->with('success','Order placed successfully');
        } else {
            if ($r->ajax()) {
                return response()->json(['status'=>'fail']);
            }
            return redirect()->route('my_cart')->with('error','Fail to place order. Try again!');
        }
    }

    //Checkout
    public function checkout(Request $r){
        $items=$r->id;
        $quantity=$r->quantity;
        $price=0;
        foreach ($items as $key => $id) {
            $product_price=0;
            $cart=Cart::where('id',$id)->select('quantity','variant_type','productid')->first();

            // update cart quantity
            $update=Cart::find($id);
            $update->quantity=$quantity[$key];
            $update->save();

            $cart_details=json_decode($cart->variant_type);

            $product=Product::where('id',$cart->productid)->select('new_price','variant')->first();
            $product_price=$product_price+$product->new_price;
            if ($product->variant_status=='yes') {
                $product_details=json_decode($product->variant);
                foreach ($product_details as $product_detail) {
                    foreach ($product_detail as $product_det) {
                        foreach ($product_det as $product_dt) {
                            foreach ($cart_details as $cart_detail) {
                                if(isset($product_dt->value) && $product_dt->value==$cart_detail && $product_dt->pricechange=='on')
                                {
                                    $product_price=$product_price+$product_dt->price;
                                }
                            }
                        }
                    }
                }
            }

            $product_price=$product_price*$quantity[$key];
            $price=$price+$product_price;
        }
        $address=UserAddress::where('user_id',Auth::User()->id)->orderBy('id','DESC')->limit(4)->get();
        $states=States::where('status',0)->get();
        foreach ($address as $add) {
            foreach ($states as $state) {
                if ($state->id==$add->state) {
                    $add->dl_charge=$state->dl_charge;
                }
            }
        }
        $items=json_encode($items);
        
        return view::make('front.checkout',compact('address','price','items','states'));
    }

    //Category Product
        public function category_product($id,$type=null){
            $brand=Brand::select('name','id')->get();
            foreach($brand as $b){
                $count=Product::where('brand',$b->id)->count();
                $b->count=$count;
            }
            $category=Category::where('slug_name',$id)->select('id')->first();
            $products=Product::orderBy('id','DESC')->where('category',$category->id)->paginate(12);

            foreach($products as $p){
                $rating_sum=Review::where('product_id',$p->id)->sum('rating');
                $review_count=Review::where('product_id',$p->id)->count();
                if($review_count!=0){
                    $p->star_rating=round($rating_sum/$review_count);
                }
                else{
                    $p->star_rating=0;
                }
                $p->review_count=$review_count;
            }
            $subcat_id='';
            $subcat_name='';
            return view::make('front.category_product',compact('products','brand','subcat_id','subcat_name'));
        }
    
    //Subcategory Product
    public function subcategory_product($id,$type=null){
        $brand=Brand::select('name','id')->get();
        foreach($brand as $b){
            $count=Product::where('brand',$b->id)->count();
            $b->count=$count;
        }

        $subcategory=SubCategory::where('slug_name',$id)->select('id','category','name')->first();
        $products=Product::orderBy('id','DESC')->where('subcategory',$subcategory->id)->paginate(12);
        foreach($products as $p){
            $rating_sum=Review::where('product_id',$p->id)->sum('rating');
            $review_count=Review::where('product_id',$p->id)->count();
            if($review_count!=0){
                $p->star_rating=round($rating_sum/$review_count);
            }
            else{
                $p->star_rating=0;
            }
            $p->review_count=$review_count;
        }

        $subcat_id=$subcategory->category;
        $subcat_name=$subcategory->name;
        if($type=='list'){
            return view::make('front.category_product',compact('products','brand','subcat_id','subcat_name'));
        }
        else{
            return view::make('front.category_product',compact('products','brand','subcat_id','subcat_name'));
        }
    }

    // subsubcategory_product
    public function subsubcategory_product($id,$type=null){
        $brand=Brand::select('name','id')->get();
        foreach($brand as $b){
            $count=Product::where('brand',$b->id)->count();
            $b->count=$count;
        }
        
        $Subsubcategory=Subsubcategory::where('slug_name',$id)->select('id','name')->first();
        $products=Product::orderBy('id','DESC')->where('Subsubcategory',$Subsubcategory->id)->paginate(12);
        foreach($products as $p){
            $rating_sum=Review::where('product_id',$p->id)->sum('rating');
            $review_count=Review::where('product_id',$p->id)->count();
            if($review_count!=0){
                $p->star_rating=round($rating_sum/$review_count);
            }
            else{
                $p->star_rating=0;
            }
            $p->review_count=$review_count;
        }
        $subcat_id=$Subsubcategory->category;
        $subcat_name=$Subsubcategory->name;
        if($type=='list'){
            return view::make('front.category_product',compact('products','brand','subcat_id','subcat_name'));
        }
        else{
            return view::make('front.category_product',compact('products','brand','subcat_id','subcat_name'));
        }
    }


    //Submit Review
    public function submit_review(Request $r){
        $insert=Review::where('product_id',$r->product_id)->where('user_id',Auth::user()->id)->first();
        if($r->rating!=''){
            $rating=$r->rating;
        }
        else{
            $rating=0;
        }
        if($insert==''){
            $insert = new Review;
           
        }
        $insert->user_id=Auth::user()->id;
        $insert->product_id=$r->product_id;
        $insert->rating=$rating;
        $insert->review=$r->review;
        $save=$insert->save();  

        if($save){
        return redirect()->route('product_listing');
        }
        else{
            return redirect()->back()->with('error','Something went wrong!');
        }
    }

    //Review
    public function review($id){
        $check=Order::whereRaw('json_contains(product_id, \'["'.decrypt($id).'"]\')')->where('user_id',Auth::user()->id)->count();
        if($check==0){
            return View::make('front.review_error');
        }
        else{
            $data=Product::where('id',decrypt($id))->first();
            return view::make('front.add_review',compact('data'));
        }
       
    }

    //Review Error
    public function review_error()
    {
        return View::make('front.review_error');
    }
    
    public function add_to_wishlist(Request $r)
    {
        $check=wishlist::where('product_id',$r->product_id)->where('user_id',$r->user_id)->first();
        if ($this->check_count($check)>0) {
            $delete=wishlist::find($check->id);
            $delete->delete();
            return response()->json(['status'=>'true']);
        }

        $insert=new wishlist;
        $insert->user_id=$r->user_id;
        $insert->product_id=$r->product_id;
        $insert->save();
        return response()->json(['status'=>'true']);
    }

    public function remove_to_wishlist(Request $r){

        $check=wishlist::find($r->id);

        if($this->check_count($check)>0 )
        {
            $check->delete();
            return response()->json(['status'=>'success','message'=>'Wishlist product has been removed.']);
        }else{
            return response()->json(['status'=>'fail','message'=>'Wishlist product not found']);
        }
    }

    public function wishlist(Request $r)
    {
        $products=DB::table('wishlists')->join('products','wishlists.product_id','=','products.id')
        ->where('wishlists.user_id',Auth::user()->id)
        ->select('products.*','wishlists.id as wishlistID')
        ->paginate(10);

        if($this->check_count($products)>0 )
        {
            foreach($products as $p){
                $rating_sum=Review::where('product_id',$p->id)->sum('rating');
                $review_count=Review::where('product_id',$p->id)->count();
                if($review_count!=0){
                    $p->star_rating=round($rating_sum/$review_count);
                }
                else{
                    $p->star_rating=0;
                }
                $p->review_count=$review_count;
            }
        }
        return view::make('front.wishlist',compact('products'));
        
    }

    //Add to cart
    public function add_to_cart(Request $r)
    {
        $check=Product::where('id',$r->product_id)->select('quantity')->first();
        if ($check->quantity<$r->quantity) {
            return response()->json(['status'=>'false','message'=>'Product so much quantity not available!!']);
        }

        $insert=new Cart;
        $insert->userid=$r->user_id;
        $insert->productid=$r->product_id;
        $insert->quantity=$r->quantity;
        $insert->variant_type=json_encode($r->varients);
        $insert->save();
        if ($insert) {
            return response()->json(['status'=>'true','message'=>'Product added successfully to cart!!']);
        } else {
            return response()->json(['status'=>'false','message'=>'Fail to add cart!!']);
        }
    }

    //Search Product
    public function product_search(Request $r){
        $url=URL('/');
        $data=Product::where('name','like', '%'.$r->keyword.'%')->select('id','name','image','slug_name')->get();
        $category=Category::where('id',$r->category)->first();
        $size=sizeof($data);
        $output = '<ul class="dropdown-menu" id="searchul" style="display:block; position:relative;width:95%;margin-left:10px;">';
        if($size > 0){
            foreach($data as $d){
                $images=json_decode($d->image);
                $proimage=$url."/image/product/".$images[0];
                $product=$url."/product/".$d->slug_name;
                $output .= '
                <p style="text-align:left;padding-left:10px;"><img src="'.$proimage.'" height="40" width="40" style="border:1px solid #333;border-radius:10px;"></i>&nbsp;&nbsp;<a href="'.$product.'">'.$d->name.'</a></p><hr>
                ';
            }
    
                $output .= '</ul>';
                return response()->json(['message'=>'success','status'=>200,'ul'=>$output]);
        }
        else{
            $output .= '
            <p style="text-align:left;padding-left:10px;margin-top:10px;color:#f74b16;"></i><b>No product found.</b></p><hr>
            ';
            $output .= '</ul>';
            return response()->json(['message'=>'error','status'=>500,'ul'=>$output]);
        }
    }
        
    public function products_ajax(Request $r){
        $products=Product::orderBy('id','DESC')->paginate(10);
        $type=$r->type;
        $view = view('front.products_ajax',compact('products','type'))->render();
        return response()->json(['html'=>$view]);
    }

    //my orders
    public function my_orders(){
        $orders=Order::where('user_id',Auth::User()->id)->orderBy('id','DESC')->paginate(10);
        foreach ($orders as $value) {
            $value->products=Product::whereIn('id',json_decode($value->product_id))->select('name','id','image')->get();
        }
        return view::make('front.my_orders',compact('orders'));
    }

 
     //forgot password
     public function forgot_password(){
        if(Auth::check())
        {
            return  Redirect::back();
        }
        else
        {
            return view::make('front.forgot_password');
        }
    }

      //my orders
    public function reset_password($id){
        if(Auth::check())
        {
            return  Redirect::back();
        }
        else
        {
            return view::make('front.password_reset');
        }
    }


    public function editaddress(Request $r){
        $data=UserAddress::where('user_id',Auth::user()->id)->where('id',$r->address_id)->first();
        $states=States::where('status',0)->get();
        return response()->json(['message'=>'success','status'=>200,'data'=>$data,'states'=>$states]);
    }

    public function deleteaddress(Request $r){
        $delete=UserAddress::where('user_id',Auth::user()->id)->where('id',$r->address_id)->delete();
        if($delete){
            return redirect()->back()->with('success','Address delete successfully');
        }
        else{
            return redirect()->back()->with('error','Something went wrong!');
        }
    }


    //Add user address
    public function addaddress(Request $r){
        // dd($r->all());
        $insert=new UserAddress;
        $insert->user_id=Auth::user()->id;
        $insert->firstname=$r->firstname;
        $insert->lastname=$r->lastname;
        $insert->address= $r->address;
        $insert->city=$r->city;
        $insert->type=$r->type;
        $insert->state=$r->state;
        $insert->default=$r->default;
        $insert->mobile=$r->mobile;
        $insert->pincode=$r->pincode;    
        $insert->save();
        if ($insert) {
            $update=UserAddress::where('id','!=',$insert->id)->update(['default'=>0]);
            if ($r->ajax()) {
                return response()->json(['status'=>'success','data'=>$insert]);
            }
            return redirect()->back()->with('success','Address added successfully');
        }
        else {
            if ($r->ajax()) {
                return response()->json(['status'=>'fail']);
            }
            return redirect()->back()->with('error','Something went wrong');
        }
    }



    public function updateaddress(Request $r){
        if($r->default==1){
            $update=UserAddress::where('id','!=',$r->id)->update(['default'=>0]);
        }
        $update=UserAddress::find($r->id);
        $update->firstname=$r->firstname;
        $update->lastname=$r->lastname;
        $update->address= $r->address;
        $update->city=$r->city;
        $update->type=$r->type;
        $update->state=$r->state;
        $update->mobile=$r->mobile;
        $update->pincode=$r->pincode;    
        $update->default=$r->default;
        $save=$update->save();
        if ($save) {
            return redirect()->back()->with('success','Address Update successfully');
        }
        else {
            return redirect()->back()->with('error','Something went wrong');
        }
    } 


    //Auth generate
    public function auth_check(Request $r)
    { 
        $user = Auth::loginUsingId($r->id);
        $user = Auth::user();
        if(Auth::check())
        {
            $user=Auth::user();
            return response()->json(['message'=>'success','status'=>200]);
        }
        else
        {
            return response()->json(['message'=>'error','status'=>200]);
        }

    }
    
    
    //Logout
    public function logout(Request $request) {
        Auth::logout();
        return redirect('/login');
    }

    //update profile

    public function mobile_verify(Request $r){
        $otp=mt_rand(10000, 99999);
        $this->send_otp($r->phone,$otp);
        $user=User::where('id',Auth::user()->id)->update(['otp'=>$otp,'phone'=>$r->phone]);
        return response()->json(['message'=>'success','status'=>200]);
    }
    
    public function update_profile(Request $r){
        $data=User::where('id',Auth::user()->id)->first();
        if($data->mobile_verify==0){
            if($data->otp==$r->otp){
                $data->name=$r->firstname;
                $data->mobile_verify=1;
                $data->lastname=$r->lastname;
                $update=$data->save();
                return response()->json(['message'=>'success','status'=>200]);
            }
            else{
                return response()->json(['message'=>'Invalid Otp','status'=>500]);
            }
        }
        else{
            $data->name=$r->firstname;
            $data->lastname=$r->lastname;
            $update=$data->save();
            return response()->json(['message'=>'success','status'=>200]);

        }
      
       
    }

    // public function send_otp(Request $r){
    //     $phone=$r->mobile;
    //     $otp=mt_rand(10000, 99999);
    //     $this->send_otp($phone,$otp);
    //     $update=User::where('id',Auth::user()->id)
    //     return response()->json(['message'=>'success','status'=>200]);
    // }



    //profile
    public function profile(){
        $data=UserAddress::where('user_id',Auth::user()->id)->get();
        return view::make('front.profile',compact('data'));
    }

    //Change Password
    public function change_password(Request $r){
        $data=User::where('id',Auth::user()->id)->first();
        
        if(Hash::check($r->old_password,$data->password)){
            # code...
            $data->password=Hash::make($r->new_password);
            $save=$data->save();
            return redirect()->back()->with('success','Password Change Successfully');
        }
        else{
            return redirect()->back()->with('error','Incorrect Old Password');
        }
    }

    //user address
    public function user_address(){
        $address=UserAddress::where('user_id',Auth::user()->id)->get();
        $states=States::where('status',0)->get();
        return view::make('front.address',compact('address','states'));     
    }


    //login
    public function login(){

        if(Auth::check())
        {
            return  Redirect::back();
        }
        else
        {
            return view::make('front.login');
        }

    }

      //register
    public function register(){

        if(Auth::check())
        {
            return  Redirect::back();
        }
        else
        {
            return view::make('front.register');
        }
        
    }

    //contact
    public function contact(){
        return view::make('front.contact');
    }
    

    //Main index page Web
    public function index(){

        $data=Category::where('publish',0)->get();
        $banner=banner::where('publish',0)->get();
        $brand=brand::where('publish',0)->get();

        $product=Product::orderBy('id','DESC')->where('status',0)->paginate(10);

        foreach($product as $p){
            $rating_sum=Review::where('product_id',$p->id)->sum('rating');
            $review_count=Review::where('product_id',$p->id)->count();
            if($review_count!=0){
                $p->star_rating=round($rating_sum/$review_count);
            }
            else{
                $p->star_rating=0;
            }
            $p->review_count=$review_count;
        }

        return view::make('front.index',compact('data','banner','product','brand'));
    }

    //Product Detail
    public function product_detail($id){

        $data=Product::where('slug_name',$id)->first();
        $brand_name=Brand::where('id',$data->brand)->select('name')->first();
        $rating=Review::where('product_id',$data->id)->get();
        $rating_sum=Review::where('product_id',$data->id)->sum('rating');
        $review_count=Review::where('product_id',$data->id)->count('id');
        if($review_count!=0){
         $star_rating=round($rating_sum/$review_count);
        }
        else{
            $star_rating=0;   
        }
        foreach($rating as $r){
            $user=User::where('id',$r->id)->select('id','name')->first();
            $user->username=$user->name;
        }
        $other_product=Product::paginate(10);

        // check if item is in wishlist
        $status=0;
        $cartStatus=0;
        if(Auth::check()){
            $check=wishlist::where('user_id',Auth::User()->id)->where('product_id',$data->id)->first();
            if ($this->check_count($check)>0) {
                $status=1;
            }

            $cartCheck=Cart::where('userid',Auth::User()->id)->where('productid',$data->id)->first();
            if ($this->check_count($cartCheck)>0) {
                $cartStatus=1;
            }

        }

        return view::make('front.product_detail',compact('brand_name','data','other_product','rating','star_rating','review_count','status','cartStatus'));
    }

        //Product Detail
        public function demo_product_detail($id){
            $data=Product::where('id',decrypt($id))->first();
            $other_product=Product::paginate(10);
            return view::make('front.demo_product_detail',compact('data','other_product'));
        }

    //Product Listing
    public function product_listing($type=null){
        $brand=Brand::select('name','id')->get();
        foreach($brand as $b){
            $count=Product::where('brand',$b->id)->count();
            $b->count=$count;
        }
        $products=Product::orderBy('id','DESC')->paginate(10);
        foreach($products as $p){
            $rating_sum=Review::where('product_id',$p->id)->sum('rating');
            $review_count=Review::where('product_id',$p->id)->count();
            if($review_count!=0){
                $p->star_rating=round($rating_sum/$review_count);
            }
            else{
                $p->star_rating=0;
            }
            $p->review_count=$review_count;
        }

        if($type=='list'){
            return view::make('front.product_list_view',compact('products','brand'));
        }
        else{
            return view::make('front.product_grid_view',compact('products','brand'));
        }
    }

    //Cart
    public function cart(){
        $data=DB::table('carts')
            ->join('products', 'carts.productid', '=', 'products.id')
            ->select('carts.*', 'products.name', 'products.image','products.new_price as price','products.tax')
            ->where('carts.userid',Auth::user()->id)
            ->get();
        return view::make('front.cart',compact('data'));
    }



    //Filter Product
    public function filter_product(Request $r){
        $url=URL('/');
        $imgurl=$url.'/image/product/';
        if($r->price_type=='high'){
            $products=Product::orderBy('new_price', 'desc');
        }
        else{
            $products=Product::orderBy('new_price', 'asc');
        }
        if($r->category){
            $products->whereIn('subcategory',$r->category);
        }
        if($r->brand){
            $products->whereIn('brand',$r->brand);
        }
        if($r->min_price!=0 && $r->max_price!=0){
          $products=$products->where('new_price','>=',$r->min_price)->where('new_price','<=',$r->max_price)->get();
        }
        else{
            $products=$products->get();  
        }


        $data='';

        // dd($products);

        if(count($products) > 0){

            foreach ($products as $product){
                $product_detail=$url.'/product/'.$product->slug_name;
                $rating_sum=Review::where('product_id',$product->id)->sum('rating');
                $review_count=Review::where('product_id',$product->id)->count();
                if($review_count!=0){
                    $star_rating=round($rating_sum/$review_count);
                }
                else{
                    $star_rating=0;
                }
                

               if($star_rating==0){
                $rating='<p class="rev"><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><span class="margin-left-10">'.$review_count.'Review(s)</span></p>';
                }
                elseif($star_rating==1){
                    $rating='<p class="rev"><i class="fa fa-star"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><span class="margin-left-10">'.$review_count.'Review(s)</span></p>';
                }
                elseif ($star_rating==2) {
                    $rating='<p class="rev"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><span class="margin-left-10">'.$review_count.'Review(s)</span></p>';
                }elseif ($star_rating==3) {
                    $rating='<p class="rev"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i> <i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><span class="margin-left-10">'.$review_count.'Review(s)</span></p>';
                }
                elseif ($star_rating==4) {
                    $rating='<p class="rev"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i> <i class="fa fa-star"></i><i class="fa fa-star-o"></i><span class="margin-left-10">'.$review_count.'Review(s)</span></p>';
                }
                elseif ($star_rating==5) {
                    $rating='<p class="rev"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i> <i class="fa fa-star"></i><i class="fa fa-star"></i><span class="margin-left-10">'.$review_count.'Review(s)</span></p>';
                }
                if ($product->variant_status=="yes"){
                    $variant=json_decode($product->variant);
                    foreach($variant as $value){
                        foreach($value as $v=>$w){
                            foreach($w as $k=>$p){
                                if($k==0){
                                $price=$product->new_price; $quantity=$p->quantity;
                                }
                            }
                        }
                    }
                }
                else{
                    $price=$product->new_price;
                    $quantity=$product->quantity;
                }
            
                if($quantity!='0'){
                    $quantity='<p>Availability: <span class="in-stock">In stock</span></p></div>';
                    }
                    else{
                    $quantity='<p>Availability: <span class="in-stock" style="color:red;">Out of stock</span></p>';
                }
        
                    $img=json_decode($product->image,true);
                    if(count($img) > 0){
                    $proimage=$img[0];
                    }
                    else {
                    $proimage='';
                    }
                if($r->type=='grid'){
                    $data .='<a href="'.$product_detail.'"><div class="item-col-4">
                    <div class="product">
                      <article> <img class="img-responsive" src="'.$imgurl.$proimage.'" alt="" style="height:200px;">
                        <span class="tag"></span> <a href="#." class="tittle">'.$product->name.'</a>'.$rating.' 
                      <div class="price"><i class="fa fa-inr" aria-hidden="true"></i>'.number_format($price,2).'</div>
                      <br>'.$quantity.'
                      </article>
                    </div>
                  </div></a>';
                }
                else{
                    $data .= '<article>                   
                    <!-- Product img -->
                    <div class="media-left">
                      <div class="item-img"> <img class="img-responsive" src="'.$imgurl.$proimage.'" alt="" >  </div>
                    </div>                  
                    <!-- Content -->
                    <div class="media-body">
                      <div class="row">                       
                        <!-- Content Left -->
                        <div class="col-sm-7"> <span class="tag"></span> <a href="#." class="tittle">'.$product->name.'</a>'.$rating.' 
                          <ul class="bullet-round-list">
                              <div class="text">'.
                              $product->short_description
                              .'</div>
                          </ul>
                        </div>                      
                        <!-- Content Right -->
                        <div class="col-sm-5 text-center"> 
                          <div class="position-center-center">
                                <div class="price"><i class="fa fa-inr" aria-hidden="true"></i>'.number_format($product->new_price,2).'</div>
                              <br>'.
                             $quantity
                        .'</div>
                      </div>
                    </div>
                  </article>';
                }
            }
            
        }
        else{
            $image=$url.'/image/error-404.png';
            $data="<div class='row' style='text-align:center;'><div class='col-md-12'><img src=".$image."><h6 style='text-align:center'>No Product Found</h6></div></div>";

        }

        // dd($data);

        
    return response()->json(['message'=>'success','data'=>$data]);
       
    }

    public function order_detail($id){
        $data=DB::table('orders')->join('user_addresses','orders.address_id','user_addresses.id')->select('orders.*','user_addresses.firstname','user_addresses.lastname','user_addresses.mobile','user_addresses.address','user_addresses.city','user_addresses.state','user_addresses.pincode')->where('orders.id',decrypt($id))->first();
        return view('front.order_detail',compact('data'));
    }
    
}