<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Auth;
use Redirect;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function check_count($check)
	{
		if($check)
		{
			$val =  sizeof(json_decode(json_encode($check),true));
		}
		else
		{
			$val =  0;
		}
		return $val;
    }


	function send_otp($phone,$otp){
		
		$curl = curl_init();
		$phone="+91".$phone;

		curl_setopt_array($curl, array(
		CURLOPT_URL => "https://api.msg91.com/api/v5/otp?invisible=1&authkey=207583An9pBb60SQ5ac4ac02&otp=".$otp."&mobile=".$phone."&template_id=60770a97f63b9979a5394b0f",
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => "",
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 30,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => "GET",
		CURLOPT_POSTFIELDS => "{\"Value1\":\"Param1\",\"Value2\":\"Param2\",\"Value3\":\"Param3\"}",
		CURLOPT_HTTPHEADER => array(
			"content-type: application/json"
		),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
		echo "cURL Error #:" . $err;
		} else {
		return $response;
	
		}
       
	}
}
