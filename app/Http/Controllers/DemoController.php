<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Order;
use App\Models\UserAddress;
use DB;
use View;
use Auth;

class DemoController extends Controller
{
    //
    public function my_orders(){
        $orders=DB::table('orders')
                    ->join('products','products.id','orders.product_id')
                    ->join('user_addresses','user_addresses.id','orders.address_id')
                    ->where('orders.user_id',Auth::User()->id)
                    ->select('orders.*','products.name as product_name','user_addresses.firstname','user_addresses.lastname','user_addresses.mobile','user_addresses.address','user_addresses.city','user_addresses.state','user_addresses.pincode')
                    ->orderBy('orders.id','DESC')->get();
                    // print_r($orders);
        return view::make('front.my_orders',compact('orders'));
    }

    public function order_detail($id){
        $data=Order::where('id',decrypt($id))->first();
        $address=UserAddress::where('id',$data->address_id)->first();
        return view('front.order_detail',compact('data','address'));
    }
}
