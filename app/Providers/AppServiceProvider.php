<?php

namespace App\Providers;
use App\Models\Category;
use App\Models\SubCategory;
use App\Models\Subsubcategory;
use App\Models\banner;
use Illuminate\Support\ServiceProvider;
use App\Http\Controllers\Controller;
use App\Models\Cart;
use App\Models\wishlist;
use Auth;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {       
        $category=Category::where('publish',0)->select('name','id','slug_name','image')->limit(9)->get();
        $subcategory=SubCategory::where('publish',0)->select('name','slug_name','id','category')->get();
        foreach ($category as $cat) {
            $subcategory=SubCategory::where('category',$cat->id)->where('publish',0)->select('name','slug_name','id','category')->get();
            
            if(sizeof(json_decode(json_encode($subcategory),true)) > 0){
                foreach($subcategory as $subcat){
                    $subsubcategory=Subsubcategory::where('category',$subcat->category)->where('subcategory',$subcat->id)->where('publish',0)->select('id','name','slug_name')->get();
                    $subcat->subsubcategory=$subsubcategory;
                }
            }

            $cat->sub_cat=$subcategory;
        }

        $banner=banner::where('publish',0)->get();\
        view()->share('category',$category);
        view()->share('subcategory',$subcategory);
        view()->share('banner',$banner);
       

        view()->composer('*', function($view) {
            if (Auth::check() ) {                
                $wishlist=wishlist::where('user_id',Auth::user()->id)->count();
                $cart=Cart::where('userid',Auth::user()->id)->count();               
                $view->with('wishlistCount', $wishlist);
                $view->with('cartCount', $cart);
            } else{
                $view->with('wishlistCount',0);
                $view->with('cartCount',0);
            } 
        });
        

    }

}
