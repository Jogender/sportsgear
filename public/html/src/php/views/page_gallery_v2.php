<?php defined('safe_access') or die('Restricted access!'); ?>

<div id="page_content">
    <div id="page_content_inner">

        <div class="gallery_grid uk-grid-width-medium-1-4 uk-grid-width-large-1-5" id="galleryGrid">
            <?php for($i=1;$i<=12;$i++) { ?>
                <div>
                    <div class="md-card md-card-hover">
                        <div class="gallery_grid_item md-card-content">
                            <a href="#" class="custom-modal-open" data-image-id="<?php echo $i;?>">
                                <img src="assets/img/gallery/Image<?php if($i < 10) { echo '0';}; echo $i; ?>.jpg" alt="">
                            </a>
                            <div class="gallery_grid_image_caption">
                                <div class="gallery_grid_image_menu" data-uk-dropdown="{pos:'top-right'}">
                                    <i class="md-icon material-icons">&#xE5D4;</i>
                                    <div class="uk-dropdown uk-dropdown-small">
                                        <ul class="uk-nav">
                                            <li><a href="#"><i class="material-icons uk-margin-small-right">&#xE150;</i> Edit</a></li>
                                            <li><a href="#"><i class="material-icons uk-margin-small-right">&#xE872;</i> Remove</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <span class="gallery_image_title uk-text-truncate"><?php echo $faker->sentence(4);?></span>
                                <span class="uk-text-muted uk-text-small"><?php echo rand(1,30);?> Jun 2018, <?php echo rand(20,100)?>KB</span>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>

        <div id="custom-ligthbox" class="uk-modal">
            <div class="uk-modal-dialog uk-modal-dialog-lightbox uk-modal-dialog-large uk-slidenav-position">
                <a href="" class="uk-modal-close uk-close uk-close-alt"></a>
                <div id="lightbox-content"></div>
            </div>
        </div>

        <script id="lightbox-template-content" type="text/x-handlebars-template">
            <div class="uk-grid uk-grid-collapse">
                <div class="uk-width-medium-2-3 uk-text-center">
                    <div class="uk-position-relative uk-display-inline-block">
                        <img src="{{ img }}" alt="" class="custom-lightbox-image">
                        <a href="#" class="uk-slidenav uk-slidenav-contrast uk-slidenav-previous uk-hidden-touch"></a>
                        <a href="#" class="uk-slidenav uk-slidenav-contrast uk-slidenav-next uk-hidden-touch"></a>
                    </div>
                </div>
                <div class="uk-width-medium-1-3">
                    <div class="uk-modal-aside">
                        <article class="uk-comment">
                            <header class="uk-comment-header">
                                <img class="md-user-image uk-comment-avatar" src="{{ user_avatar }}" alt="">
                                <h4 class="uk-comment-title">{{ user_name }}</h4>
                                <div class="uk-comment-meta">25/Jun/18 14:26</div>
                            </header>
                            <div class="uk-comment-body">
                                <p>{{ comment }}</p>
                            </div>
                        </article>
                    </div>
                </div>
            </div>
        </script>

<?php
echo '<script>';
echo 'lightbox_data = [';
for($i=0;$i<=12;$i++) { ?>
            {
                'img': 'assets/img/gallery/Image<?php if($i < 10) { echo '0';}; echo $i; ?>.jpg',
                'user_avatar': 'assets/img/avatars/avatar_<?php $randImg = rand(1,12); if($randImg<10) echo '0'; echo $randImg; ?>_tn.png',
                'user_name': '<?php echo htmlspecialchars(addslashes($faker->firstName)); echo ' '.htmlspecialchars(addslashes($faker->lastName)); ?>',
                'comment': '<?php echo $faker->sentence(20)?>'
            }<?php if($i != 12) echo ','; ?>
<?php
};
echo ']';
echo '</script>';
?>

    </div>
</div>