<?php defined('safe_access') or die('Restricted access!'); ?>

    <div id="page_content">
        <div id="page_content_inner">

            <div class="md-card">
                <div class="md-card-content">
                    <div class="uk-grid" data-uk-grid-margin>
                        <div class="uk-width-1-1">
                            <div id="gantt_chart"></div>
                            <script>
                                ganttData = [
                                    {
                                        name: "Concept",
                                        series: [
                                            {
                                                name: "Brainstorm",
                                                sub_series: [
                                                    {
                                                        id: 1,
                                                        start: '08/01/2018',
                                                        end: '08/03/2018',
                                                        color: "#039BE5",
                                                        title: 'Custom title',
                                                        link: 'http://themeforest.com',
                                                        user_name: "<?php echo $faker->firstNameMale; echo ' '.$faker->lastName; ?>",
                                                        user_avatar: "assets/img/avatars/avatar_01_tn.png"
                                                    },
                                                    {
                                                        id: 2,
                                                        start: '08/05/2018',
                                                        end: '08/08/2018',
                                                        color: "#039BE5"
                                                    }
                                                ]
                                            },
                                            {
                                                name: "Wireframes",
                                                sub_series: [
                                                    {
                                                        id: 3,
                                                        start: '08/04/2018',
                                                        end: '08/07/2018',
                                                        color: "#0288D1",
                                                        title: 'lorem ipsum dolor',
                                                        user_name: "<?php echo $faker->firstNameMale; echo ' '.$faker->lastName; ?>",
                                                        user_avatar: "assets/img/avatars/avatar_03_tn.png"
                                                    },
                                                    {
                                                        id: 4,
                                                        start: '08/10/2018',
                                                        end: '08/14/2018',
                                                        color: "#0288D1"
                                                    },
                                                    {
                                                        id: 5,
                                                        start: '08/18/2018',
                                                        end: '08/26/2018',
                                                        color: "#0277BD",
                                                        user_name: "<?php echo $faker->firstNameMale; echo ' '.$faker->lastName; ?>",
                                                        user_avatar: "assets/img/avatars/avatar_06_tn.png"
                                                    }
                                                ]
                                            },
                                            {
                                                id: 6,
                                                name: "Concept description",
                                                start: '08/06/2018',
                                                end: '08/10/2018',
                                                color: "#0277BD"
                                            }
                                        ]
                                    },
                                    {
                                        name: "Design",
                                        series: [
                                            {
                                                id: 7,
                                                name: "Sketching",
                                                start: '08/08/2018',
                                                end: '08/16/2018',
                                                color: "#673AB7"
                                            },
                                            {
                                                id: 8,
                                                name: "Photography",
                                                start: '08/10/2018',
                                                end: '08/16/2018',
                                                color: "#5E35B1",
                                                title: 'Some inspirations',
                                                link: 'https://unsplash.com/',
                                                user_name: "<?php echo $faker->firstNameMale; echo ' '.$faker->lastName; ?>",
                                                user_avatar: "assets/img/avatars/avatar_05_tn.png"
                                            },
                                            {
                                                name: "Feedback",
                                                sub_series: [
                                                    {
                                                        id: 9,
                                                        start: '08/19/2018',
                                                        end: '08/21/2018',
                                                        color: "#512DA8"
                                                    },
                                                    {
                                                        id: 10,
                                                        start: '08/24/2018',
                                                        end: '08/28/2018',
                                                        color: "#512DA8"
                                                    }
                                                ]

                                            },
                                            {
                                                id: 11,
                                                name: "Final Design",
                                                start: '08/21/2018',
                                                end: '08/29/2018',
                                                color: "#4527A0",
                                                user_name: "<?php echo $faker->firstNameFemale; echo ' '.$faker->lastName; ?>",
                                                user_avatar: "assets/img/avatars/avatar_02_tn.png"
                                            }
                                        ]
                                    },
                                    {
                                        name: "Implementation",
                                        series: [
                                            {
                                                id: 12,
                                                name: "Specifications",
                                                start: '08/26/2018',
                                                end: '09/06/2018',
                                                color: "#8BC34A"
                                            },
                                            {
                                                id: 13,
                                                name: "Templates",
                                                start: '09/04/2018',
                                                end: '09/10/2018',
                                                color: "#7CB342"
                                            },
                                            {
                                                id: 14,
                                                name: "Database",
                                                start: '09/05/2018',
                                                end: '09/13/2018',
                                                color: "#689F38"
                                            },
                                            {
                                                id: 15,
                                                name: "Integration",
                                                start: '09/16/2018',
                                                end: '10/10/2018',
                                                color: "#558B2F",
                                                user_name: "<?php echo $faker->firstNameMale; echo ' '.$faker->lastName; ?>",
                                                user_avatar: "assets/img/avatars/avatar_07_tn.png"
                                            }
                                        ]
                                    },
                                    {
                                        name: "Testing & Delivery",
                                        series: [
                                            {
                                                id: 16,
                                                name:   "Focus Group",
                                                start:  '10/17/2018',
                                                end:    '10/27/2018',
                                                color:  "#F57C00"
                                            },
                                            {
                                                name:   "Stress Test",
                                                sub_series: [
                                                    {
                                                        id: 17,
                                                        start:  '10/25/2018',
                                                        end:    '11/06/2018',
                                                        color:  "#EF6C00"
                                                    },
                                                    {
                                                        id: 18,
                                                        start:  '11/09/2018',
                                                        end:    '11/12/2018',
                                                        color:  "#EF6C00"
                                                    }
                                                ]
                                            },
                                            {
                                                id: 19,
                                                name:   "Delivery",
                                                start:  '11/07/2018',
                                                end:    '11/12/2018',
                                                color:  "#E65100",
                                                user_name: "<?php echo $faker->firstNameFemale; echo ' '.$faker->lastName; ?>",
                                                user_avatar: "assets/img/avatars/avatar_06_tn.png"
                                            }
                                        ]
                                    }
                                ];
                            </script>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
