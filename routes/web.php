<?php

use Illuminate\Support\Facades\Route;
//User
use App\Http\Controllers\HomeController;
use App\Http\Controllers\WebController;
use App\Http\Controllers\DemoController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\FacebookController;
use App\Http\Controllers\Auth\GoogleController;

//Admin
use App\Http\Controllers\Admin\BannerController;
use App\Http\Controllers\Admin\CategoryController;
use App\Http\Controllers\Admin\ProductController;
use App\Http\Controllers\Admin\OrderController;
use App\Http\Controllers\Admin\UserController;



/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Web routes
Route::get('/mailTemp',[WebController::class, 'mailTemp'])->name('mailTemp');


Route::get('/test',[WebController::class, 'test'])->name('test');
Route::get('/',[WebController::class, 'index'])->name('index');
Route::get('/product/{id}',[WebController::class,'product_detail'])->name('product_info');
Route::get('/demo-product/{id}',[WebController::class,'demo_product_detail'])->name('demo_product_detail');
Route::get('/product-listing/{type?}',[WebController::class,'product_listing'])->name('product_listing');
Route::get('/brand-product/{id}',[WebController::class,'brand_product'])->name('brand_product');
Route::get('/category-product/{id}/{type?}',[WebController::class,'category_product'])->name('category_product');
Route::get('/subcategory-product/{id}/{type?}',[WebController::class,'subcategory_product'])->name('subcategory_product');
Route::get('/sub-2-category_product/{id}/{type?}',[WebController::class,'subsubcategory_product'])->name('subsubcategory_product');

Route::get('/contact',[WebController::class,'contact'])->name('contact');

Route::post('/auth_check',[WebController::class,'auth_check'])->name('auth_check');
Route::post('/filter-product',[WebController::class,'filter_product'])->name('filter_product');
Route::post('product-search',[WebController::class,'product_search'])->name('product_search');
Route::get('/review-error',[WebController::class,'review_error'])->name('review_error');



//Social Login
Route::get('auth/facebook', [FacebookController::class, 'redirectToFacebook'])->name('fb_login');
Route::get('auth/facebook/callback', [FacebookController::class, 'handleFacebookCallback'])->name('fb_callback');
Route::get('auth/google', [GoogleController::class, 'redirectToGoogle'])->name('gmail_login');
Route::get('auth/google/callback', [GoogleController::class,'handleGoogleCallback'])->name('gmail_callback');
Route::post('payment_status',[WebController::class,'payment_status'])->name('payment_status');


//aUTH guest login 
Route::middleware(['guest'])->group(function () {

    Route::get('/login',[WebController::class,'login'])->name('user_login');
    Route::get('/register',[WebController::class,'register'])->name('user_register');
    Route::get('/forgot-password',[WebController::class,'forgot_password'])->name('password_forgot');
    Route::get('/password-reset/{id}',[WebController::class,'reset_password'])->name('reset_password');
    Route::post('/update_password',[WebController::class,'update_password'])->name('update_password');

});

Route::middleware(['auth'])->group(function () {

    Route::post('mobile-verify',[WebController::class,'mobile_verify'])->name('mobile_verify');
    Route::post('place-order',[WebController::class,'place_order'])->name('place_order');
    Route::get('order-detail/{id}',[WebController::class,'order_detail'])->name('order_detail');
    
    Route::post('/add-to-cart',[WebController::class,'add_to_cart'])->name('add_to_cart');
    Route::get('/my-cart',[WebController::class,'cart'])->name('my_cart');

    Route::post('add_to_wishlist',[WebController::class,'add_to_wishlist'])->name('add_to_wishlist');
    Route::post('remove_to_wishlist',[WebController::class,'remove_to_wishlist'])->name('remove_to_wishlist');
    Route::get('/wishlist',[WebController::class,'wishlist'])->name('wishlist');

    Route::post('/update-address',[WebController::class,'updateaddress'])->name('update_address');
    Route::get('/home', [HomeController::class, 'index'])->name('home');
    Route::get('/my-profile',[WebController::class,'profile'])->name('user_profile');
    Route::post('/change-password',[WebController::class,'change_password'])->name('change_password');
    Route::post('/update-profile',[WebController::class,'update_profile'])->name('update_profile');
    Route::post('/add-address',[WebController::class,'addaddress'])->name('add_address');
    Route::post('/edit-address',[WebController::class,'editaddress'])->name('edit_address');
    Route::post('/delete-address',[WebController::class,'deleteaddress'])->name('delete_address');
    Route::get('/my-orders',[WebController::class,'my_orders'])->name('my_orders');
    Route::post('/add-cart',[WebController::class,'my_orders'])->name('add_cart');
    Route::get('address',[WebController::class,'user_address'])->name('user_address');
    Route::get('review/{id}',[WebController::class,'review'])->name('review');
    Route::post('submit-review',[WebController::class,'submit_review'])->name('submit_review');
    Route::get('/product-info/{id}',[WebController::class,'product_detail'])->name('product_info_auth');
    Route::post('/checkout',[WebController::class,'checkout'])->name('checkout');

    Route::get('/logout',[WebController::class,'logout'])->name('user_logout');


});

Route::prefix('admin')->group(function () {
    Auth::routes();
});

//Admin prefix + middleware
Route::middleware(['is_admin','auth'])->group(function () {

    Route::prefix('admin')->group(function () {
        Route::get('home', [HomeController::class, 'adminHome'])->name('admin.home');
        Route::get('users',[UserController::class,'users'])->name('admin.users');
        Route::get('user-detail/{id}',[UserController::class,'user_detail'])->name('admin.user_detail');

        Route::get('view-states', [HomeController::class, 'view_states'])->name('view_states');
        Route::post('update_state', [HomeController::class, 'update_state'])->name('update_state');
        Route::post('delete_state', [HomeController::class, 'delete_state'])->name('delete_state');

        Route::get('/banner', [BannerController::class, 'banner'])->name('banner');
       

        //Product
        Route::get('/product', [ProductController::class, 'product'])->name('product');
        Route::post('/add-product', [ProductController::class, 'add_product'])->name('add_product');
        Route::get('/view-product', [ProductController::class, 'view_product'])->name('view_product');
        Route::get('/product-detail/{id}', [ProductController::class, 'product_detail'])->name('product_detail');
        Route::get('/edit-product/{id}', [ProductController::class, 'edit_product'])->name('edit_product');
        Route::post('/update-product', [ProductController::class, 'update_product'])->name('update_product');

        //Category Controller
        Route::get('/category', [CategoryController::class, 'category'])->name('category');
        Route::get('/sub-category', [CategoryController::class, 'subcategory'])->name('subcategory');
        Route::get('/sub-sub-category', [CategoryController::class, 'subsubcategory'])->name('subsubcategory');
        Route::get('/product', [ProductController::class, 'product'])->name('product');
        Route::get('/order', [OrderController::class, 'order'])->name('order');
        Route::get('/order-details/{id}', [OrderController::class, 'order_details'])->name('order_details');
        Route::post('order-status',[OrderController::class],'order_status')->name('order_status');
        Route::post('change_order_status',[OrderController::class,'change_order_status'])->name('change_order_status');
        Route::get('view-invoice/{id}',[OrderController::class,'view_invoice'])->name('view_invoice');

       //Brand Details
        Route::get('/brand', [CategoryController::class, 'brand'])->name('brand');
        Route::get('/profile', [BannerController::class, 'profile'])->name('profile');
        Route::get('/setting', [BannerController::class, 'setting'])->name('setting');
        
        // Route::get('/logout', [BannerController::class, 'logout'])->name('logout');
        Route::get('/user', [BannerController::class, 'user'])->name('user');

        Route::get('/driver', [BannerController::class, 'driver'])->name('driver');
        Route::get('/pin_code', [BannerController::class, 'pin_code'])->name('pin_code');
        Route::get('/time_slot', [BannerController::class, 'time_slot'])->name('time_slot');

        Route::get('/bulk_image', [CategoryController::class, 'bulk_image'])->name('bulk_image');
        Route::post('/image_upload', [CategoryController::class, 'image_upload'])->name('image_upload');

    });
});




