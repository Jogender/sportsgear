<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

//API
use App\Http\Controllers\Api\GeneralController;
use App\Http\Controllers\Api\BrandController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

//check email
Route::post('register',[GeneralController::class,'register']);
Route::post('login',[GeneralController::class,'login']);
Route::post('verify-otp',[GeneralController::class,'verify_otp']);
Route::post('check_email',[GeneralController::class,'check_email']);
Route::post('update_password',[GeneralController::class,'update_password']);
Route::post('change_password',[GeneralController::class,'change_password']);



//Banner Api
Route::post('banner', [GeneralController::class, 'banner']);
Route::post('delete_banner', [GeneralController::class, 'delete_banner']);
Route::post('publish_banner', [GeneralController::class, 'publish_banner']);


//Category Api
Route::post('category', [GeneralController::class, 'category']);
Route::post('delete_category', [GeneralController::class, 'delete_category']);
Route::post('publish_category', [GeneralController::class, 'publish_category']);
Route::post('get_subcategory',[GeneralController::class, 'get_subcategory']);
Route::post('get_subsubcategory',[GeneralController::class, 'get_subsubcategory']);

//SubCategory Api
Route::post('subcategory', [GeneralController::class, 'subcategory']);
Route::post('delete_subcategory', [GeneralController::class, 'delete_subcategory']);
Route::post('publish_subcategory', [GeneralController::class, 'publish_subcategory']);

//Subsubcategory
//SubCategory Api
Route::post('subsubcategory', [GeneralController::class, 'subsubcategory']);
Route::post('delete_subsubcategory', [GeneralController::class, 'delete_subsubcategory']);
Route::post('publish_subsubcategory', [GeneralController::class, 'publish_subsubcategory']);


//Product
    //remove image from product
    Route::post('remove_product_file', [GeneralController::class, 'remove_product_file']);
    Route::post('publish_product', [GeneralController::class, 'publish_product']);
    Route::post('delete_product', [GeneralController::class, 'delete_product']);



//Cart
Route::post('add_to_cart',[GeneralController::class, 'add_to_cart']);
Route::post('remove_product',[GeneralController::class, 'remove_product']);



//Brand Api
Route::post('brand', [BrandController::class, 'brand']);
Route::post('delete_brand', [BrandController::class, 'delete_brand']);
Route::post('publish_brand', [BrandController::class, 'publish_brand']);
