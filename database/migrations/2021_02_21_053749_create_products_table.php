<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('category');
            $table->string('subcategory');
            $table->string('subsubcategory');
            $table->longtext('description');
            $table->longtext('short_description');
            $table->string('image');
            $table->integer('featured')->default(0);
            $table->integer('topweekly')->default(0);
            $table->integer('status')->default(0);
            $table->integer('old_price')->default(0);
            $table->integer('new_price')->default(0);
            $table->string('variant_status')->default('no');
            $table->string('variant')->default('[]');
            $table->string('brand');
            $table->integer('quantity')->default(0);
            $table->string('origin');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
